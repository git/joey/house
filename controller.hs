-- | Reads sensors from the poller, and feeds them to the FRP automation
-- to decide what actions to take to manage the house's power consumption
-- etc.
--
-- This is a separate program from the poller so it will keep running if
-- the poller crashes, and so additional instances of it can be run to
-- test new automation.

{-# LANGUAGE BangPatterns, LambdaCase #-}

import Control.Concurrent
import Control.Concurrent.Async
import Control.Concurrent.STM
import Control.Monad
import Control.Exception
import Reactive.Banana.Automation
import Data.Time.Clock
import Data.Time.Clock.POSIX
import System.Environment
import System.Timeout
import System.IO

import Automation
import Automation.Types
import Automation.SafeMode
import Sensors (SensedValue(..), Override(..), CcID(..))
import DataUnits
import WebServers
import GPIO
import GPIO.SamlexInverter
import CarCharger.GrizzlE
import qualified Epsolar
import Utility.ThreadScheduler

main :: IO ()
main = forever $ do
	w <- newWatchdog
	args <- getArgs
	logger <- mkLogger
	carcharger <- getCarChargerHandle logger
	let actuators = case args of
		[] -> (\a -> logger (show a) >> driveActuators a carcharger >> appeaseWatchdog w)
		["test"] -> (\a -> logger (show a) >> appeaseWatchdog w)
		_ -> error "bad command line"
	-- Avoid continuing to run when the automation somehow breaks and
	-- is not doing anything, by requiring it to use an actuator at
	-- least once every 20 minutes.
	runWatchdog (Seconds (60*20)) w $ runAutomation 
		homeAutomation 
		timestampedSensors 
		actuators
		feedSensors
	safeMode
	hPutStrLn stderr "restarting"

mkLogger :: IO (String -> IO ())
mkLogger = do
	v <- newMVar ()
	return $ \a -> do
		() <- takeMVar v
		putStrLn a
		hFlush stdout
		putMVar v ()

timestampedSensors :: IO (Sensors (TVar UTCTime))
timestampedSensors = mkSensors $ newTVarIO =<< getCurrentTime

-- | Feed sensor values from the poller into the automation.
--
-- Momentary glitches reading a sensor, or the poller being briefly
-- down are ignored, but if no new value arrives for a sensor within
-- 10 minutes, it's set to SensorUnavailable so that the house can fall
-- back to a safe mode.
feedSensors :: Sensors (TVar UTCTime) -> IO ()
feedSensors sensors = 
	withAsync feedunavailable $ \feedunavailablethread ->
		withAsync (feedsecondhand 1) $ const $
		withAsync (feedminutehand 1) $ const $
			withAsync feedcurrenttime $ const $ do
				withAsync feedswitches $ const $ do
					feedpoller
					-- If the feedpoller somehow crashes,
					-- keep running the feedunavailable
					-- so the automation can react to the
					-- lack of sensor data.
					wait feedunavailablethread
  where
	feedpoller = do
		streampollerthread <- async streampoller
		e <- waitCatch streampollerthread
		hPutStrLn stderr $ "streampoller crashed! " ++ show e
	streampoller = do
		fridgeoverridethread <- newEmptyTMVarIO
		inverteroverridethread <- newEmptyTMVarIO
		kitchencounteroutletoverridethread <- newEmptyTMVarIO
		springpumpoverridethread <- newEmptyTMVarIO
		carchargeroverridethread <- newEmptyTMVarIO
		streamPollerWebserverReconnect $ \case
			Right (CcOutputVolts (CcID 1) v) ->
				updatesensor (ccBatteryVoltage sensors) v
			Right (TrimetricBatteryVolts v) ->
				updatesensor (trimetricBatteryVoltage sensors) v
			Right (TrimetricBatteryPercent v) ->
				updatesensor (trimetricBatteryPercent sensors) v
			Right (TrimetricBatteryWatts v) ->
				updatesensor (trimetricBatteryWatts sensors) v
			Right (CcBatteryPercent (CcID 1) v) -> 
				updatesensor (ccBatteryPercent sensors) v
			Right (CcInputWatts (CcID 1) v) -> 
				updatesensor (cc1InputWatts sensors) v
			Right (CcInputWatts (CcID 2) v) -> 
				updatesensor (cc2InputWatts sensors) v
			Right (CcInputWatts (CcID 3) v) -> 
				updatesensor (cc3InputWatts sensors) v
			Right (CcInputWatts (CcID 4) v) -> 
				updatesensor (cc4InputWatts sensors) v
			Right (CcInputWatts (CcID 5) v) -> 
				updatesensor (cc5InputWatts sensors) v
			Right (CcKWHGeneratedToday (CcID 1) v) -> 
				updatesensor (cc1KwhGeneratedToday sensors) v
			Right (CcKWHGeneratedToday (CcID 2) v) -> 
				updatesensor (cc2KwhGeneratedToday sensors) v
			Right (CcKWHGeneratedToday (CcID 3) v) -> 
				updatesensor (cc3KwhGeneratedToday sensors) v
			Right (CcKWHGeneratedToday (CcID 4) v) -> 
				updatesensor (cc4KwhGeneratedToday sensors) v
			Right (CcKWHGeneratedToday (CcID 5) v) -> 
				updatesensor (cc5KwhGeneratedToday sensors) v
			Right (CcStatusCode (CcID 1) c) ->
				updatesensor (ccChargingStatus sensors)
					(Epsolar.chargingStatus <$> c)
			Right (FridgeRelaySetTo p) -> do
				updatesensor (fridgeRelaySetTo sensors) (Just p)
			Right (PorchTemperature v) ->
				updatesensor (porchTemperature sensors) v
			Right (FridgeTemperature v) ->
				updatesensor (fridgeTemperature sensors) v
			Right (AutomationFridgeRuntimeToday v) ->
				updatesensor (lastKnownFridgeRuntimeToday sensors) (Just v)
			Right (CarStateOfCharge v) ->
				updatesensor (carStateOfCharge sensors) v
			Right (CarChargeLimit v) ->
				updatesensor (carChargeLimit sensors) v
			Right (CarPluggedIn v) ->
				updatesensor (carPluggedIn sensors) v
			Right (CarChargerEnabled v) ->
				updatesensor (carChargerEnabled sensors) v
			Right (FridgeOverride o) ->
				override fridgeoverridethread fridgeOverride o
			Right (InverterOverride o) ->
				override inverteroverridethread inverterOverride o
			Right (KitchenCounterOutletOverride o) ->
				override kitchencounteroutletoverridethread kitchenCounterOutletOverride o
			Right (CarChargerOverride o) ->
				override carchargeroverridethread carChargerOverride o
			Right (SpringPumpOverride o) ->
				override springpumpoverridethread springPumpOverride o
			Right (SpringPumpRunCycle v) -> 
				gotEvent (springPumpRunCycle sensors) v
			Right _ -> return ()
			Left e -> hPutStrLn stderr e
	
	updatesensor s (Just v) = do
		now <- getCurrentTime
		atomically $ writeTVar (fromEventSource s) now
		sensed s v
	updatesensor s Nothing = do
		sensorUnavailable s
	
	feedunavailable :: IO ()
	feedunavailable = do
		threadDelay (fromIntegral oneMinute * 10)
		now <- getCurrentTime
		let checklast s = do
			prev <- atomically $ readTVar (fromEventSource s)
			if now `diffUTCTime` prev > 600
				then do
					atomically $ writeTVar (fromEventSource s) now
					sensorUnavailable s
				else return ()
		checklast (ccBatteryVoltage sensors)
		checklast (trimetricBatteryVoltage sensors)
		checklast (ccBatteryPercent sensors)
		checklast (cc1InputWatts sensors)
		checklast (cc2InputWatts sensors)
		checklast (cc3InputWatts sensors)
		checklast (cc4InputWatts sensors)
		checklast (cc5InputWatts sensors)
		checklast (ccChargingStatus sensors)
		checklast (cc1KwhGeneratedToday sensors)
		checklast (cc2KwhGeneratedToday sensors)
		checklast (cc3KwhGeneratedToday sensors)
		checklast (cc4KwhGeneratedToday sensors)
		checklast (cc5KwhGeneratedToday sensors)
		checklast (fridgeTemperature sensors)
		feedunavailable
	
	feedminutehand :: Int -> IO ()
	feedminutehand = feedclock oneMinute minuteHand
	
	feedsecondhand :: Int -> IO ()
	feedsecondhand = feedclock oneSecond secondHand

	feedclock d f n = do
		-- Putting the delay first gives sensor values time to
		-- arrive before events that are rate limited start
		-- occurring.
		threadDelay (fromIntegral d)
		let !n' = max 0 (succ n `mod` 60)
		gotEvent (f sensors) (ClockSignal n')
		feedclock d f n'

	feedcurrenttime :: IO ()
	feedcurrenttime = do
		clockSignal (currentTime sensors)
		threadDelay (fromIntegral oneMinute)
		feedcurrenttime

	-- GPIO may need to be polled for some switches, but it's also
	-- possible for some GPIO inputs to trigger interrupts. So,
	-- switches are not handled by the poller.
	feedswitches :: IO ()
	feedswitches = forever $ do
		-- Poll every 1 second, as this GPIO port does not support
		-- interrupts. (Need to rewire daughterboard to move it to 
		-- a port that does.)
		threadDelaySeconds (Seconds 1)
		readGPIOValue kitchenCounterSwitch >>= \case
			Just v -> do
				now <- getPOSIXTime
				gotEvent (kitchenCounterSwitchSetTo sensors) $
					Sensed (v, now)
			Nothing ->
				sensorUnavailable (kitchenCounterSwitchSetTo sensors)

	-- Send event to initiate an override, then wait until it expires
	-- and send event to clear the override.
	--
	-- Only one override can be active at a time, so if another comes
	-- in, cancel any previous clearing thread.
	override tv f o@(OverrideSeconds secs _) = do
		stopoverrideclearer tv
		maybe (return ()) cancel =<< atomically (tryTakeTMVar tv)
		gotEvent (f sensors) (fmap powerSettingToPowerChange o)
		clearer <- async (overrideclearer f secs)
		atomically $ putTMVar tv clearer
	override tv f DisableOverride = do
		clearoverride f
		stopoverrideclearer tv
	
	stopoverrideclearer tv = 
		maybe (return ()) cancel =<< atomically (tryTakeTMVar tv)

	overrideclearer f secs = do
		if secs > 0
			then threadDelaySeconds (Seconds secs)
			else return ()
		clearoverride f
	
	clearoverride f = gotEvent (f sensors) DisableOverride

driveActuators :: Actuators -> CarChargerHandle -> IO ()
driveActuators a carcharger =
	-- All exceptions are caught and ignored; if an actuator
	-- fails to work, the automation will generally retry it.
	--
	-- The actuation is done in a separate thread to avoid
	-- blocking any other desired actuations if it takes too long.
	-- Wait up to 10 minutes for it to finish before timing out.
	void $ forkIO $ void $ timeout (fromIntegral oneMinute * 10) $ void $ try' (go a)
  where
	go (FridgeRelay p) = do
		now <- getPOSIXTime
		setGPIOPower relayA $ powerChangeToPowerSetting p
		void $ postSensedValue $ FridgeRelaySetTo (powerChangeToPowerSetting p, now)
	go (SpringPumpFloatSwitch p) = do
		setSpringPumpPower $ powerChangeToPowerSetting p
	go (UsbHubsRelay p) = do
		setGPIOPower relayC $ powerChangeToPowerSetting p
		void $ postSensedValue $ UsbHubsRelaySetTo $ powerChangeToPowerSetting p
	go (KitchenCounterOutletRelay p) = do
		void $ postSensedValue $ KitchenCounterOutletRelaySetTo $ powerChangeToPowerSetting p
		setGPIOPower relayD $ powerChangeToPowerSetting p
	go (InverterPower p) =
		setSamlexInverterPower $ powerChangeToPowerSetting p
	go (CarCharger p) =
		setCarChargerPower carcharger $ powerChangeToPowerSetting p
	-- Send back to the poller so it will be available on its
	-- web server.
	go (OverallStatus s) = void $
		postSensedValue $ AutomationOverallStatus s
	go (FridgeRuntimeToday v) = void $
		postSensedValue $ AutomationFridgeRuntimeToday v
	go (FridgeRunDuration v) = void $
		postSensedValue $ AutomationFridgeRunDuration v
	go (Debug _ _) = return ()
	go (ActuatorSequence s) = mapM_ gos s

	gos (InSequence actuator) = go actuator
	gos (PauseFor t) = threadDelay $ floor $ t * fromInteger oneSecond

	try' :: IO a -> IO (Either SomeException a)
	try' = try

oneMinute:: Integer
oneMinute = oneSecond * 60

newtype Watchdog = Watchdog (TVar UTCTime)

newWatchdog :: IO Watchdog
newWatchdog = Watchdog <$> (newTVarIO =<< getCurrentTime)

appeaseWatchdog :: Watchdog -> IO ()
appeaseWatchdog (Watchdog w) = atomically . writeTVar w =<< getCurrentTime

-- Run action, and if it fails to appease the watchdog periodically, 
-- cancels the action.
runWatchdog :: Seconds -> Watchdog -> IO () -> IO ()
runWatchdog t (Watchdog w) a = void $ a `race` poller
  where
	poller = do
		start <- getCurrentTime
		threadDelaySeconds t
		lastactivity <- atomically $ readTVar w
		if lastactivity < start
			then return ()
			else poller
