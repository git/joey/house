/* From https://gist.github.com/user890104/60110bcb6272d0e5e177
 *
 * License unknown!
 */

#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <fcntl.h>
#include <errno.h>
#include <sys/ioctl.h>

#include <linux/i2c-dev.h>

#define BYTES2SHORT(X) (X[0] << 8 | X[1])

enum cmd_id {
	MEAS_HUM,
	MEAS_TEMP,
	READ_TEMP,
	RESET,
	WRITE_USER_REG,
	READ_USER_REG,
	WRITE_HEATER_REG,
	READ_HEATER_REG,
	READ_SN_1,
	READ_SN_2,
	READ_FWREV,
	CMDS_COUNT
};

char **cmd;

unsigned int i2c_rw(int file, const char *wdata, size_t wlen, char *rdata, size_t rlen) {
	if (wlen) {
		if (write(file, wdata, wlen) != wlen) {
			perror("write");
			return 1;
		}
	}
	
	if (rlen) {
		int retries = 10;
		int result = read(file, rdata, rlen);

		while (
			result == -1 && // read error
			errno == ENXIO && // NACK
			(--retries > 0)
		) {
			result = read(file, rdata, rlen); // retry
			usleep(5000); // wait 5 ms
		}
		
		if (result != rlen) {
			perror("read");
			return 2;
		}		
	}
	
	return 0;
}

int si7021_print_env(int file) {
	unsigned char resp[3];
	unsigned char resp2[3];
	
	if (i2c_rw(file, cmd[MEAS_HUM], 1, resp, 3)) {
		return 1;
	}
	if (i2c_rw(file, cmd[MEAS_HUM], 1, resp2, 3)) {
		return 1;
	}

	float hum = 125.0 * BYTES2SHORT(resp) / 65536 - 6;
	float hum2 = 125.0 * BYTES2SHORT(resp2) / 65536 - 6;

	if (hum - hum2 > 3 || hum - hum2 < -3) {
		return 1;
	}
	
	if (i2c_rw(file, cmd[READ_TEMP], 1, resp, 2)) {
		return 1;
	}
	if (i2c_rw(file, cmd[READ_TEMP], 1, resp2, 2)) {
		return 1;
	}
	
	float temp = 175.72 * BYTES2SHORT(resp) / 65536 - 46.85;
	float temp2 = 175.72 * BYTES2SHORT(resp) / 65536 - 46.85;

	if (temp - temp > 1 || temp - temp2 < -1) {
		return 1;
	}
	
	printf("Temperature in Celsius : %f\nRelative Humidity : %f\n", temp, hum);
	
	return 0;
}

int main(int argc, char **argv) {
	cmd = calloc(CMDS_COUNT, sizeof(char *));

	cmd[MEAS_HUM] = "\xF5";
	cmd[MEAS_TEMP] = "\xF3";
	cmd[READ_TEMP] = "\xE0";
	cmd[RESET] = "\xFE";
	cmd[WRITE_USER_REG] = "\xE6";
	cmd[READ_USER_REG] = "\xE7";
	cmd[WRITE_HEATER_REG] = "\x51";
	cmd[READ_HEATER_REG] = "\x11";
	cmd[READ_SN_1] = "\xFA\x0F";
	cmd[READ_SN_2] = "\xFC\xC9";
	cmd[READ_FWREV] = "\x84\xB8";

    int file;
    char *filename;
    int addr = 0x40;
	
	filename = strdup("/dev/i2c-1");

    if ((file = open(filename, O_RDWR)) < 0) {
		perror("open");
        exit(1);
    }

    if (ioctl(file, I2C_SLAVE, addr) < 0) {
		perror("ioctl I2C_SLAVE");
        exit(1);
    }
	
	si7021_print_env(file);
	
	close(file);
	
	return 0;
}
