function produced_watts() {
	this.getName = function() {return 'produced_watts';}
	this.getDSNames = function() {return ['cc1_input_watts','cc2_input_watts','cc3_input_watts','cc4_input_watts','cc5_input_watts'];}
	this.computeResult = function(val_list) {return val_list[0]+val_list[1]+val_list[2]+val_list[3]+val_list[4];}
}
       
function consumed_watts() {
	this.getName = function() {return 'consumed_watts';}
	this.getDSNames = function() {return ['battery_watts','cc1_input_watts','cc2_input_watts','cc3_input_watts','cc4_input_watts','cc5_input_watts'];}
	this.computeResult = function(val_list) {return (val_list[1]+val_list[2]+val_list[3]+val_list[4]+val_list[5])-val_list[0];}
}

function kwh_today() {
	this.getName = function() {return 'kwh_today';}
	this.getDSNames = function() {return ['cc1_kwh_today','cc2_kwh_today','cc3_kwh_today','cc4_kwh_today','cc5_kwh_today'];}
	this.computeResult = function(val_list) {return val_list[0]+val_list[1]+val_list[2]+val_list[3]+val_list[4];}
} 

var graph_opts={ legend: { noColumns:4 } };

var rrdflot_defaultsfrequent={ use_checked_DSs:true, checked_DSs:['battery_percent', 'produced_watts', 'consumed_watts', 'battery_watts'] };
var dsfrequent=
	{ battery_percent: { label: 'battery percent', color: "#000000" }
	, produced_watts: { label: 'produced watts', color: "#42f448" }
	, consumed_watts: { label: 'consumed watts', color: "#f20c0c" }
	, battery_watts: { label: 'battery watts', color: "#016daf" }
	, battery_volts: { label: 'battery volts', color: "#016daf" }
	, cc1_input_volts: { label: 'cc1 input volts', color: "#CC66FF" }
	, cc1_output_volts: { label: 'cc1 output volts', color: "#CC66FF" }
	, cc1_input_watts: { label: 'cc1 watts', color: "#CC66FF" }
	, cc2_input_volts: { label: 'cc2 input volts', color: "#6600FF" }
	, cc2_output_volts: { label: 'cc2 output volts', color: "#6600FF" }
	, cc2_input_watts: { label: 'cc2 watts', color: "#6600FF" }
	, cc3_input_volts: { label: 'cc3 input volts', color: "#0099FF" }
	, cc3_output_volts: { label: 'cc3 output volts', color: "#0099FF" }
	, cc3_input_watts: { label: 'cc3 watts', color: "#0099FF" }
	, cc4_input_volts: { label: 'cc4 input volts', color: "#00CCCC" }
	, cc4_output_volts: { label: 'cc4 output volts', color: "#00CCCC" }
	, cc4_input_watts: { label: 'cc4 watts', color: "#00CCCC" }
	, cc5_input_volts: { label: 'cc5 input volts', color: "#CC00CC" }
	, cc5_output_volts: { label: 'cc5 output volts', color: "#CC00CC" }
	, cc5_input_watts: { label: 'cc5 watts', color: "#CC00CC" }
	};
var ds_op_frequent=["battery_percent",new produced_watts(), new consumed_watts(), "battery_watts", "battery_volts", "cc1_input_volts", "cc2_input_volts", "cc3_input_volts", "cc4_input_volts", "cc5_input_volts", "cc1_output_volts", "cc2_output_volts", "cc3_output_volts", "cc4_output_volts", "cc5_output_volts", "cc1_input_watts", "cc2_input_watts", "cc3_input_watts", "cc4_input_watts", "cc5_input_watts"];

var rrdflot_defaultsinfrequent= { use_checked_DSs:true, checked_DSs:['kwh_today'] }
var dsinfrequent=
	{ kwh_today: { label: 'kWH today', color: "#35c649" }
	, cc1_kwh_today: { label: 'cc1 kWH today', color: "#CC66FF" }
	, cc1_kwh_total: { label: 'cc1 kWH total', color: "#23bc38" }
	, cc1_temp_celsius: { label: 'cc1 temp (C)', color: "#32c3ef" }
	, cc1_status_code: { label: 'cc1 status code', color: "#000000" }
	, cc2_kwh_today: { label: 'cc2 kWH today', color: "#6600FF" }
	, cc2_kwh_total: { label: 'cc2 kWH total', color: "#23bc38" }
	, cc2_temp_celsius: { label: 'cc2 temp (C)', color: "#32c3ef" }
	, cc2_status_code: { label: 'cc2 status code', color: "#000000" }
	, cc3_kwh_today: { label: 'cc3 kWH today', color: "#0099FF" }
	, cc3_kwh_total: { label: 'cc3 kWH total', color: "#23bc38" }
	, cc3_temp_celsius: { label: 'cc3 temp (C)', color: "#32c3ef" }
	, cc3_status_code: { label: 'cc3 status code', color: "#000000" }
	, cc4_kwh_today: { label: 'cc4 kWH today', color: "#00CCCC" }
	, cc4_kwh_total: { label: 'cc4 kWH total', color: "#23bc38" }
	, cc4_temp_celsius: { label: 'cc4 temp (C)', color: "#32c3ef" }
	, cc4_status_code: { label: 'cc4 status code', color: "#000000" }
	, cc5_kwh_today: { label: 'cc5 kWH today', color: "#CC00CC" }
	, cc5_kwh_total: { label: 'cc5 kWH total', color: "#23bc38" }
	, cc5_temp_celsius: { label: 'cc5 temp (C)', color: "#32c3ef" }
	, cc5_status_code: { label: 'cc5 status code', color: "#000000" }
	, trimetric_cc_status: { label: 'trimetric cc status code', color: "#111111" }
	, max_battery_volts: { label: 'max battery voltage today' }
	, min_battery_volts: { label: 'min battery voltage today' }
	, porch_temp_celsius: { label: 'porch temp (C)', color: "#ce2200" }
	, slab_temp: { label: 'house floor temp (C)', color: "#938e86" }
	, hall_temp_celsius: { label: 'house temp (C)', color: "#895200" }
	, fridge_temp_celsius: { label: 'fridge main temp (C)', color: "#177dea" }
	, fridge_hours_today: { label: 'fridge hours running today', color: "#961396" }
	, fridge_run_duration: { label: 'fridge run durations (minutes)', color: "#9e0716" }
	, fridge_aux_temp: { label: 'fridge water temp (C)', color: "#0c0093" }
	, spare_probe_temp: { label: 'spare probe temp (C)', color: "#008492" }
	, cc1_battery_percent: { label: 'cc1 battery percent estimate' }
	, battery_percent: { label: 'battery percent', color: "#000000" }
	, car_soc: { label: 'car state of charge' }
	, sensor_failures: { label: 'sensor failures' }
	};
var ds_op_infrequent=[new kwh_today(),'cc1_kwh_today','cc2_kwh_today','cc3_kwh_today','cc4_kwh_today','cc5_kwh_today','cc1_kwh_total','cc2_kwh_total','cc3_kwh_total','cc4_kwh_total','cc5_kwh_total','cc1_temp_celsius','cc2_temp_celsius','cc3_temp_celsius','cc4_temp_celsius','cc5_temp_celsius','cc1_status_code','cc2_status_code','cc3_status_code','cc4_status_code','cc5_status_code','max_battery_volts','min_battery_volts','trimetric_cc_status','porch_temp_celsius','slab_temp','hall_temp_celsius','fridge_temp_celsius','fridge_hours_today','fridge_run_duration','fridge_aux_temp','spare_probe_temp','cc1_battery_percent','battery_percent','car_soc','sensor_failures'];
