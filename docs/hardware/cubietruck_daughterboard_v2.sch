EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text Label 10600 7650 0    50   ~ 0
2
Wire Wire Line
	3550 5200 3550 5100
Connection ~ 3550 5200
Wire Wire Line
	3550 5100 4350 5100
Wire Wire Line
	5950 6150 6300 6150
Connection ~ 5950 6150
Wire Wire Line
	5950 6150 5950 6100
Wire Wire Line
	4200 5300 4200 6150
Wire Wire Line
	4200 6150 5950 6150
$Comp
L cubietruck_daughterboard-rescue:R-cubietruck_daughterboard-rescue R?
U 1 1 5E8CC8E6
P 5950 5950
F 0 "R?" V 6030 5950 50  0000 C CNN
F 1 "10K" V 5950 5950 50  0000 C CNN
F 2 "" V 5880 5950 50  0001 C CNN
F 3 "" H 5950 5950 50  0001 C CNN
	1    5950 5950
	-1   0    0    1   
$EndComp
Connection ~ 6300 5000
Wire Wire Line
	6300 5000 6400 5000
Wire Wire Line
	6300 4950 6300 5000
NoConn ~ 6300 5800
Wire Wire Line
	6300 4600 6300 4650
$Comp
L cubietruck_daughterboard-rescue:R-cubietruck_daughterboard-rescue R?
U 1 1 5F11F7F9
P 6300 4800
F 0 "R?" V 6380 4800 50  0000 C CNN
F 1 "10K" V 6300 4800 50  0000 C CNN
F 2 "" V 6230 4800 50  0001 C CNN
F 3 "" H 6300 4800 50  0001 C CNN
	1    6300 4800
	-1   0    0    1   
$EndComp
NoConn ~ 6300 5400
Connection ~ 6300 4600
Wire Wire Line
	6300 4600 6300 4450
Wire Wire Line
	6300 4600 6250 4600
Wire Wire Line
	5650 4600 5950 4600
$Comp
L cubietruck_daughterboard-rescue:4N25-cubietruck_daughterboard-rescue PS
U 1 1 5E67977D
P 6750 5800
F 0 "PS" H 6550 6000 50  0000 L CNN
F 1 "4N35" H 6750 6000 50  0000 L CNN
F 2 "Housings_DIP:DIP-6_W7.62mm" H 6550 5600 50  0001 L CIN
F 3 "" H 6750 5800 50  0001 L CNN
	1    6750 5800
	-1   0    0    -1  
$EndComp
$Comp
L power:Earth #PWR0111
U 1 1 5E6CED10
P 6400 5200
F 0 "#PWR0111" H 6400 4950 50  0001 C CNN
F 1 "Earth" H 6400 5050 50  0001 C CNN
F 2 "" H 6400 5200 50  0001 C CNN
F 3 "~" H 6400 5200 50  0001 C CNN
	1    6400 5200
	-1   0    0    -1  
$EndComp
Wire Wire Line
	6400 5200 6450 5200
$Comp
L power:Earth #PWR0112
U 1 1 5E721E28
P 6450 5900
F 0 "#PWR0112" H 6450 5650 50  0001 C CNN
F 1 "Earth" H 6450 5750 50  0001 C CNN
F 2 "" H 6450 5900 50  0001 C CNN
F 3 "~" H 6450 5900 50  0001 C CNN
	1    6450 5900
	-1   0    0    -1  
$EndComp
$Comp
L cubietruck_daughterboard-rescue:4N25-cubietruck_daughterboard-rescue PT
U 1 1 5E68B994
P 6750 5300
F 0 "PT" H 6550 5500 50  0000 L CNN
F 1 "4N35" H 6750 5500 50  0000 L CNN
F 2 "Housings_DIP:DIP-6_W7.62mm" H 6550 5100 50  0001 L CIN
F 3 "" H 6750 5300 50  0001 L CNN
	1    6750 5300
	1    0    0    1   
$EndComp
Wire Wire Line
	7150 5700 7050 5700
Wire Wire Line
	7150 5300 7150 5350
Wire Wire Line
	7150 5650 7150 5700
$Comp
L cubietruck_daughterboard-rescue:R-cubietruck_daughterboard-rescue R?
U 1 1 5E69F64A
P 7150 5500
F 0 "R?" V 7230 5500 50  0000 C CNN
F 1 "2.2K" V 7150 5500 50  0000 C CNN
F 2 "" V 7080 5500 50  0001 C CNN
F 3 "" H 7150 5500 50  0001 C CNN
	1    7150 5500
	-1   0    0    -1  
$EndComp
Connection ~ 5950 5800
Wire Wire Line
	6250 5000 6250 5400
Text Notes 9900 5950 0    60   ~ 0
inverter status (unused)
$Comp
L cubietruck_daughterboard-rescue:Screw_Terminal_01x04-cubietruck_daughterboard-rescue J?
U 1 1 5E59143F
P 8650 5250
F 0 "J?" H 8650 5450 50  0000 C CNN
F 1 "spring pump" H 8650 4950 50  0000 C CNN
F 2 "" H 8650 5250 50  0001 C CNN
F 3 "" H 8650 5250 50  0001 C CNN
	1    8650 5250
	1    0    0    -1  
$EndComp
Text Notes 5200 4650 0    60   ~ 0
PI20
$Comp
L cubietruck_daughterboard-rescue:R-cubietruck_daughterboard-rescue R?
U 1 1 5EDA5F3E
P 6100 4600
F 0 "R?" H 6180 4600 50  0000 C CNN
F 1 "1K" V 6100 4600 50  0000 C CNN
F 2 "" V 6030 4600 50  0001 C CNN
F 3 "" H 6100 4600 50  0001 C CNN
	1    6100 4600
	0    1    1    0   
$EndComp
NoConn ~ 6000 4100
NoConn ~ 6000 4200
Text Notes 5200 4550 0    60   ~ 0
PI21
Text Notes 1350 4150 0    60   ~ 0
relayF CTRL
Text Notes 1350 4050 0    60   ~ 0
relayE CTRL
Text Notes 1350 3950 0    60   ~ 0
relayD CTRL
Text Notes 1350 4700 0    60   ~ 0
relayC CTRL
Text Notes 4600 5050 0    60   ~ 0
PC22
Text Notes 4600 4450 0    60   ~ 0
PB3
Text Notes 4600 4350 0    60   ~ 0
PB4
Wire Wire Line
	2150 5200 3550 5200
Wire Wire Line
	2150 5300 3900 5300
Text Notes 1850 5250 2    60   ~ 0
onewire signal
Text Notes 4600 4550 0    60   ~ 0
PI15
Text Notes 4600 4650 0    60   ~ 0
PI14
Text Notes 4600 5150 0    60   ~ 0
PC21
Text Notes 4600 5350 0    60   ~ 0
VCC 3.3V
Text Notes 5150 5150 0    60   ~ 0
PC19
Text Notes 5150 5050 0    60   ~ 0
PC20
Text Notes 5200 5350 0    60   ~ 0
GND
Text Notes 5050 3950 0    60   ~ 0
VCC 5V
Text Notes 5150 4250 0    60   ~ 0
PB18
Text Notes 5150 4150 0    60   ~ 0
PB19
Wire Wire Line
	5650 5000 5950 5000
Wire Wire Line
	5950 5650 5950 5800
Wire Wire Line
	5950 5100 5650 5100
Wire Wire Line
	5950 5350 5950 5100
$Comp
L cubietruck_daughterboard-rescue:R-cubietruck_daughterboard-rescue R?
U 1 1 5E928350
P 5950 5500
F 0 "R?" V 6030 5500 50  0000 C CNN
F 1 "1K" V 5950 5500 50  0000 C CNN
F 2 "" V 5880 5500 50  0001 C CNN
F 3 "" H 5950 5500 50  0001 C CNN
	1    5950 5500
	1    0    0    -1  
$EndComp
$Comp
L cubietruck_daughterboard-rescue:R-cubietruck_daughterboard-rescue R?
U 1 1 5E6F4A52
P 6100 5000
F 0 "R?" H 6180 5000 50  0000 C CNN
F 1 "1K" V 6100 5000 50  0000 C CNN
F 2 "" V 6030 5000 50  0001 C CNN
F 3 "" H 6100 5000 50  0001 C CNN
	1    6100 5000
	0    1    1    0   
$EndComp
Wire Wire Line
	3850 5200 3900 5200
Connection ~ 4200 5300
Wire Wire Line
	4200 5300 4350 5300
Text Notes 1650 5450 0    60   ~ 0
GND
$Comp
L cubietruck_daughterboard-rescue:Conn_01x15-cubietruck_daughterboard-rescue J?
U 1 1 5E534314
P 4550 4600
F 0 "J?" H 4550 5400 50  0000 C CNN
F 1 "left row" H 4550 3800 50  0000 C CNN
F 2 "" H 4550 4600 50  0001 C CNN
F 3 "" H 4550 4600 50  0001 C CNN
	1    4550 4600
	1    0    0    -1  
$EndComp
Text Notes 4650 3800 0    60   ~ 0
Cubietruck CN8
$Comp
L cubietruck_daughterboard-rescue:Conn_01x15-cubietruck_daughterboard-rescue J?
U 1 1 5E53430E
P 5450 4600
F 0 "J?" H 5450 5400 50  0000 C CNN
F 1 "right row" H 5450 3800 50  0000 C CNN
F 2 "" H 5450 4600 50  0001 C CNN
F 3 "" H 5450 4600 50  0001 C CNN
	1    5450 4600
	-1   0    0    -1  
$EndComp
$Comp
L cubietruck_daughterboard-rescue:Screw_Terminal_01x04-cubietruck_daughterboard-rescue J?
U 1 1 5E58CE8A
P 8650 3850
F 0 "J?" H 8650 4050 50  0000 C CNN
F 1 "i2c" H 8650 3550 50  0000 C CNN
F 2 "" H 8650 3850 50  0001 C CNN
F 3 "" H 8650 3850 50  0001 C CNN
	1    8650 3850
	1    0    0    -1  
$EndComp
$Comp
L cubietruck_daughterboard-rescue:Screw_Terminal_01x04-cubietruck_daughterboard-rescue J?
U 1 1 5E5989E7
P 8650 4550
F 0 "J?" H 8650 4750 50  0000 C CNN
F 1 "switches" H 8650 4250 50  0000 C CNN
F 2 "" H 8650 4550 50  0001 C CNN
F 3 "" H 8650 4550 50  0001 C CNN
	1    8650 4550
	1    0    0    -1  
$EndComp
Text Notes 8750 3900 0    60   ~ 0
VCC 3.3V
Wire Wire Line
	8450 4050 8450 4100
NoConn ~ 7250 4100
Text Notes 8750 4000 0    60   ~ 0
SCL
Text Notes 8750 4100 0    60   ~ 0
SDA
Text Notes 1350 4600 0    60   ~ 0
relayA CTRL
Text Notes 1350 4500 0    60   ~ 0
relayB CTRL
$Comp
L cubietruck_daughterboard-rescue:R-cubietruck_daughterboard-rescue R?
U 1 1 5E5F35DB
P 3700 5200
F 0 "R?" V 3780 5200 50  0000 C CNN
F 1 "4.7K" V 3700 5200 50  0000 C CNN
F 2 "" V 3630 5200 50  0001 C CNN
F 3 "" H 3700 5200 50  0001 C CNN
	1    3700 5200
	0    -1   1    0   
$EndComp
Text Notes 1850 5350 2    60   ~ 0
VCC 3.3V
$Comp
L power:Earth #PWR0116
U 1 1 5E5B5910
P 2150 5400
F 0 "#PWR0116" H 2150 5150 50  0001 C CNN
F 1 "Earth" H 2150 5250 50  0001 C CNN
F 2 "" H 2150 5400 50  0001 C CNN
F 3 "~" H 2150 5400 50  0001 C CNN
	1    2150 5400
	-1   0    0    -1  
$EndComp
$Comp
L cubietruck_daughterboard-rescue:Screw_Terminal_01x04-cubietruck_daughterboard-rescue J?
U 1 1 5E5A1FC2
P 1950 3900
F 0 "J?" H 1950 4100 50  0000 C CNN
F 1 "relays" H 1950 3600 50  0000 C CNN
F 2 "" H 1950 3900 50  0001 C CNN
F 3 "" H 1950 3900 50  0001 C CNN
	1    1950 3900
	-1   0    0    -1  
$EndComp
$Comp
L cubietruck_daughterboard-rescue:Screw_Terminal_01x04-cubietruck_daughterboard-rescue J?
U 1 1 5E57AD65
P 1950 4550
F 0 "J?" H 1950 4750 50  0000 C CNN
F 1 "relays" H 1950 4250 50  0000 C CNN
F 2 "" H 1950 4550 50  0001 C CNN
F 3 "" H 1950 4550 50  0001 C CNN
	1    1950 4550
	-1   0    0    -1  
$EndComp
$Comp
L cubietruck_daughterboard-rescue:R-cubietruck_daughterboard-rescue R?
U 1 1 5F539950
P 3600 4400
F 0 "R?" H 3680 4400 50  0000 C CNN
F 1 "220" V 3600 4400 50  0000 C CNN
F 2 "" V 3530 4400 50  0001 C CNN
F 3 "" H 3600 4400 50  0001 C CNN
	1    3600 4400
	0    1    1    0   
$EndComp
Wire Wire Line
	2650 4400 2850 4400
$Comp
L cubietruck_daughterboard-rescue:R-cubietruck_daughterboard-rescue R?
U 1 1 5F54B7FB
P 3600 3950
F 0 "R?" H 3680 3950 50  0000 C CNN
F 1 "220" V 3600 3950 50  0000 C CNN
F 2 "" V 3530 3950 50  0001 C CNN
F 3 "" H 3600 3950 50  0001 C CNN
	1    3600 3950
	0    1    1    0   
$EndComp
$Comp
L cubietruck_daughterboard-rescue:R-cubietruck_daughterboard-rescue R?
U 1 1 5F63CC43
P 3600 3500
F 0 "R?" H 3680 3500 50  0000 C CNN
F 1 "220" V 3600 3500 50  0000 C CNN
F 2 "" V 3530 3500 50  0001 C CNN
F 3 "" H 3600 3500 50  0001 C CNN
	1    3600 3500
	0    1    1    0   
$EndComp
$Comp
L cubietruck_daughterboard-rescue:R-cubietruck_daughterboard-rescue R?
U 1 1 5F63CC58
P 3600 3050
F 0 "R?" H 3680 3050 50  0000 C CNN
F 1 "220" V 3600 3050 50  0000 C CNN
F 2 "" V 3530 3050 50  0001 C CNN
F 3 "" H 3600 3050 50  0001 C CNN
	1    3600 3050
	0    1    1    0   
$EndComp
$Comp
L cubietruck_daughterboard-rescue:R-cubietruck_daughterboard-rescue R?
U 1 1 5F63CC6D
P 3600 2600
F 0 "R?" H 3680 2600 50  0000 C CNN
F 1 "220" V 3600 2600 50  0000 C CNN
F 2 "" V 3530 2600 50  0001 C CNN
F 3 "" H 3600 2600 50  0001 C CNN
	1    3600 2600
	0    1    1    0   
$EndComp
$Comp
L power:Earth #PWR0117
U 1 1 5F665E2E
P 3450 2400
F 0 "#PWR0117" H 3450 2150 50  0001 C CNN
F 1 "Earth" H 3450 2250 50  0001 C CNN
F 2 "" H 3450 2400 50  0001 C CNN
F 3 "~" H 3450 2400 50  0001 C CNN
	1    3450 2400
	1    0    0    -1  
$EndComp
$Comp
L power:Earth #PWR0118
U 1 1 5F666DBA
P 3450 2850
F 0 "#PWR0118" H 3450 2600 50  0001 C CNN
F 1 "Earth" H 3450 2700 50  0001 C CNN
F 2 "" H 3450 2850 50  0001 C CNN
F 3 "~" H 3450 2850 50  0001 C CNN
	1    3450 2850
	1    0    0    -1  
$EndComp
$Comp
L power:Earth #PWR0119
U 1 1 5F66BBBE
P 3450 3300
F 0 "#PWR0119" H 3450 3050 50  0001 C CNN
F 1 "Earth" H 3450 3150 50  0001 C CNN
F 2 "" H 3450 3300 50  0001 C CNN
F 3 "~" H 3450 3300 50  0001 C CNN
	1    3450 3300
	1    0    0    -1  
$EndComp
$Comp
L power:Earth #PWR0120
U 1 1 5F66D1A9
P 3450 3750
F 0 "#PWR0120" H 3450 3500 50  0001 C CNN
F 1 "Earth" H 3450 3600 50  0001 C CNN
F 2 "" H 3450 3750 50  0001 C CNN
F 3 "~" H 3450 3750 50  0001 C CNN
	1    3450 3750
	1    0    0    -1  
$EndComp
$Comp
L power:Earth #PWR0121
U 1 1 5F66DA41
P 3450 4200
F 0 "#PWR0121" H 3450 3950 50  0001 C CNN
F 1 "Earth" H 3450 4050 50  0001 C CNN
F 2 "" H 3450 4200 50  0001 C CNN
F 3 "~" H 3450 4200 50  0001 C CNN
	1    3450 4200
	1    0    0    -1  
$EndComp
Wire Wire Line
	2650 3950 2850 3950
Wire Wire Line
	2650 3500 2850 3500
Wire Wire Line
	2650 3050 2850 3050
Wire Wire Line
	2850 2600 2650 2600
$Comp
L cubietruck_daughterboard-rescue:Screw_Terminal_01x04-cubietruck_daughterboard-rescue J?
U 1 1 5E58347B
P 1950 5200
F 0 "J?" H 1950 5400 50  0000 C CNN
F 1 "Onewire" H 1950 4900 50  0000 C CNN
F 2 "" H 1950 5200 50  0001 C CNN
F 3 "" H 1950 5200 50  0001 C CNN
	1    1950 5200
	-1   0    0    -1  
$EndComp
Wire Wire Line
	3750 5000 4350 5000
Wire Wire Line
	3850 4600 3850 4400
Wire Wire Line
	3850 4600 4350 4600
Wire Wire Line
	3950 4500 3950 3950
Wire Wire Line
	3950 3950 3750 3950
Wire Wire Line
	3950 4500 4350 4500
Wire Wire Line
	4350 4400 4050 4400
Wire Wire Line
	4050 4400 4050 3500
Wire Wire Line
	4050 3500 3750 3500
Wire Wire Line
	4350 4300 4150 4300
Wire Wire Line
	4150 4300 4150 3050
Wire Wire Line
	4150 3050 3750 3050
Wire Wire Line
	5650 4500 6000 4500
Wire Wire Line
	5950 5800 6450 5800
Wire Wire Line
	6250 5400 6450 5400
Wire Wire Line
	6300 5000 6300 6150
Wire Wire Line
	7050 5200 7300 5200
Wire Wire Line
	5650 4200 7250 4200
Wire Wire Line
	2150 4450 2500 4450
Wire Wire Line
	2500 4450 2500 3750
Wire Wire Line
	2150 4550 2550 4550
Wire Wire Line
	3750 5000 3750 4850
$Comp
L power:Earth #PWR0122
U 1 1 5F66F289
P 3450 4650
F 0 "#PWR0122" H 3450 4400 50  0001 C CNN
F 1 "Earth" H 3450 4500 50  0001 C CNN
F 2 "" H 3450 4650 50  0001 C CNN
F 3 "~" H 3450 4650 50  0001 C CNN
	1    3450 4650
	1    0    0    -1  
$EndComp
Wire Wire Line
	2650 4850 2850 4850
$Comp
L cubietruck_daughterboard-rescue:R-cubietruck_daughterboard-rescue R?
U 1 1 5F5159A3
P 3600 4850
F 0 "R?" H 3680 4850 50  0000 C CNN
F 1 "220" V 3600 4850 50  0000 C CNN
F 2 "" V 3530 4850 50  0001 C CNN
F 3 "" H 3600 4850 50  0001 C CNN
	1    3600 4850
	0    1    1    0   
$EndComp
Wire Wire Line
	2150 4650 2850 4650
NoConn ~ 2650 4650
Wire Wire Line
	2550 4200 2550 4550
Wire Wire Line
	3750 4400 3850 4400
NoConn ~ 2650 4200
NoConn ~ 2650 3750
NoConn ~ 2650 3300
NoConn ~ 2650 2850
Wire Wire Line
	2400 2850 2850 2850
Wire Wire Line
	2450 3300 2850 3300
Wire Wire Line
	2500 3750 2850 3750
Wire Wire Line
	2550 4200 2850 4200
$Comp
L power:Earth #PWR?
U 1 1 5FE51A11
P 4250 4700
F 0 "#PWR?" H 4250 4450 50  0001 C CNN
F 1 "Earth" H 4250 4550 50  0001 C CNN
F 2 "" H 4250 4700 50  0001 C CNN
F 3 "~" H 4250 4700 50  0001 C CNN
	1    4250 4700
	1    0    0    -1  
$EndComp
$Comp
L power:Earth #PWR?
U 1 1 5FE5B356
P 5750 4700
F 0 "#PWR?" H 5750 4450 50  0001 C CNN
F 1 "Earth" H 5750 4550 50  0001 C CNN
F 2 "" H 5750 4700 50  0001 C CNN
F 3 "~" H 5750 4700 50  0001 C CNN
	1    5750 4700
	1    0    0    -1  
$EndComp
Wire Wire Line
	4250 4700 4350 4700
Wire Wire Line
	5650 4700 5750 4700
Wire Wire Line
	3900 5200 3900 5300
Connection ~ 3900 5300
Wire Wire Line
	3900 5300 4200 5300
Wire Wire Line
	2550 5700 2650 5700
Wire Wire Line
	2650 5700 2750 5700
Connection ~ 2650 5700
$Comp
L Relay_SolidState:TLP3123 U?
U 1 1 5E553B8D
P 3150 2500
F 0 "U?" H 3150 2183 50  0000 C CNN
F 1 "AQY211EH" H 3150 2274 50  0000 C CNN
F 2 "Package_SO:SO-4_4.4x3.9mm_P2.54mm" H 2950 2300 50  0001 L CIN
F 3 "https://toshiba.semicon-storage.com/info/docget.jsp?did=10047&prodName=TLP3123" H 3150 2500 50  0001 L CNN
	1    3150 2500
	-1   0    0    1   
$EndComp
$Comp
L Relay_SolidState:TLP3123 U?
U 1 1 5E5956A1
P 3150 2950
F 0 "U?" H 3150 2633 50  0000 C CNN
F 1 "AQY211EH" H 3150 2724 50  0000 C CNN
F 2 "Package_SO:SO-4_4.4x3.9mm_P2.54mm" H 2950 2750 50  0001 L CIN
F 3 "https://toshiba.semicon-storage.com/info/docget.jsp?did=10047&prodName=TLP3123" H 3150 2950 50  0001 L CNN
	1    3150 2950
	-1   0    0    1   
$EndComp
$Comp
L Relay_SolidState:TLP3123 U?
U 1 1 5E5964EC
P 3150 3400
F 0 "U?" H 3150 3083 50  0000 C CNN
F 1 "AQY211EH" H 3150 3174 50  0000 C CNN
F 2 "Package_SO:SO-4_4.4x3.9mm_P2.54mm" H 2950 3200 50  0001 L CIN
F 3 "https://toshiba.semicon-storage.com/info/docget.jsp?did=10047&prodName=TLP3123" H 3150 3400 50  0001 L CNN
	1    3150 3400
	-1   0    0    1   
$EndComp
$Comp
L Relay_SolidState:TLP3123 U?
U 1 1 5E597143
P 3150 3850
F 0 "U?" H 3150 3533 50  0000 C CNN
F 1 "AQY211EH" H 3150 3624 50  0000 C CNN
F 2 "Package_SO:SO-4_4.4x3.9mm_P2.54mm" H 2950 3650 50  0001 L CIN
F 3 "https://toshiba.semicon-storage.com/info/docget.jsp?did=10047&prodName=TLP3123" H 3150 3850 50  0001 L CNN
	1    3150 3850
	-1   0    0    1   
$EndComp
$Comp
L Relay_SolidState:TLP3123 U?
U 1 1 5E597DD1
P 3150 4300
F 0 "U?" H 3150 3983 50  0000 C CNN
F 1 "AQY211EH" H 3150 4074 50  0000 C CNN
F 2 "Package_SO:SO-4_4.4x3.9mm_P2.54mm" H 2950 4100 50  0001 L CIN
F 3 "https://toshiba.semicon-storage.com/info/docget.jsp?did=10047&prodName=TLP3123" H 3150 4300 50  0001 L CNN
	1    3150 4300
	-1   0    0    1   
$EndComp
Connection ~ 2650 3050
Wire Wire Line
	2650 2600 2650 3050
Connection ~ 2650 3500
Wire Wire Line
	2650 3050 2650 3500
Connection ~ 2650 3950
Wire Wire Line
	2650 3500 2650 3950
Connection ~ 2650 4400
Wire Wire Line
	2650 3950 2650 4400
$Comp
L Relay_SolidState:TLP3123 U?
U 1 1 5E5D3654
P 3150 4750
F 0 "U?" H 3150 4433 50  0000 C CNN
F 1 "AQY211EH" H 3150 4524 50  0000 C CNN
F 2 "Package_SO:SO-4_4.4x3.9mm_P2.54mm" H 2950 4550 50  0001 L CIN
F 3 "https://toshiba.semicon-storage.com/info/docget.jsp?did=10047&prodName=TLP3123" H 3150 4750 50  0001 L CNN
	1    3150 4750
	-1   0    0    1   
$EndComp
Wire Wire Line
	2650 4400 2650 4850
Text Notes 8750 3800 0    60   ~ 0
GND
Text Notes 8750 4500 0    60   ~ 0
kitchen counter switch
$Comp
L power:Earth #PWR0114
U 1 1 5E61BF67
P 8450 3750
F 0 "#PWR0114" H 8450 3500 50  0001 C CNN
F 1 "Earth" H 8450 3600 50  0001 C CNN
F 2 "" H 8450 3750 50  0001 C CNN
F 3 "~" H 8450 3750 50  0001 C CNN
	1    8450 3750
	1    0    0    -1  
$EndComp
$Comp
L Connector:RJ9 inverter
U 1 1 5E6F55E8
P 9550 5800
F 0 "inverter" H 9850 5500 50  0000 R CNN
F 1 "RJ9" H 9450 5500 50  0000 R CNN
F 2 "" V 9550 5850 50  0001 C CNN
F 3 "~" V 9550 5850 50  0001 C CNN
	1    9550 5800
	-1   0    0    1   
$EndComp
Wire Wire Line
	8400 6050 8400 5800
Wire Wire Line
	8600 6500 8600 6250
Wire Wire Line
	7050 5300 7150 5300
Connection ~ 7150 5300
Wire Wire Line
	7150 5300 7250 5300
Wire Wire Line
	8400 6250 8400 6300
Wire Wire Line
	2750 5700 2750 5750
Wire Wire Line
	2650 5700 2650 5750
Wire Wire Line
	2550 5750 2550 5700
Wire Wire Line
	2650 5700 2650 4850
Connection ~ 2650 4850
NoConn ~ 2650 5200
NoConn ~ 2650 5300
Wire Wire Line
	2350 2400 2850 2400
Wire Wire Line
	3750 2600 6000 2600
Text Notes 5200 4750 0    60   ~ 0
GND
Text Notes 4600 4750 0    60   ~ 0
GND
$Comp
L Connector_Generic:Conn_01x03 J?
U 1 1 5EA48D0F
P 2650 5950
F 0 "J?" V 2522 6130 50  0000 L CNN
F 1 "house power 24V 6A max +terminal" V 2750 5300 50  0000 L CNN
F 2 "" H 2650 5950 50  0001 C CNN
F 3 "~" H 2650 5950 50  0001 C CNN
	1    2650 5950
	0    1    1    0   
$EndComp
$Comp
L Connector_Generic:Conn_01x04 J?
U 1 1 5EA64C53
P 8500 6050
F 0 "J?" V 8464 5762 50  0000 R CNN
F 1 "inverter" V 8373 5762 50  0000 R CNN
F 2 "" H 8500 6050 50  0001 C CNN
F 3 "~" H 8500 6050 50  0001 C CNN
	1    8500 6050
	0    -1   -1   0   
$EndComp
Wire Wire Line
	7300 6300 8400 6300
Wire Wire Line
	7250 6400 8500 6400
Wire Wire Line
	8500 6400 8500 6250
Wire Wire Line
	8600 5900 9150 5900
Wire Wire Line
	8500 5700 9150 5700
NoConn ~ 6400 4100
NoConn ~ 6400 4200
NoConn ~ 6400 4450
$Comp
L power:Earth #PWR?
U 1 1 5E7576C7
P 8450 4750
F 0 "#PWR?" H 8450 4500 50  0001 C CNN
F 1 "Earth" H 8450 4600 50  0001 C CNN
F 2 "" H 8450 4750 50  0001 C CNN
F 3 "~" H 8450 4750 50  0001 C CNN
	1    8450 4750
	1    0    0    -1  
$EndComp
Text Notes 8750 4800 0    60   ~ 0
switch common (not house ground)
$Comp
L cubietruck_daughterboard-rescue:4N25-cubietruck_daughterboard-rescue PT?
U 1 1 5EC9E4C5
P 7150 4850
F 0 "PT?" H 6950 5050 50  0000 L CNN
F 1 "4N35" H 7150 5050 50  0000 L CNN
F 2 "Housings_DIP:DIP-6_W7.62mm" H 6950 4650 50  0001 L CIN
F 3 "" H 7150 4850 50  0001 L CNN
	1    7150 4850
	1    0    0    1   
$EndComp
$Comp
L power:Earth #PWR?
U 1 1 5ECD3B0A
P 6850 4750
F 0 "#PWR?" H 6850 4500 50  0001 C CNN
F 1 "Earth" H 6850 4600 50  0001 C CNN
F 2 "" H 6850 4750 50  0001 C CNN
F 3 "~" H 6850 4750 50  0001 C CNN
	1    6850 4750
	-1   0    0    -1  
$EndComp
$Comp
L cubietruck_daughterboard-rescue:R-cubietruck_daughterboard-rescue R?
U 1 1 5ECD4896
P 6600 4750
F 0 "R?" H 6680 4750 50  0000 C CNN
F 1 "1K" V 6600 4750 50  0000 C CNN
F 2 "" V 6530 4750 50  0001 C CNN
F 3 "" H 6600 4750 50  0001 C CNN
	1    6600 4750
	1    0    0    -1  
$EndComp
Wire Wire Line
	6600 4400 5650 4400
NoConn ~ 6400 4400
NoConn ~ 6600 4450
NoConn ~ 6000 4400
Text Notes 5250 4450 0    60   ~ 0
PI3
Text Notes 8750 5350 0    60   ~ 0
spring pump led positive (w/300 ohm resistor)
Wire Wire Line
	6400 3850 8450 3850
Wire Wire Line
	7250 3950 8450 3950
Wire Wire Line
	6000 2600 6000 4500
Wire Wire Line
	7250 3950 7250 4200
Wire Wire Line
	5650 4100 8450 4100
Wire Wire Line
	6400 3850 6400 5000
$Comp
L cubietruck_daughterboard-rescue:4N25-cubietruck_daughterboard-rescue PS?
U 1 1 5EDAFAB8
P 7900 5600
F 0 "PS?" H 7700 5800 50  0000 L CNN
F 1 "4N35" H 7900 5800 50  0000 L CNN
F 2 "Housings_DIP:DIP-6_W7.62mm" H 7700 5400 50  0001 L CIN
F 3 "" H 7900 5600 50  0001 L CNN
	1    7900 5600
	-1   0    0    -1  
$EndComp
Text Notes 5200 4350 0    60   ~ 0
PB2
$Comp
L cubietruck_daughterboard-rescue:R-cubietruck_daughterboard-rescue R?
U 1 1 5EDD542A
P 6900 4300
F 0 "R?" H 6980 4300 50  0000 C CNN
F 1 "1K" V 6900 4300 50  0000 C CNN
F 2 "" V 6830 4300 50  0001 C CNN
F 3 "" H 6900 4300 50  0001 C CNN
	1    6900 4300
	0    1    1    0   
$EndComp
Wire Wire Line
	5650 4300 6750 4300
NoConn ~ 6000 4300
NoConn ~ 6400 4300
Wire Wire Line
	6300 4450 8450 4450
Wire Wire Line
	6600 4400 6600 4600
Wire Wire Line
	6600 4900 6600 4950
Wire Wire Line
	6600 4950 6850 4950
Wire Wire Line
	8200 5350 8200 5500
Wire Wire Line
	8200 5350 8450 5350
Wire Wire Line
	8300 5700 8200 5700
$Comp
L power:Earth #PWR?
U 1 1 5EF05D16
P 7600 5700
F 0 "#PWR?" H 7600 5450 50  0001 C CNN
F 1 "Earth" H 7600 5550 50  0001 C CNN
F 2 "" H 7600 5700 50  0001 C CNN
F 3 "~" H 7600 5700 50  0001 C CNN
	1    7600 5700
	-1   0    0    -1  
$EndComp
Wire Wire Line
	7850 4750 7850 5150
Wire Wire Line
	7450 4750 7850 4750
Wire Wire Line
	7750 4850 7750 5250
Wire Wire Line
	7450 4850 7750 4850
Wire Wire Line
	7050 4300 7500 4300
Wire Wire Line
	7500 4300 7500 5600
Wire Wire Line
	7500 5600 7600 5600
$Comp
L cubietruck_daughterboard-rescue:R-cubietruck_daughterboard-rescue R?
U 1 1 5EF20CF1
P 7500 5900
F 0 "R?" V 7580 5900 50  0000 C CNN
F 1 "10K" V 7500 5900 50  0000 C CNN
F 2 "" V 7430 5900 50  0001 C CNN
F 3 "" H 7500 5900 50  0001 C CNN
	1    7500 5900
	-1   0    0    1   
$EndComp
Wire Wire Line
	7500 5750 7500 5600
Connection ~ 7500 5600
Wire Wire Line
	7500 6050 7500 6150
Connection ~ 6300 6150
NoConn ~ 7050 6150
NoConn ~ 7250 6150
NoConn ~ 7300 6150
Wire Wire Line
	8600 5900 8600 6050
Wire Wire Line
	8400 5800 9150 5800
Wire Wire Line
	8500 5700 8500 6050
NoConn ~ 8500 5800
NoConn ~ 7500 4750
NoConn ~ 7500 4850
NoConn ~ 7500 4450
Text Notes 8750 5150 0    60   ~ 0
spring pump float switch negative
Text Notes 8750 5250 0    60   ~ 0
spring pump float switch positive
Wire Wire Line
	2450 4100 2150 4100
Wire Wire Line
	2450 3300 2450 4100
Wire Wire Line
	2400 4000 2150 4000
Wire Wire Line
	2400 2850 2400 4000
Wire Wire Line
	2350 3900 2150 3900
Wire Wire Line
	2350 2400 2350 3900
Wire Wire Line
	7300 5200 7300 6300
Wire Wire Line
	7250 5300 7250 6400
Wire Wire Line
	7500 6150 6300 6150
Text Notes 7050 7000 0    50   ~ 0
All GPIO input pins are pulled up, so 1=off
Wire Wire Line
	7050 6500 8600 6500
Wire Wire Line
	7050 6500 7050 5900
Text Notes 7050 6650 0    50   ~ 0
Note: Ground symbols are Cubietruck power supply negative, not house's ground.
Text Notes 900  3800 0    50   ~ 0
to relay coil positive
Wire Wire Line
	7750 5250 8450 5250
Text Notes 8750 5450 0    60   ~ 0
spring pump led negative
Wire Wire Line
	8450 5450 8300 5450
Wire Wire Line
	8300 5450 8300 5700
Wire Wire Line
	7850 5150 8450 5150
Text Notes 9900 5750 0    60   ~ 0
inverter control positive
Text Notes 9900 5850 0    60   ~ 0
inverter control negative
Text Notes 9100 6300 0    50   ~ 0
Note: Headers on top board for inverter\nare backwards, so connect 1 to 4, 2 to 3, etc
$EndSCHEMATC
