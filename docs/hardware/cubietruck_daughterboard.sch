EESchema Schematic File Version 4
LIBS:cubietruck_daughterboard-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L cubietruck_daughterboard-rescue:R-cubietruck_daughterboard-rescue R?
U 1 1 5ACA899D
P 4700 1850
F 0 "R?" H 4780 1850 50  0000 C CNN
F 1 "1K" V 4700 1850 50  0000 C CNN
F 2 "" V 4630 1850 50  0001 C CNN
F 3 "" H 4700 1850 50  0001 C CNN
	1    4700 1850
	0    1    1    0   
$EndComp
$Comp
L cubietruck_daughterboard-rescue:4N25-cubietruck_daughterboard-rescue U?
U 1 1 5ACA8ACD
P 4050 1750
F 0 "U?" H 3850 1950 50  0000 L CNN
F 1 "4N35" H 4050 1950 50  0000 L CNN
F 2 "Housings_DIP:DIP-6_W7.62mm" H 3850 1550 50  0001 L CIN
F 3 "" H 4050 1750 50  0001 L CNN
	1    4050 1750
	-1   0    0    1   
$EndComp
$Comp
L cubietruck_daughterboard-rescue:Conn_01x15-cubietruck_daughterboard-rescue J?
U 1 1 5ACA958B
P 5300 1450
F 0 "J?" H 5300 2250 50  0000 C CNN
F 1 "right row" H 5300 650 50  0000 C CNN
F 2 "" H 5300 1450 50  0001 C CNN
F 3 "" H 5300 1450 50  0001 C CNN
	1    5300 1450
	1    0    0    -1  
$EndComp
$Comp
L cubietruck_daughterboard-rescue:Conn_01x15-cubietruck_daughterboard-rescue J?
U 1 1 5ACA9647
P 5150 3250
F 0 "J?" H 5150 4050 50  0000 C CNN
F 1 "left row" H 5150 2450 50  0000 C CNN
F 2 "" H 5150 3250 50  0001 C CNN
F 3 "" H 5150 3250 50  0001 C CNN
	1    5150 3250
	1    0    0    -1  
$EndComp
Text Notes 4950 600  0    60   ~ 0
Cubietruck CN8
$Comp
L power:Earth #PWR?
U 1 1 5ACA9972
P 5000 2150
F 0 "#PWR?" H 5000 1900 50  0001 C CNN
F 1 "Earth" H 5000 2000 50  0001 C CNN
F 2 "" H 5000 2150 50  0001 C CNN
F 3 "" H 5000 2150 50  0001 C CNN
	1    5000 2150
	1    0    0    -1  
$EndComp
Text Notes 5400 1900 0    60   ~ 0
PC20 GPIO - to AIMS power button
Text Notes 5400 2000 0    60   ~ 0
PC19 GPIO - read AIMS power status (1=off)
$Comp
L cubietruck_daughterboard-rescue:4N25-cubietruck_daughterboard-rescue U?
U 1 1 5ACAABC7
P 3300 2500
F 0 "U?" H 3100 2700 50  0000 L CNN
F 1 "4N35" H 3300 2700 50  0000 L CNN
F 2 "Housings_DIP:DIP-6_W7.62mm" H 3100 2300 50  0001 L CIN
F 3 "" H 3300 2500 50  0001 L CNN
	1    3300 2500
	1    0    0    -1  
$EndComp
NoConn ~ 3550 1650
$Comp
L cubietruck_daughterboard-rescue:R-cubietruck_daughterboard-rescue R?
U 1 1 5ACABE5F
P 3000 2100
F 0 "R?" V 3080 2100 50  0000 C CNN
F 1 "2.2K" V 3000 2100 50  0000 C CNN
F 2 "" V 2930 2100 50  0001 C CNN
F 3 "" H 3000 2100 50  0001 C CNN
	1    3000 2100
	1    0    0    -1  
$EndComp
$Comp
L cubietruck_daughterboard-rescue:R-cubietruck_daughterboard-rescue R?
U 1 1 5ACAC479
P 4100 2500
F 0 "R?" V 4180 2500 50  0000 C CNN
F 1 "1K" V 4100 2500 50  0000 C CNN
F 2 "" V 4030 2500 50  0001 C CNN
F 3 "" H 4100 2500 50  0001 C CNN
	1    4100 2500
	0    1    1    0   
$EndComp
$Comp
L cubietruck_daughterboard-rescue:R-cubietruck_daughterboard-rescue R?
U 1 1 5ACAC60E
P 3750 2900
F 0 "R?" V 3830 2900 50  0000 C CNN
F 1 "10K" V 3750 2900 50  0000 C CNN
F 2 "" V 3680 2900 50  0001 C CNN
F 3 "" H 3750 2900 50  0001 C CNN
	1    3750 2900
	1    0    0    -1  
$EndComp
Text Notes 5250 4000 0    60   ~ 0
VCC 3.3V
$Comp
L cubietruck_daughterboard-rescue:R-cubietruck_daughterboard-rescue R?
U 1 1 5ACED482
P 4450 3850
F 0 "R?" V 4530 3850 50  0000 C CNN
F 1 "4.7K" V 4450 3850 50  0000 C CNN
F 2 "" V 4380 3850 50  0001 C CNN
F 3 "" H 4450 3850 50  0001 C CNN
	1    4450 3850
	0    1    1    0   
$EndComp
$Comp
L power:Earth #PWR?
U 1 1 5ACEFBDE
P 4450 1650
F 0 "#PWR?" H 4450 1400 50  0001 C CNN
F 1 "Earth" H 4450 1500 50  0001 C CNN
F 2 "" H 4450 1650 50  0001 C CNN
F 3 "" H 4450 1650 50  0001 C CNN
	1    4450 1650
	1    0    0    -1  
$EndComp
$Comp
L power:Earth #PWR?
U 1 1 5ACEFF2B
P 3600 2600
F 0 "#PWR?" H 3600 2350 50  0001 C CNN
F 1 "Earth" H 3600 2450 50  0001 C CNN
F 2 "" H 3600 2600 50  0001 C CNN
F 3 "" H 3600 2600 50  0001 C CNN
	1    3600 2600
	1    0    0    -1  
$EndComp
$Comp
L power:Earth #PWR?
U 1 1 5ACF0851
P 4200 3550
F 0 "#PWR?" H 4200 3300 50  0001 C CNN
F 1 "Earth" H 4200 3400 50  0001 C CNN
F 2 "" H 4200 3550 50  0001 C CNN
F 3 "" H 4200 3550 50  0001 C CNN
	1    4200 3550
	1    0    0    -1  
$EndComp
NoConn ~ 4100 3750
$Comp
L power:Earth #PWR?
U 1 1 5B09A57B
P 4950 1550
F 0 "#PWR?" H 4950 1300 50  0001 C CNN
F 1 "Earth" H 4950 1400 50  0001 C CNN
F 2 "" H 4950 1550 50  0001 C CNN
F 3 "" H 4950 1550 50  0001 C CNN
	1    4950 1550
	1    0    0    -1  
$EndComp
$Comp
L cubietruck_daughterboard-rescue:Screw_Terminal_01x04-cubietruck_daughterboard-rescue J?
U 1 1 5B09B3AB
P 2400 1500
F 0 "J?" H 2400 1700 50  0000 C CNN
F 1 "AIMS Inverter RJ22 port" H 2400 1200 50  0000 C CNN
F 2 "" H 2400 1500 50  0001 C CNN
F 3 "" H 2400 1500 50  0001 C CNN
	1    2400 1500
	-1   0    0    -1  
$EndComp
$Comp
L cubietruck_daughterboard-rescue:Screw_Terminal_01x04-cubietruck_daughterboard-rescue J?
U 1 1 5B09BA08
P 4000 3350
F 0 "J?" H 4000 3550 50  0000 C CNN
F 1 "Onewire" H 4000 3050 50  0000 C CNN
F 2 "" H 4000 3350 50  0001 C CNN
F 3 "" H 4000 3350 50  0001 C CNN
	1    4000 3350
	0    -1   -1   0   
$EndComp
$Comp
L power:Earth #PWR?
U 1 1 5B09C1C6
P 4450 3100
F 0 "#PWR?" H 4450 2850 50  0001 C CNN
F 1 "Earth" H 4450 2950 50  0001 C CNN
F 2 "" H 4450 3100 50  0001 C CNN
F 3 "" H 4450 3100 50  0001 C CNN
	1    4450 3100
	1    0    0    -1  
$EndComp
Text Notes 5400 800  0    60   ~ 0
VCC 5V
Text Notes 5250 3200 0    60   ~ 0
PI14 GPIO - to relayA CTRL
Text Notes 5250 3300 0    60   ~ 0
PI15 GPIO - to relayB CTRL
Text Notes 5250 3800 0    60   ~ 0
PC21 GPIO - to Onewire signal
$Comp
L power:Earth #PWR?
U 1 1 5B0AB6A3
P 4550 950
F 0 "#PWR?" H 4550 700 50  0001 C CNN
F 1 "Earth" H 4550 800 50  0001 C CNN
F 2 "" H 4550 950 50  0001 C CNN
F 3 "" H 4550 950 50  0001 C CNN
	1    4550 950 
	1    0    0    -1  
$EndComp
$Comp
L power:Earth #PWR?
U 1 1 5B0AB788
P 4850 3350
F 0 "#PWR?" H 4850 3100 50  0001 C CNN
F 1 "Earth" H 4850 3200 50  0001 C CNN
F 2 "" H 4850 3350 50  0001 C CNN
F 3 "" H 4850 3350 50  0001 C CNN
	1    4850 3350
	1    0    0    -1  
$EndComp
Text Notes 5400 1000 0    60   ~ 0
PB19 GPIO - i2c SDA
Text Notes 5400 1100 0    60   ~ 0
PB18 GPIO - i2c SCL
Wire Wire Line
	5100 1850 4850 1850
Wire Wire Line
	4350 1850 4550 1850
Wire Wire Line
	4350 1650 4450 1650
Wire Wire Line
	3750 1650 3250 1650
Wire Wire Line
	3250 1650 3250 1500
Wire Wire Line
	3250 1500 2600 1500
Wire Wire Line
	3000 1750 3550 1750
Wire Wire Line
	3550 1750 3550 1400
Wire Wire Line
	3550 1400 2600 1400
Wire Wire Line
	3000 2250 3000 2400
Wire Wire Line
	3000 1950 3000 1750
Connection ~ 3550 1750
Wire Wire Line
	3000 2600 2900 2600
Wire Wire Line
	2900 2600 2900 1600
Wire Wire Line
	2900 1600 2600 1600
Wire Wire Line
	3750 2750 3750 2500
Wire Wire Line
	3950 2500 3600 2500
Wire Wire Line
	4250 2500 4800 2500
Wire Wire Line
	4800 2500 4800 1950
Wire Wire Line
	4800 1950 5100 1950
Wire Wire Line
	3750 3950 3750 3050
Wire Wire Line
	4000 3550 4000 3750
Connection ~ 4750 3750
Connection ~ 4300 3950
Wire Wire Line
	5100 2150 5000 2150
Connection ~ 4100 3950
Wire Wire Line
	4100 3950 4100 3550
Wire Wire Line
	3750 3950 4100 3950
Wire Wire Line
	4950 1550 5100 1550
Wire Wire Line
	4450 3050 4450 3100
Wire Wire Line
	4300 3850 4300 3950
Wire Wire Line
	4600 3850 4750 3850
Wire Wire Line
	4750 3850 4750 3750
Wire Wire Line
	4950 3350 4850 3350
Wire Wire Line
	4750 950  4750 1050
Wire Wire Line
	4750 1050 5100 1050
Wire Wire Line
	4850 950  5100 950 
Wire Wire Line
	4650 950  4650 1150
$Comp
L cubietruck_daughterboard-rescue:Screw_Terminal_01x04-cubietruck_daughterboard-rescue J?
U 1 1 5B09BC36
P 4550 2850
F 0 "J?" H 4550 3050 50  0000 C CNN
F 1 "relays" H 4550 2550 50  0000 C CNN
F 2 "" H 4550 2850 50  0001 C CNN
F 3 "" H 4550 2850 50  0001 C CNN
	1    4550 2850
	0    -1   -1   0   
$EndComp
Wire Wire Line
	4750 3050 4750 3150
Wire Wire Line
	4750 3150 4950 3150
Wire Wire Line
	4650 3050 4650 3250
Wire Wire Line
	4650 3250 4950 3250
$Comp
L power:+5V #PWR?
U 1 1 5B0AE0E1
P 4550 3300
F 0 "#PWR?" H 4550 3150 50  0001 C CNN
F 1 "+5V" H 4550 3440 50  0000 C CNN
F 2 "" H 4550 3300 50  0001 C CNN
F 3 "" H 4550 3300 50  0001 C CNN
	1    4550 3300
	1    0    0    1   
$EndComp
Wire Wire Line
	4550 3050 4550 3300
$Comp
L cubietruck_daughterboard-rescue:Screw_Terminal_01x04-cubietruck_daughterboard-rescue J?
U 1 1 5B0AE41F
P 4650 750
F 0 "J?" H 4650 950 50  0000 C CNN
F 1 "i2c" H 4650 450 50  0000 C CNN
F 2 "" H 4650 750 50  0001 C CNN
F 3 "" H 4650 750 50  0001 C CNN
	1    4650 750 
	0    -1   -1   0   
$EndComp
$Comp
L power:+3.3V #PWR?
U 1 1 5B0AE713
P 4650 1150
F 0 "#PWR?" H 4650 1000 50  0001 C CNN
F 1 "+3.3V" H 4650 1290 50  0000 C CNN
F 2 "" H 4650 1150 50  0001 C CNN
F 3 "" H 4650 1150 50  0001 C CNN
	1    4650 1150
	1    0    0    1   
$EndComp
Wire Wire Line
	3550 1750 3750 1750
Wire Wire Line
	4750 3750 4950 3750
Wire Wire Line
	4300 3950 4950 3950
Wire Wire Line
	4100 3950 4300 3950
Wire Wire Line
	4000 3750 4750 3750
Text Notes 5250 3400 0    60   ~ 0
Ground
Text Notes 5250 3500 0    60   ~ 0
(unused)
$EndSCHEMATC
