{-# LANGUAGE OverloadedStrings, LambdaCase #-}

module WebServers (
	module WebServers,
	PowerChange(..),
) where

import Network.Wai
import Network.Wai.Handler.Warp
import Network.HTTP.Types
import Network.HTTP.Client
import Control.Concurrent.STM
import Control.Concurrent.Async
import Control.Exception
import Control.Monad
import Data.Monoid
import Data.String
import Data.Char
import Data.Binary.Builder
import Data.Time.Clock.POSIX
import Reactive.Banana.Automation (PowerChange (..))
import qualified Data.Aeson as JSON
import qualified Data.ByteString as B
import qualified Data.ByteString.Lazy as L
import qualified Data.Text as T
import qualified Data.Text.Encoding as T
import qualified Data.Map.Strict as M
import Prelude

import Sensors
import Utility.ThreadScheduler

pollerHostname :: String
pollerHostname = "house"

pollerBindPort :: Port
pollerBindPort = 8090

-- This web server should not be exposed to the public internet for two
-- reasons:
--  * the data is potentially private
--  * the use of a TChan allows for potential memory DOS by a client
--
-- Currently this is ok because it only listens on ipv6, and the NAT
-- firewalls it.
pollerSettings :: Settings
pollerSettings = setPort pollerBindPort defaultSettings

type AddSensedValue = SensedValue -> IO ()

type SensedValueBuffer = TVar (M.Map KeySensedValue SensedValue)

type GetSensedValue = KeySensedValue -> IO (Maybe SensedValue)

-- | The main request is GET, which streams JSON objects for
-- SensedValues.
--
-- Also supported is POST of a SensedValue.
runPollerWebserver
	:: (AddSensedValue -> SensedValue -> IO Bool)
	-> IO (Async (), AddSensedValue, GetSensedValue)
runPollerWebserver postsensedvaluecallback = do
	bchan <- newBroadcastTChanIO
	buf <- newTVarIO M.empty
	let addsensedvalue v = atomically $ do
		writeTChan bchan v
		-- Don't buffer overrides, as that could make them take
		-- effect at a later point.
		if isOverride v
			then return ()
			else modifyTVar' buf (M.insert (keySensedValue' v) v)
	let server = do
		r <- try' $ runSettings pollerSettings (serve bchan buf addsensedvalue)
		threadDelaySeconds (Seconds 2)
		putStrLn $ "webserver thread unexpectedly exited: " ++ show r ++ " (restarting)"
		server
	thread <- async server
	return (thread, addsensedvalue, getsensedvalue buf)
  where
	getsensedvalue buf k = atomically $ M.lookup k <$> readTVar buf
	serve bchan buf addsensedvalue req respond
		| requestMethod req == methodGet =
			servesensedvalues bchan buf respond
		| requestMethod req == methodPost =
			respond =<< servepost addsensedvalue req
		| otherwise = respond $
			responseLBS badRequest400 [] "unsupported method"

	servesensedvalues bchan buf respond = do
		chan <- atomically $ dupTChan bchan
		respond $ responseStream status200 [] $ \swrite sflush -> do
			let send v = do
				swrite $ fromLazyByteString $
					JSON.encode v <> "\n"
				sflush
			mapM_ send . M.elems =<< atomically (readTVar buf)
			let go = do
				send =<< atomically (readTChan chan)
				go
			go
	
	servepost addsensedvalue req = do
		b <- lazyRequestBody req
		case JSON.eitherDecode b of
			Left e -> return $ responseLBS badRequest400 [] $
				L.fromStrict $ T.encodeUtf8 $ T.pack $
					"json parse failed: " ++ e
			Right s -> do
				r <- postsensedvaluecallback addsensedvalue s
				return $ if r
					then responseLBS accepted202 [] ""
					else unsupportedrequest

	unsupportedrequest = responseLBS badRequest400 [] "unsupported request"
	
	try' :: IO a -> IO (Either SomeException a)
	try' = try

webManager :: IO Manager
webManager = newManager defaultManagerSettings

-- | Stream from the poller webserver, with automatic reconnection.
--
-- Note that the callback must complete within 60s or it will be canceled.
streamPollerWebserverReconnect :: (Either String SensedValue -> IO ()) -> IO ()
streamPollerWebserverReconnect callback = loop
  where
	loop = do
		-- new manager each time through loop since may be moving
		-- between networks
		manager <- webManager
		tv <- newTVarIO =<< getPOSIXTime
		_ <- go manager tv `race` waittimeout tv
		callback (Left "lost connection")
		threadDelaySeconds (Seconds 2)
		loop
	
	try' :: IO a -> IO (Either SomeException a)
	try' = try
	
	go manager tv = void $ try' $ streamPollerWebserver manager $ \ev -> do
		atomically . writeTVar tv =<< getPOSIXTime
		callback ev
		return True
	
	waittimeout tv = do
		threadDelaySeconds (Seconds 10)
		now <- getPOSIXTime
		lastupdate <- atomically $ readTVar tv
		if now > lastupdate + 60
			then return ()
			else waittimeout tv

-- | Stream from the poller webserver, until the callback yields False.
--
-- Throws exceptions for network errors.
streamPollerWebserver :: Manager -> (Either String SensedValue -> IO Bool) -> IO ()
streamPollerWebserver manager callback = withResponse request manager $ \resp ->
	if Network.HTTP.Client.responseStatus resp == ok200
		then brread (responseBody resp) []
		else return ()
  where
	request = Network.HTTP.Client.defaultRequest
		{ host = fromString pollerHostname
		, port = pollerBindPort
		, method = methodGet
		, path = "/"
		}

	brread rb l = do
		bs <- brRead rb
		if B.null bs
			then void $ callback $
				parsejson $ L.fromChunks (reverse l)
			else
				let (c, rest) = B.break (== fromIntegral (ord '\n')) bs
				in case B.splitAt 1 rest of
					("\n", rest') -> do
						continue <- callback $ 
							parsejson $ L.fromChunks (reverse (c:l))
						if continue
							then brread rb [rest']
							else return ()
					_ -> brread rb (bs:l)
	
	parsejson b = case JSON.eitherDecode b of
		Left e -> Left $
			"warning: unable to parse json from poller: " ++ e ++ " (" ++ show b ++ ")"
		Right v -> Right v

-- | Post a SensedValue to the poller webserver.
postSensedValue :: SensedValue -> IO Bool
postSensedValue s = do
	manager <- newManager defaultManagerSettings
	let request = Network.HTTP.Client.defaultRequest
		{ host = fromString pollerHostname
		, port = pollerBindPort
		, method = methodPost
		, path = "/"
		, Network.HTTP.Client.requestBody =
			RequestBodyLBS (JSON.encode s)
		}
	v <- try' $ do
		withResponse request manager $ \resp ->
			return $ Network.HTTP.Client.responseStatus resp == accepted202
	case v of
		Right r -> return r
		Left _ -> return False
  where
	try' :: IO a -> IO (Either SomeException a)
	try' = try

sensorFailure :: GetSensedValue -> AddSensedValue -> IO ()
sensorFailure getsensedvalue addsensedvalue = do
	n <- getsensedvalue (keySensedValue SensorFailures) >>= return . \case
		Just (SensorFailures n) -> n
		_ -> 0
	addsensedvalue (SensorFailures (n+1))
