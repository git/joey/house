module GPIO where

import System.Process
import Control.Concurrent
import System.Directory
import Control.Exception

import DataUnits

-- In is for ports that read a value; out is for ports that can be
-- controlled. It can be dangerous to mess them up.
data Direction = In | Out
	deriving (Eq, Show)

-- A value that is read from or sent to a port.
data GPIOValue = High | Low
	deriving (Eq, Show)

data GPIOPort = GPIOPort Int Direction

sysExportInterface :: FilePath
sysExportInterface = "/sys/class/gpio/export"

sysDirectionInterface :: GPIOPort -> FilePath
sysDirectionInterface (GPIOPort p _) =
	"/sys/class/gpio/gpio" ++ show p ++ "/direction"

sysValueInterface :: GPIOPort -> FilePath
sysValueInterface (GPIOPort p _) =
	"/sys/class/gpio/gpio" ++ show p ++ "/value"

-- Enable port and set direction.
--
-- Also lets the gpio group access the port.
enableGPIOPort :: GPIOPort -> IO ()
enableGPIOPort g@(GPIOPort p d) = do
	enabled <- isGPIOPortEnabled g
	if enabled
		then return ()
		else do
			writeFile sysExportInterface (show p)
			-- Unfortunately the sysfs interface is racey;
			-- have to wait for the port to show up.
			waitport
			writeFile (sysDirectionInterface g) $ case d of
				In -> "in"
				Out -> "out"
			let v = sysValueInterface g
			callProcess "chgrp" ["gpio", v]
			callProcess "chmod" ["g+rw", v]
  where
	waitport = do
		threadDelay 10000
		enabled <- isGPIOPortEnabled g
		if enabled
			then return ()
			else waitport

isGPIOPortEnabled :: GPIOPort -> IO Bool
isGPIOPortEnabled g = doesFileExist $ sysValueInterface g

readGPIOValue :: GPIOPort -> IO (Maybe GPIOValue)
readGPIOValue g = do
	s <- try' $ readFile $ sysValueInterface g
	case s of
		Right ('0':_) -> return $ Just Low
		Right ('1':_) -> return $ Just High
		_ -> return Nothing
  where
	try' :: IO a -> IO (Either SomeException a)
	try' = try

writeValue :: GPIOPort -> GPIOValue -> IO ()
writeValue g v = writeFile (sysValueInterface g) $ case v of
	Low -> "0"
	High -> "1"

relayA :: GPIOPort
relayA = GPIOPort 270 Out -- PI14

relayB :: GPIOPort
relayB = GPIOPort 271 Out -- PI15

relayC :: GPIOPort
relayC = GPIOPort 86 Out -- PC22

relayD :: GPIOPort
relayD = GPIOPort 277 Out -- PI21

relayE :: GPIOPort
relayE = GPIOPort 36 Out -- PB4

relayF :: GPIOPort
relayF = GPIOPort 35 Out -- PB3

springPumpRunIndicator :: GPIOPort
springPumpRunIndicator = GPIOPort 34 In -- PB2

-- This is a float switch input line to the pump controller, which
-- can be closed to stop the pump.
springPumpFloatSwitch :: GPIOPort
springPumpFloatSwitch = GPIOPort 259 Out -- PI3

kitchenCounterSwitch :: GPIOPort
kitchenCounterSwitch = GPIOPort 276 In -- PI20

-- This GPIO port is used to turn on and off the inverter.
inverterControlPort :: GPIOPort
inverterControlPort = relayB

-- This GPIO port was used to turn on and off the inverter.
-- (Replaced with relayB, so unused now.)
inverterControlPortOld :: GPIOPort
inverterControlPortOld = GPIOPort 84 Out -- PC20

-- This GPIO port was used to detect when the inverter is on and off.
-- (Samlex does not need this so unused now.)
inverterMonitorPortOld :: GPIOPort
inverterMonitorPortOld = GPIOPort 83 In -- PC19

-- Disabling power to the GPIO port opens the switch and lets
-- the pump run.
setSpringPumpPower :: PowerSetting -> IO ()
setSpringPumpPower (PowerSetting b) =
	setGPIOPower springPumpFloatSwitch (PowerSetting (not b))

setGPIOPower :: GPIOPort -> PowerSetting -> IO ()
setGPIOPower relayport st = writeValue relayport $
	case st of
		PowerSetting True -> High
		PowerSetting False -> Low

-- Set up GPIO ports. Must be run as root. May run repeatedly.
setupGPIOPorts :: IO ()
setupGPIOPorts = do
	enableGPIOPort inverterControlPortOld
	enableGPIOPort inverterMonitorPortOld
	enableGPIOPort relayA
	enableGPIOPort relayB
	enableGPIOPort relayC
	enableGPIOPort relayD
	enableGPIOPort relayE
	enableGPIOPort relayF
	enableGPIOPort springPumpRunIndicator
	enableGPIOPort springPumpFloatSwitch
	enableGPIOPort kitchenCounterSwitch
