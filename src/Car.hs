module Car where

import DataUnits
import Utility.ThreadScheduler
import Utility.PartialPrelude
import WebServers
import qualified Sensors

import Control.Exception
import Control.Monad
import Network.HTTP.Client
import Network.HTTP.Client.TLS
import Data.Char
import Data.Maybe
import qualified Data.ByteString.Lazy.Char8 as L8

data CarStatus = CarStatus
	{ stateOfCharge :: Percentage
	, chargeLimit :: Percentage
	}
	deriving (Show)

getCarStatus :: Manager -> IO (Maybe CarStatus)
getCarStatus mgr = either (const Nothing) id <$> try' go
  where
	go = do
		request <- parseRequest "http://sparrow.kitenet.net:4000/"
		parse
			. stripwhitespace
			. striptags False . L8.unpack
			. responseBody
			<$> httpLbs request mgr

	parse :: [String] -> Maybe CarStatus
	parse ls = CarStatus
		<$> (Percentage <$> parseStateOfCharge ls)
		<*> (Percentage <$> parseChargeLimit ls)

	parseStateOfCharge ls = case drop 1 $ dropWhile (/= "State of Charge") ls of
		-- "x% (y%)" when cold, x% is available SoC and y% is
		-- warm SoC
		(l:_) -> headMaybe $ mapMaybe readish $ words $
			filter (\c -> c /= '(' && c/= ')') l
		_ -> Nothing

	parseChargeLimit ls = case drop 1 $ dropWhile (/= "Charge Limit") ls of
		(l:_) -> lastMaybe $ mapMaybe readish $ words l
		_ -> Nothing

	striptags _ ('<':s) = striptags True s
	striptags _ ('>':s) = striptags False s
	striptags False (c:s) = c : striptags False s
	striptags True (_:s) = striptags True s
	striptags _ [] = []

	stripwhitespace = map (dropWhile isSpace)
		. filter (not . (all isSpace))
		. lines

	try' :: IO a -> IO (Either SomeException a)
        try' = try

pollCarStatus :: Seconds -> AddSensedValue -> GetSensedValue -> IO ()
pollCarStatus howoften sensed getsendsensedvalue = do
	mgr <- newTlsManager
	forever $ do
		let report val = do
			sensed val
			when (Sensors.sensorFailed val) $
				sensorFailure getsendsensedvalue sensed
		status <- getCarStatus mgr
		report (Sensors.CarStateOfCharge $ stateOfCharge <$> status)
		report (Sensors.CarChargeLimit $ chargeLimit <$> status)
		threadDelaySeconds howoften
