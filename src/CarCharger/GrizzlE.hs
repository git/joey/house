-- Grizzle-E mini connect car charger

{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DeriveGeneric #-}

module CarCharger.GrizzlE where

import DataUnits
import Utility.ThreadScheduler
import WebServers
import qualified Sensors

import Control.Concurrent.STM
import Control.Concurrent.Async
import Control.Exception
import Control.Monad
import Network.HTTP.Client
import Network.HTTP.Client.TLS
import Network.HTTP.Types
import Data.String
import Data.Maybe
import Data.Aeson
import GHC.Generics

data CarChargerHandle = CarChargerHandle (TMVar Bool) Manager

setCarChargerPower :: CarChargerHandle -> PowerSetting -> IO ()
setCarChargerPower (CarChargerHandle h _) (PowerSetting b) = 
	atomically $ do
		-- If a previous write has not been serviced yet,
		-- superscede it with this one.
		void $ tryTakeTMVar h
		putTMVar h b

getCarChargerHandle :: (String -> IO ()) -> IO CarChargerHandle
getCarChargerHandle logger = do
	mgr <- newTlsManager
	h <- newEmptyTMVarIO
	void $ async $ serviceCarChargerHandle logger mgr h
	return (CarChargerHandle h mgr)

-- Not really an API as such, this was sniffed from the web interface.
carChargerAPIEndpoint :: String
carChargerAPIEndpoint = "http://uc_evse.lan/pageEvent"

chargerAPIRequest :: RequestBody -> Request
chargerAPIRequest reqbody = 
	(parseRequest_ carChargerAPIEndpoint)
		{ method = "POST"
		, requestBody = reqbody
		}

chargeStart :: Request
chargeStart = chargerAPIRequest $ RequestBodyBS "evseEnabled=0"

chargeStop :: Request
chargeStop = chargerAPIRequest $ RequestBodyBS "evseEnabled=1"

-- Not used yet
chargingAmps :: Int -> Request
chargingAmps amps = chargerAPIRequest $ RequestBodyBS $ fromString $
	"currentSet=" ++ show amps

serviceCarChargerHandle :: (String -> IO ()) -> Manager -> TMVar Bool -> IO ()
serviceCarChargerHandle logger mgr h = waitnewreq
  where
	waitnewreq = do
		v <- atomically $ takeTMVar h
		servicereq v
	
	-- The charger has a bit of wifi packet loss, so this retries
	-- sending commands to it repeatedly until it succeeds, or until a
	-- new request is made.
	servicereq oldv = do
		v <- atomically $
			fromMaybe oldv <$> tryTakeTMVar h
		res <- case v of
			False -> mkreq chargeStop
			True -> mkreq chargeStart
		case res of
			Right True -> do
				-- Delay 30 seconds before sending any more
				-- requests, just to avoid excess traffic
				-- and any posssible flapping between
				-- states.
				threadDelaySeconds (Seconds 30)
				waitnewreq
			_ -> do
				threadDelaySeconds (Seconds 1)
				servicereq v
	
	mkreq req = try' $ do
		logger $ "car API: " ++ show req
		withResponse req mgr $ \resp ->
			if responseStatus resp == ok200
				then return True
				else do
					br <- brRead (responseBody resp)
					logger $ "car API failed:" ++ show (responseStatus resp) ++ " " ++ show br
					return False
	
	try' :: IO a -> IO (Either SomeException a)
        try' = try

carChargerStatusEndpoint :: String
carChargerStatusEndpoint = "http://uc_evse.lan/main"

data CarChargerStatus = CarChargerStatus
	{ pilot :: Int
	-- ^ 1 for EVSE state B (plugged in)
	-- 2 for EVSE state C (charge request)
	, evseEnabled :: Int
	-- ^ 0 when charging is enabled, and 1 when not
	-- (same as chargeStart and chargeStop)
	}
	deriving (Generic, Show)

instance FromJSON CarChargerStatus

isCarPluggedIn :: CarChargerStatus -> Bool
isCarPluggedIn ccs = pilot ccs == 1 || pilot ccs == 2

isCarCharging :: CarChargerStatus -> Bool
isCarCharging ccs = pilot ccs == 2

isCarChargerEnabled :: CarChargerStatus -> Bool
isCarChargerEnabled ccs = evseEnabled ccs == 0

chargerStatusRequest :: Request
chargerStatusRequest = 
	(parseRequest_ carChargerStatusEndpoint)
		{ method = "POST"
		, requestBody = mempty
		}

getCarChargerStatus :: CarChargerHandle -> IO (Maybe CarChargerStatus)
getCarChargerStatus (CarChargerHandle _ mgr) = 
	either (const Nothing) id <$> try' go
  where
	go = do
		resp <- httpLbs chargerStatusRequest mgr
		return (decode (responseBody resp))
	
	try' :: IO a -> IO (Either SomeException a)
        try' = try

pollCarChargerStatus :: Seconds -> AddSensedValue -> GetSensedValue -> IO ()
pollCarChargerStatus howoften sensed getsendsensedvalue = do
	h <- getCarChargerHandle (\_ -> return ())
	forever $ do
		let report val = do
			sensed val
			when (Sensors.sensorFailed val) $
				sensorFailure getsendsensedvalue sensed
		status <- getCarChargerStatus h
		report (Sensors.CarPluggedIn $ isCarPluggedIn <$> status)
		report (Sensors.CarChargerEnabled $ isCarChargerEnabled <$> status)
		threadDelaySeconds howoften
