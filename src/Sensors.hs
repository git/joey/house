{-# LANGUAGE GADTs, DeriveGeneric, GeneralizedNewtypeDeriving, DeriveFunctor, FlexibleInstances #-}

module Sensors where

import Data.Aeson
import GHC.Generics
import Data.Time.Clock
import Data.Time.Clock.POSIX
import Data.Time.Calendar

import DataUnits
import Epsolar
import qualified Trimetric

newtype CcID = CcID Int
	deriving (Generic, Show, Eq, Num)

-- | A value read from a sensor.
-- May contain Nothing if the sensor is not available for some reason.
data SensedValue where
	CcInputVolts :: CcID -> Maybe Volts -> SensedValue
	CcInputAmps :: CcID -> Maybe Amps -> SensedValue
	CcInputWatts :: CcID -> Maybe Watts -> SensedValue
	CcOutputVolts :: CcID -> Maybe Volts -> SensedValue
	CcTemperature :: CcID -> Maybe Temperature -> SensedValue
	CcBatteryPercent :: CcID -> Maybe Percentage -> SensedValue
	CcMaxBatteryVoltsToday :: CcID -> Maybe Volts -> SensedValue
	CcMinBatteryVoltsToday :: CcID -> Maybe Volts -> SensedValue
	CcKWHGeneratedToday :: CcID -> Maybe KWH -> SensedValue
	CcKWHGeneratedTotal :: CcID -> Maybe KWH -> SensedValue
	CcStatusCode :: CcID -> Maybe EpsolarStatusCode -> SensedValue
	PorchTemperature :: Maybe Temperature -> SensedValue
	SlabTemperature :: Maybe Temperature -> SensedValue
	HallTemperature :: Maybe Temperature -> SensedValue
	HallHumidity :: Maybe RelativeHumidity -> SensedValue
	FridgeTemperature :: Maybe Temperature -> SensedValue
	FridgeAuxTemperature :: Maybe Temperature -> SensedValue
	SpareProbeTemperature :: Maybe Temperature -> SensedValue
	TrimetricBatteryPercent :: Maybe Percentage -> SensedValue
	TrimetricBatteryVolts :: Maybe Volts -> SensedValue
	TrimetricBatteryWatts :: Maybe Watts -> SensedValue
	TrimetricCCStatusCode :: Maybe Trimetric.CCStatusCode -> SensedValue
	CarStateOfCharge :: Maybe Percentage -> SensedValue
	CarChargeLimit :: Maybe Percentage -> SensedValue
	CarPluggedIn :: Maybe Bool -> SensedValue
	CarChargerEnabled :: Maybe Bool -> SensedValue

	-- Count of number of sensor failures since the poller was last started.
	SensorFailures :: Integer -> SensedValue

	-- The automation sends these back to the poller.
	AutomationOverallStatus :: [Status] -> SensedValue
	AutomationFridgeRuntimeToday :: (Hours, Maybe Day) -> SensedValue
	AutomationFridgeRunDuration :: Minutes -> SensedValue
	FridgeRelaySetTo :: (PowerSetting, POSIXTime) -> SensedValue
	KitchenCounterOutletRelaySetTo :: PowerSetting -> SensedValue
	UsbHubsRelaySetTo :: PowerSetting -> SensedValue

	-- Set by user to override the fridge automation temporarily.
	FridgeOverride :: Override PowerSetting -> SensedValue
	-- Set by user to override the automation of the inverter's
	-- power temporarily.
	InverterOverride :: Override PowerSetting -> SensedValue
	-- Set by user to override the kitchen counter outlet.
	KitchenCounterOutletOverride :: Override PowerSetting -> SensedValue
	-- Set by user to override the car charger.
	CarChargerOverride :: Override PowerSetting -> SensedValue
	-- Set by the user to control the spring pump.
	SpringPumpOverride :: Override PowerSetting -> SensedValue
	SpringPumpRunCycle :: RunCycle -> SensedValue 
	deriving (Generic, Show, Eq)

isOverride :: SensedValue -> Bool
isOverride (FridgeOverride _) = True
isOverride (InverterOverride _) = True
isOverride (KitchenCounterOutletOverride _) = True
isOverride (CarChargerOverride _) = True
isOverride (SpringPumpOverride _) = True
isOverride _ = False

sensorFailed :: SensedValue -> Bool
sensorFailed (CcInputVolts _ Nothing) = True
sensorFailed (CcInputAmps _ Nothing) = True
sensorFailed (CcInputWatts _ Nothing) = True
sensorFailed (CcOutputVolts _ Nothing) = True
sensorFailed (CcTemperature _ Nothing) = True
sensorFailed (CcBatteryPercent _ Nothing) = True
sensorFailed (CcMaxBatteryVoltsToday _ Nothing) = True
sensorFailed (CcMinBatteryVoltsToday _ Nothing) = True
sensorFailed (CcKWHGeneratedToday _ Nothing) = True
sensorFailed (CcKWHGeneratedTotal _ Nothing) = True
sensorFailed (CcStatusCode _ Nothing) = True
sensorFailed (PorchTemperature Nothing) = True
sensorFailed (SlabTemperature Nothing) = True
sensorFailed (HallTemperature Nothing) = True
sensorFailed (HallHumidity Nothing) = True
sensorFailed (FridgeTemperature Nothing) = True
sensorFailed (FridgeAuxTemperature Nothing) = True
-- spare probe is not always connected
sensorFailed (SpareProbeTemperature Nothing) = False
sensorFailed (TrimetricBatteryPercent Nothing) = True
sensorFailed (TrimetricBatteryVolts Nothing) = True
sensorFailed (TrimetricBatteryWatts Nothing) = True
sensorFailed (TrimetricCCStatusCode Nothing) = True
sensorFailed (CarStateOfCharge Nothing) = True
sensorFailed (CarChargeLimit Nothing) = True
sensorFailed (CarPluggedIn Nothing) = True
sensorFailed (CarChargerEnabled Nothing) = True
sensorFailed _ = False

class DefSensedValue a where
	defSensedValue :: a

instance DefSensedValue (Maybe a) where
	defSensedValue = Nothing

instance DefSensedValue [a] where
	defSensedValue = []

instance DefSensedValue Bool where
	defSensedValue = False

instance DefSensedValue Integer where
	defSensedValue = 0

instance DefSensedValue (Override a) where
	defSensedValue = DisableOverride

instance DefSensedValue PowerSetting where
	defSensedValue = PowerSetting False

instance (DefSensedValue a, DefSensedValue b) => DefSensedValue (a, b) where
	defSensedValue = (defSensedValue, defSensedValue)

instance DefSensedValue Hours where
	defSensedValue = Hours 0

instance DefSensedValue Minutes where
	defSensedValue = Minutes 0

newtype KeySensedValue = KeySensedValue String
	deriving (Eq, Ord)

-- | A key for a SensedValue constructor, suitable for insertion in a map.
keySensedValue :: DefSensedValue a => (a -> SensedValue) -> KeySensedValue
keySensedValue c = keySensedValue' (c defSensedValue)

keySensedValue' :: SensedValue -> KeySensedValue
keySensedValue' (CcInputVolts (CcID n) _) = KeySensedValue $ "CcInputVolts " ++ show n
keySensedValue' (CcInputAmps (CcID n) _) = KeySensedValue $ "CcInputAmps " ++ show n
keySensedValue' (CcInputWatts (CcID n) _) = KeySensedValue $ "CcInputWatts " ++ show n
keySensedValue' (CcOutputVolts (CcID n) _) = KeySensedValue $ "CcOutputVolts " ++ show n
keySensedValue' (CcTemperature (CcID n) _) = KeySensedValue $ "CcTemperature " ++ show n
keySensedValue' (CcBatteryPercent (CcID n) _) = KeySensedValue $ "CcBatteryPercent " ++ show n
keySensedValue' (CcMaxBatteryVoltsToday (CcID n) _) = KeySensedValue $ "CcMaxBatteryVoltsToday " ++ show n
keySensedValue' (CcMinBatteryVoltsToday (CcID n) _) = KeySensedValue $ "CcMinBatteryVoltsToday " ++ show n
keySensedValue' (CcKWHGeneratedToday (CcID n) _) = KeySensedValue $ "CcKWHGeneratedToday " ++ show n
keySensedValue' (CcKWHGeneratedTotal (CcID n) _) = KeySensedValue $ "CcKWHGeneratedTotal " ++ show n
keySensedValue' (CcStatusCode (CcID n) _) = KeySensedValue $ "CcStatusCode " ++ show n
keySensedValue' (PorchTemperature _) = KeySensedValue "PorchTemperature"
keySensedValue' (SlabTemperature _) = KeySensedValue "SlabTemperature"
keySensedValue' (HallTemperature _) = KeySensedValue "HallTemperature"
keySensedValue' (HallHumidity _) = KeySensedValue "HallHumidity"
keySensedValue' (FridgeTemperature _) = KeySensedValue "FridgeTemperature"
keySensedValue' (FridgeAuxTemperature _) = KeySensedValue "FridgeAuxTemperature"
keySensedValue' (SpareProbeTemperature _) = KeySensedValue "SpareProbeTemperature"
keySensedValue' (TrimetricBatteryPercent _) = KeySensedValue "TrimetricBatteryPercent"
keySensedValue' (TrimetricBatteryVolts _) = KeySensedValue  "TrimetricBatteryVolts"
keySensedValue' (TrimetricBatteryWatts _) = KeySensedValue "TrimetricBatteryWatts"
keySensedValue' (TrimetricCCStatusCode _) = KeySensedValue "TrimetricCCStatusCode"
keySensedValue' (CarStateOfCharge _) = KeySensedValue "CarStateOfCharge"
keySensedValue' (CarChargeLimit _) = KeySensedValue "CarChargeLimit"
keySensedValue' (CarPluggedIn _) = KeySensedValue "CarPluggedIn"
keySensedValue' (CarChargerEnabled _) = KeySensedValue "CarChargerEnabled"
keySensedValue' (AutomationOverallStatus _) = KeySensedValue "AutomationOverallStatus"
keySensedValue' (AutomationFridgeRuntimeToday _) = KeySensedValue "AutomationFridgeRuntimeToday"
keySensedValue' (AutomationFridgeRunDuration _) = KeySensedValue "AutomationFridgeRunDuration"
keySensedValue' (FridgeRelaySetTo _) = KeySensedValue "FridgeRelaySetTo"
keySensedValue' (KitchenCounterOutletRelaySetTo _) = KeySensedValue "KitchenCounterOutletRelaySetTo"
keySensedValue' (UsbHubsRelaySetTo _) = KeySensedValue "UsbHubsRelaySetTo"
keySensedValue' (FridgeOverride _) = KeySensedValue "FridgeOverride"
keySensedValue' (SpringPumpOverride _) = KeySensedValue "SpringPumpOverride"
keySensedValue' (SpringPumpRunCycle _) = KeySensedValue "SpringPumpRunCycle"
keySensedValue' (InverterOverride _) = KeySensedValue "InverterOverride"
keySensedValue' (KitchenCounterOutletOverride _) = KeySensedValue "KitchenCounterOutletOverride"
keySensedValue' (CarChargerOverride _) = KeySensedValue "CarChargerOverride"
keySensedValue' (SensorFailures _) = KeySensedValue "SensorFailures"

-- | Format as a plain string with no units; used for populating rrd files.
formatSensedValue :: SensedValue -> Maybe String
formatSensedValue (CcInputVolts _ d) = formatDataUnit <$> d
formatSensedValue (CcInputAmps _ d) = formatDataUnit <$> d
formatSensedValue (CcInputWatts _ d) = formatDataUnit <$> d
formatSensedValue (CcOutputVolts _ d) = formatDataUnit <$> d
formatSensedValue (CcTemperature _ d) = formatDataUnit <$> d
formatSensedValue (CcBatteryPercent _ d) = formatDataUnit <$> d
formatSensedValue (CcMaxBatteryVoltsToday _ d) = formatDataUnit <$> d
formatSensedValue (CcMinBatteryVoltsToday _ d) = formatDataUnit <$> d
formatSensedValue (CcKWHGeneratedToday _ d) = formatDataUnit <$> d
formatSensedValue (CcKWHGeneratedTotal _ d) = formatDataUnit <$> d
formatSensedValue (CcStatusCode _ d) = formatDataUnit <$> d
formatSensedValue (PorchTemperature d) = formatDataUnit <$> d
formatSensedValue (SlabTemperature d) = formatDataUnit <$> d
formatSensedValue (HallTemperature d) = formatDataUnit <$> d
formatSensedValue (HallHumidity d) = formatDataUnit <$> d
formatSensedValue (FridgeTemperature d) = formatDataUnit <$> d
formatSensedValue (FridgeAuxTemperature d) = formatDataUnit <$> d
formatSensedValue (SpareProbeTemperature d) = formatDataUnit <$> d
formatSensedValue (TrimetricBatteryPercent d) = formatDataUnit <$> d
formatSensedValue (TrimetricBatteryVolts d) = formatDataUnit <$> d
formatSensedValue (TrimetricBatteryWatts d) = formatDataUnit <$> d
formatSensedValue (TrimetricCCStatusCode d) = formatDataUnit <$> d
formatSensedValue (CarStateOfCharge d) = formatDataUnit <$> d
formatSensedValue (CarChargeLimit d) = formatDataUnit <$> d
formatSensedValue (CarPluggedIn _) = Nothing
formatSensedValue (CarChargerEnabled _) = Nothing
formatSensedValue (AutomationOverallStatus _) = Nothing
formatSensedValue (AutomationFridgeRuntimeToday (d, _)) =
	Just $ formatDataUnit d
formatSensedValue (AutomationFridgeRunDuration d) = Just $ formatDataUnit d
formatSensedValue (FridgeRelaySetTo _) = Nothing
formatSensedValue (KitchenCounterOutletRelaySetTo _) = Nothing
formatSensedValue (UsbHubsRelaySetTo _) = Nothing
formatSensedValue (FridgeOverride _) = Nothing
formatSensedValue (SpringPumpOverride _) = Nothing
formatSensedValue (SpringPumpRunCycle _) = Nothing
formatSensedValue (InverterOverride _) = Nothing
formatSensedValue (KitchenCounterOutletOverride _) = Nothing
formatSensedValue (CarChargerOverride _) = Nothing
formatSensedValue (SensorFailures d) = Just $ show d

formatAsHours :: NominalDiffTime -> String
formatAsHours secs = showDoublePrecision 2
	(fromRational (toRational secs) / 60 / 60)

instance ToJSON SensedValue
instance FromJSON SensedValue

instance ToJSON CcID
instance FromJSON CcID

data Override a
	= OverrideSeconds Int a
	| DisableOverride
	deriving (Generic, Show, Eq, Ord, Functor)

instance ToJSON a => ToJSON (Override a)
instance FromJSON a => FromJSON (Override a)

applyOverride :: (o -> a) -> Override o -> a -> a
applyOverride conv (OverrideSeconds _ a) _ = conv a
applyOverride _conv DisableOverride a = a

getOverride :: Override o -> Maybe o
getOverride (OverrideSeconds _ o) = Just o
getOverride DisableOverride = Nothing

data Overridable v = Overridden v | SetTo v | NotKnown
	deriving (Eq, Show, Generic)

instance ToJSON v => ToJSON (Overridable v)
instance FromJSON v => FromJSON (Overridable v)

mkOverridable :: Override a -> Maybe a -> Overridable a
mkOverridable (OverrideSeconds _ override) _ = Overridden override
mkOverridable DisableOverride (Just a) = SetTo a
mkOverridable DisableOverride Nothing = NotKnown

data Status
	= LowPower
	| Sunny
	| InverterStatus (Overridable PowerSetting)
	| FridgeStatus (Overridable PowerSetting)
	| FridgeThoughts String
	| KitchenCounterOutletStatus PowerSetting
	| CarChargerStatus (Overridable PowerSetting)
	| SpringPumpStatus PowerSetting
	deriving (Eq, Show, Generic)

instance ToJSON Status
instance FromJSON Status

data RunCycle = OnOffRunCycle
	{ onMinutes :: Int
	, offMinutes :: Int
	} deriving (Eq, Show, Generic)

instance ToJSON RunCycle
instance FromJSON RunCycle
