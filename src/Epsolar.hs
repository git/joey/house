{-# LANGUAGE DeriveGeneric, GeneralizedNewtypeDeriving #-}

module Epsolar where

import DataUnits
import qualified Batteries

import GHC.Generics
import Data.Aeson
import Text.Read
import Data.Binary.Get
import Data.Bits
import Data.Functor.Compose
import Control.Exception
import Control.Concurrent
import Control.Monad
import Reactive.Banana.Automation
import qualified System.Modbus as Modbus

data RegisterVectorFor t = GetRegisterVector Modbus.RegisterVector

data RegisterInfo t = RegisterInfo Modbus.Addr (Get t)

newtype EpsolarContext = EpsolarContext Modbus.Context

data EpsolarHandle = EpsolarHandle FilePath (MVar EpsolarContext)

mkEpsolarHandle :: FilePath -> IO EpsolarHandle
mkEpsolarHandle dev = EpsolarHandle <$> pure dev <*> newEmptyMVar

-- These device names are assigned by a udev configuration.
epsolarSerialDevice1 :: FilePath
epsolarSerialDevice1 = "/dev/epsolar1"
epsolarSerialDevice2 :: FilePath
epsolarSerialDevice2 = "/dev/epsolar2"
epsolarSerialDevice3 :: FilePath
epsolarSerialDevice3 = "/dev/epsolar3"
epsolarSerialDevice4 :: FilePath
epsolarSerialDevice4 = "/dev/epsolar4"
epsolarSerialDevice5 :: FilePath
epsolarSerialDevice5 = "/dev/epsolar5"

openEpsolarContext :: FilePath -> IO EpsolarContext
openEpsolarContext dev = do
	ctx <- Modbus.new_rtu
		dev
		(Modbus.Baud 115200)
		Modbus.ParityNone
		(Modbus.DataBits 8)
		(Modbus.StopBits 1)
	Modbus.set_slave ctx (Modbus.DeviceAddress 1)
	Modbus.connect ctx
	Modbus.set_error_recovery ctx Modbus.ErrorRecoveryLinkProtocol
	return (EpsolarContext ctx)

-- Deal with Epsolar's glitchy modbus comms. If the action fails,
-- or any exception is caught, closes and reopens the connection,
-- and tries it a second time.
withEpsolarContext :: EpsolarHandle -> (EpsolarContext -> IO (Either String t)) -> IO (Either String t)
withEpsolarContext (EpsolarHandle dev mv) a = go (2 :: Integer) ""
  where
	go 0 err = return (Left err)
	go retries _ = do
		-- avoid querys too close together, which sometimes
		-- break due to the flakiness of the modbus comms.
		-- (this could probably be done better by adjusting the
		-- timeouts in libmodbus)
		threadDelay 1500000

		ctx <- tryReadMVar mv >>= \case
			Nothing -> do
				ctx <- openEpsolarContext dev
				putMVar mv ctx
				return ctx
			Just ctx -> return ctx
		
		r <- try' (a ctx)
		case r of
			Right (Right t) -> return (Right t)
			Right (Left s) -> do
				reconnect
				go (retries - 1) s
			Left ex -> do
				reconnect
				go (retries - 1) ("failed to communicate with epsolar " ++ dev ++ ": " ++ show ex)

	try' :: IO a -> IO (Either SomeException a)
	try' = try
	
	reconnect = void $ tryTakeMVar mv

-- Turns the load control on/off.
setEpsolarLoad :: EpsolarContext -> PowerSetting -> IO (Either String ())
setEpsolarLoad (EpsolarContext ctx) (PowerSetting b) = do
	Modbus.write_bit ctx (Modbus.Addr 2) $ case b of
		True -> 1
		False -> 0
	return (Right ())

-- Read from the charge controller using modbus. 
-- The register vector is pre-allocated to avoid unncessary reallocation.
readEpsolar :: EpsolarContext -> RegisterVectorFor t -> RegisterInfo t -> IO (Either String t)
readEpsolar (EpsolarContext ctx) (GetRegisterVector regs) (RegisterInfo startaddr getter) = do
	b <- Modbus.read_input_registers ctx startaddr regs
	return $ case runGetOrFail getter b of
		Left (_, _, err) -> Left err
		Right (_, _, res) -> Right res

readEpsolarStatus :: EpsolarHandle -> RegisterVectorFor EpsolarStatus -> IO (Either String EpsolarStatus)
readEpsolarStatus h v = withEpsolarContext h $ \ctx ->
	readEpsolar ctx v regsEpsolarStatus

-- These registers can all be read in one block.
data EpsolarStatus = EpsolarStatus
	{ epsolar_input_volts :: Volts
	, epsolar_input_amps :: Amps
	, epsolar_input_watts :: Watts
	, epsolar_output_volts :: Volts
	, epsolar_output_amps :: Amps
	, epsolar_output_watts :: Watts
	}
	deriving (Show)

regsEpsolarStatus :: RegisterInfo EpsolarStatus
regsEpsolarStatus = RegisterInfo (Modbus.Addr 0x3100) $ EpsolarStatus
	<$> (Volts <$> epeverfloat)  -- register 0x3100
	<*> (Amps <$> epeverfloat)   -- register 0x3101
	<*> (Watts <$> epeverfloat2) -- register 0x3102 (low), 0x3103 (high)
	<*> (Volts <$> epeverfloat)  -- register 0x3104
	<*> (Amps <$> epeverfloat)   -- register 0x3105
	<*> (Watts <$> epeverfloat2) -- register 0x3106 (low), 0x3107 (high)

mkEpsolarStatusRegisterVector :: IO (RegisterVectorFor EpsolarStatus)
mkEpsolarStatusRegisterVector = 
	GetRegisterVector <$> Modbus.mkRegisterVector 8

-- A couple other status registers that are often wanted, but can't be read
-- in a single block.
data EpsolarExtStatus = EpsolarExtStatus
	{ epsolar_inside_temp :: Temperature
	, epsolar_battery_percent :: Percentage
	}
	deriving (Show)

readEpsolarExtStatus :: EpsolarHandle -> RegisterVectorFor EpsolarSingleValue -> IO (Either String EpsolarExtStatus)
readEpsolarExtStatus h regs = getCompose $ EpsolarExtStatus
	<$> v readEpsolarInsideTemp
	<*> v readEpsolarBatteryPercent
  where
	v a = Compose (withEpsolarContext h $ \ctx -> a ctx regs)

-- Bundle of additional status registers that are not as often wanted.
data EpsolarOtherStatus = EpsolarOtherStatus
	{ epsolar_kwh_generated_today :: KWH
	, epsolar_kwh_generated_total :: KWH
	, epsolar_max_input_volts_today :: Volts
	, epsolar_max_battery_volts_today :: Volts
	, epsolar_min_battery_volts_today :: Volts
	, epsolar_battery_temp :: Temperature
	, epsolar_statuscode :: EpsolarStatusCode
	}
	deriving (Show)

readEpsolarOtherStatus :: EpsolarHandle -> RegisterVectorFor EpsolarSingleValue -> RegisterVectorFor EpsolarSingleValueWide -> IO (Either String EpsolarOtherStatus)
readEpsolarOtherStatus h regs1 regs2 = getCompose $ EpsolarOtherStatus
	<$> v2 readEpsolarKWHGeneratedToday
	<*> v2 readEpsolarKWHGeneratedTotal
	<*> v1 readEpsolarMaxInputVoltsToday
	<*> v1 readEpsolarMaxBatteryVoltsToday
	<*> v1 readEpsolarMinBatteryVoltsToday
	<*> v1 readEpsolarBatteryTemp
	<*> v1 readEpsolarStatusCode
  where
	v1 a = Compose (withEpsolarContext h $ \ctx -> a ctx regs1)
	v2 a = Compose (withEpsolarContext h $ \ctx -> a ctx regs2)

-- Used when reading a single value that comes from one register.
data EpsolarSingleValue = EpsolarSingleValue

-- Used when reading a single value that comes from two registers.
data EpsolarSingleValueWide = EpsolarSingleValueWide

mkEpsolarSingleValueRegisterVector :: IO (RegisterVectorFor EpsolarSingleValue)
mkEpsolarSingleValueRegisterVector =
	GetRegisterVector <$> Modbus.mkRegisterVector 1

mkEpsolarSingleValueWideRegisterVector :: IO (RegisterVectorFor EpsolarSingleValueWide)
mkEpsolarSingleValueWideRegisterVector =
	GetRegisterVector <$> Modbus.mkRegisterVector 2

readEpsolarSingleValue :: EpsolarContext -> RegisterVectorFor EpsolarSingleValue -> RegisterInfo t -> IO (Either String t)
readEpsolarSingleValue ctx (GetRegisterVector regs) =
	readEpsolar ctx (GetRegisterVector regs)

readEpsolarSingleValueWide :: EpsolarContext -> RegisterVectorFor EpsolarSingleValueWide -> RegisterInfo t -> IO (Either String t)
readEpsolarSingleValueWide ctx (GetRegisterVector regs) =
	readEpsolar ctx (GetRegisterVector regs)

readEpsolarInsideTemp :: EpsolarContext -> RegisterVectorFor EpsolarSingleValue -> IO (Either String Temperature)
readEpsolarInsideTemp ctx regs = readEpsolarSingleValue ctx regs $
	RegisterInfo (Modbus.Addr 0x3111) (TemperatureCelsius <$> epeverfloat)

readEpsolarBatteryPercent :: EpsolarContext -> RegisterVectorFor EpsolarSingleValue -> IO (Either String Percentage)
readEpsolarBatteryPercent ctx regs = readEpsolarSingleValue ctx regs $
	RegisterInfo (Modbus.Addr 0x311A) (Percentage . (* 100) <$> epeverfloat)

readEpsolarKWHGeneratedToday :: EpsolarContext -> RegisterVectorFor EpsolarSingleValueWide -> IO (Either String KWH)
readEpsolarKWHGeneratedToday ctx regs = readEpsolarSingleValueWide ctx regs $
	RegisterInfo (Modbus.Addr 0x330C) (KWH <$> epeverfloat2)

readEpsolarKWHGeneratedTotal :: EpsolarContext -> RegisterVectorFor EpsolarSingleValueWide -> IO (Either String KWH)
readEpsolarKWHGeneratedTotal ctx regs = readEpsolarSingleValueWide ctx regs $
	RegisterInfo (Modbus.Addr 0x3312) (KWH <$> epeverfloat2)

readEpsolarMaxInputVoltsToday :: EpsolarContext -> RegisterVectorFor EpsolarSingleValue -> IO (Either String Volts)
readEpsolarMaxInputVoltsToday ctx regs = readEpsolarSingleValue ctx regs $
	RegisterInfo (Modbus.Addr 0x3300) (Volts <$> epeverfloat)

readEpsolarMaxBatteryVoltsToday :: EpsolarContext -> RegisterVectorFor EpsolarSingleValue -> IO (Either String Volts)
readEpsolarMaxBatteryVoltsToday ctx regs = readEpsolarSingleValue ctx regs $
	RegisterInfo (Modbus.Addr 0x3302) (Volts <$> epeverfloat)

readEpsolarMinBatteryVoltsToday :: EpsolarContext -> RegisterVectorFor EpsolarSingleValue -> IO (Either String Volts)
readEpsolarMinBatteryVoltsToday ctx regs = readEpsolarSingleValue ctx regs $
	RegisterInfo (Modbus.Addr 0x3303) (Volts <$> epeverfloat)

readEpsolarBatteryTemp :: EpsolarContext -> RegisterVectorFor EpsolarSingleValue -> IO (Either String Temperature)
readEpsolarBatteryTemp ctx regs = readEpsolarSingleValue ctx regs $
	RegisterInfo (Modbus.Addr 0x331D) (TemperatureCelsius <$> epeverfloat)

readEpsolarStatusCode :: EpsolarContext -> RegisterVectorFor EpsolarSingleValue -> IO (Either String EpsolarStatusCode)
readEpsolarStatusCode ctx regs = readEpsolarSingleValue ctx regs $
	RegisterInfo (Modbus.Addr 0x3201) (EpsolarStatusCode <$> epeverint)

newtype EpsolarStatusCode = EpsolarStatusCode Int
	deriving (Eq, Show, Generic)

instance DataUnit EpsolarStatusCode where
        parseDataUnit _ s = EpsolarStatusCode <$> readMaybe s
        formatDataUnit (EpsolarStatusCode n) = show n

instance ToJSON EpsolarStatusCode
instance FromJSON EpsolarStatusCode

-- | Status reported by the charge controller.
data ChargingStatus = NoCharging | Float | Boost | Equalization
	deriving (Show, Eq)

chargingStatus :: EpsolarStatusCode -> ChargingStatus
chargingStatus (EpsolarStatusCode n) = case (testBit n 3, testBit n 2) of
	(False, False) -> NoCharging
	(False, True)  -> Float
	(True,  False) -> Boost
	(True,  True)  -> Equalization

newtype Load t = Load t
	deriving (Eq, Show, Num)

-- | There is no status code for absorb, but the charge controller
-- does seem to include such a stage. In Boost at around 85% full, 
-- the voltage is held steady at the configured absorbVoltage, 
-- and the watts taper off as the battery finishes charging.
--
-- This looks similar to a lack of sunlight, but connecting an additional
-- load will probably result in many more watts being produced.
--
-- A watts estimate for the current load has to be provided to this
-- function.
probablyInAbsorb :: ChargingStatus -> Percentage -> Volts -> Watts -> Load Watts -> Bool
probablyInAbsorb Boost batterypercent volts wattsproduced (Load wattsload) = and
	[ wattdiff > Watts 20
	, batterypercent >= Percentage 80
	, batterypercent < Percentage 100
	, volts `inRange` Batteries.absorbVoltage
	]
  where
	wattdiff = wattsproduced - wattsload
probablyInAbsorb _ _ _ _ _ = False

epeverint :: Get Int
epeverint = fromIntegral <$> getWord16host

epeverfloat :: Get Double
epeverfloat = epeverdecimals 2 . fromIntegral <$> getWord16host

epeverfloat2 :: Get Double
epeverfloat2 = do
	l <- (fromIntegral <$> getWord16host) :: Get Double
	h <- (fromIntegral <$> getWord16host) :: Get Double
	return (epeverdecimals 2 (l + h*65536))

epeverdecimals ::Int -> Double -> Double
epeverdecimals n v = v / (10^n)
