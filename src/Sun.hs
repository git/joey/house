module Sun where

import Data.Time.Horizon
import Data.Time.Calendar
import Data.Time.Clock
import Data.Time.LocalTime

localSunset :: Day -> UTCTime
localSunset d = sunset d houseLong houseLat

localSunrise :: Day -> UTCTime
localSunrise d = sunrise d houseLong houseLat

-- This is the coordinates of my dentist, which is close enough to me.
-- He would not appreciate the black helicopters.
houseLong :: LongitudeWest
houseLong  = 83.007

houseLat :: LatitudeNorth
houseLat = 36.406
