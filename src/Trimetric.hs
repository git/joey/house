-- TM2030 serial data

{-# LANGUAGE DeriveGeneric #-}

module Trimetric where

import DataUnits

import GHC.Generics
import Data.Aeson
import Data.Maybe
import Text.Read
import System.Process
import Control.Monad

data TrimetricValue
	= TrimetricBatteryVolts Volts
	| TrimetricFilteredVolts Volts
	-- ^ 5 minute filtered average
	| TrimetricBattery2Volts Volts
	| TrimetricBatteryAmps Amps
	| TrimetricFilteredAmps Amps
	-- ^ 5 minute filtered average
	| TrimetricCCStatusCode CCStatusCode
	| TrimetricAmpHoursFromFull AH
	| TrimetricBatteryPercent Percentage
	| TrimetricBatteryWatts Watts
	| TrimetricHoursSinceCharged Hours
	| TrimetricHoursSinceEqualized Hours
	| TrimetricReplacedPercentage Percentage
	-- ^ Percent of used amp-hours since the last full charge.
	| TrimetricLowestPreviousDischargeAmpHours AH
	deriving (Eq, Show, Generic)

instance ToJSON TrimetricValue
instance FromJSON TrimetricValue

newtype CCStatusCode = CCStatusCode Int
        deriving (Eq, Show, Generic)

instance DataUnit CCStatusCode where
	--The first number is the charge controller state as guessed by
	--the trimetric. The rest is its guess at a pulse width, not very
	--relevant-so skipped.
	parseDataUnit _ s = CCStatusCode <$> readMaybe (take 1 s)
	formatDataUnit (CCStatusCode n) = show n

instance ToJSON CCStatusCode
instance FromJSON CCStatusCode

data CCStatus
	= CCDischargingLow -- ^ below 98% full
	| CCBulk
	| CCAbsorb
	| CCFloat
	| CCFinish
	| CCMaxVoltageFinish
	| CCDischargingHigh
        deriving (Eq, Show, Generic)

instance ToJSON CCStatus
instance FromJSON CCStatus

ccStatus :: CCStatusCode -> Maybe CCStatus
ccStatus (CCStatusCode c) = case c of
	0 -> Just CCDischargingLow
	1 -> Just CCBulk
	2 -> Just CCAbsorb
	3 -> Just CCFloat
	4 -> Just CCFinish
	5 -> Just CCMaxVoltageFinish
	6 -> Just CCDischargingHigh
	_ -> Nothing

parseTrimetricValue :: String -> String -> Maybe TrimetricValue
parseTrimetricValue "V" n = TrimetricBatteryVolts <$> parseDataUnit 2 n
parseTrimetricValue "FV" n = TrimetricFilteredVolts <$> parseDataUnit 2 n
parseTrimetricValue "V2" n = TrimetricBattery2Volts <$> parseDataUnit 2 n 
parseTrimetricValue "A" n = TrimetricBatteryAmps <$> parseDataUnit 2 n
parseTrimetricValue "FA" n = TrimetricFilteredAmps <$> parseDataUnit 2 n
parseTrimetricValue "PW" n = TrimetricCCStatusCode <$> parseDataUnit 0 n
parseTrimetricValue "AH" n = TrimetricAmpHoursFromFull <$> parseDataUnit 2 n
parseTrimetricValue "T%" n = TrimetricBatteryPercent <$> parseDataUnit 2 n
parseTrimetricValue "W" n = TrimetricBatteryWatts <$> parseDataUnit 2 n
parseTrimetricValue "DSC" n = (\d -> TrimetricHoursSinceCharged (d*24))
	<$> parseDataUnit 2 n
parseTrimetricValue "DSE" n = (\d -> TrimetricHoursSinceEqualized (d*24))
	<$> parseDataUnit 2 n
parseTrimetricValue "r%" n = TrimetricReplacedPercentage <$> parseDataUnit 2 n
parseTrimetricValue "pD" n = TrimetricLowestPreviousDischargeAmpHours 
	<$> parseDataUnit 2 n
parseTrimetricValue _ _ = Nothing

-- | Never ending stream of data from the trimetric.
streamTrimetricSerialPort :: IO [TrimetricValue]
streamTrimetricSerialPort = do
	configTrimetricSerialPort 100
	parseTrimetricStream <$> readFile trimetricSerialPort

prepSampleTrimetricStream :: IO ()
prepSampleTrimetricStream = configTrimetricSerialPort 100

-- | Sample enough values that the returned list should include at least
-- one of every field. Call prepSampleTrimetricStream once before
-- calling this now and then to get the current values.
sampleTrimetricStream :: IO [TrimetricValue]
sampleTrimetricStream = take 15 . parseTrimetricStream
	<$> readFile trimetricSerialPort

-- | This can parse the stream  of data from any point, it need not be from
-- the start. 
--
-- The first value in the stream will be skipped
-- ie "99,V=27,A=11" will start parsing at the "A=11"
-- and "W=600,V=27" will start pasring at the "V=27"
-- (because the first value may be a truncated "PW=600")
parseTrimetricStream :: String -> [TrimetricValue]
parseTrimetricStream = mapMaybe (uncurry parseTrimetricValue) 
	. sep . dropWhile (/= '=')
  where
	sep [] = []
	sep s = case break (== '=') s of
		(k, ('=':s')) -> case break (== ',') s' of
			(v, (',':r)) -> (k, v) : sep r
			(v, []) -> (k, v) : []
			_ -> sep []
		_ -> sep []

-- | Set up the serial port to read.
--
-- The Int is the number of characters to buffer and get in each read.
-- Around 100 will get most of the different values in each read, and
-- result in only a few reads per second.
configTrimetricSerialPort :: Int -> IO ()
configTrimetricSerialPort bufsz = do
	stty ["-F", trimetricSerialPort, "2400"]
	stty ["-F", trimetricSerialPort, "-icanon"]
	stty ["-F", trimetricSerialPort, "min", show bufsz]
  where
	stty ps = do
		(Nothing, Nothing, Nothing, ph) <- createProcess (proc "stty" ps)
		void $ waitForProcess ph

trimetricSerialPort :: FilePath
trimetricSerialPort = "/dev/ttyUSB0"
