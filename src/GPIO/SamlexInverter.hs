-- Samlex inverter

module GPIO.SamlexInverter where

import GPIO
import DataUnits

setSamlexInverterPower :: PowerSetting -> IO ()
setSamlexInverterPower (PowerSetting b) = 
	setGPIOPower inverterControlPort (PowerSetting b)
