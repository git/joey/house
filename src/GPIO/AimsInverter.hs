-- AIMS inverter (not used any longer because it died).

module GPIO.AimsInverter where

import Control.Concurrent

import GPIO
import DataUnits

getAimsInverterPowerSetting :: IO PowerSetting
getAimsInverterPowerSetting = do
	v <- readGPIOValue inverterMonitorPortOld
	case v of
		-- The way the daughter board is wired up,
		-- the port will be Low when the power Led is lit.
		Just Low -> return $ PowerSetting True
		Just High -> return $ PowerSetting False
		Nothing -> error "failed reading AIMS inverter power status!"

toggleAimsInverterPower :: IO ()
toggleAimsInverterPower = do
	-- Equivilant to pressing the control button.
	writeValue inverterControlPort High
	-- Wait half a second for the aims to react.
	threadDelay 500000
	-- And release the control button.
	writeValue inverterControlPort Low

setAimsInverterPower :: PowerSetting -> IO ()
setAimsInverterPower want = do
	st <- getAimsInverterPowerSetting
	if st == want
		then return ()
		else toggleAimsInverterPower
