{- thread scheduling
 -
 - Copyright 2012, 2013 Joey Hess <id@joeyh.name>
 - Copyright 2011 Bas van Dijk & Roel van Dijk
 -
 - License: BSD-2-clause
 -}

module Utility.ThreadScheduler where

import Control.Monad
import Control.Concurrent

newtype Seconds = Seconds { fromSeconds :: Int }
	deriving (Eq, Ord, Show)

type Microseconds = Integer

threadDelaySeconds :: Seconds -> IO ()
threadDelaySeconds (Seconds n) = unboundDelay (fromIntegral n * oneSecond)

{- Like threadDelay, but not bounded by an Int.
 -
 - There is no guarantee that the thread will be rescheduled promptly when the
 - delay has expired, but the thread will never continue to run earlier than
 - specified.
 - 
 - Taken from the unbounded-delay package to avoid a dependency for 4 lines
 - of code.
 -}
unboundDelay :: Microseconds -> IO ()
unboundDelay time = do
	let maxWait = min time $ toInteger (maxBound :: Int)
	threadDelay $ fromInteger maxWait
	when (maxWait /= time) $ unboundDelay (time - maxWait)

oneSecond :: Microseconds
oneSecond = 1000000
