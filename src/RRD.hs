module RRD where

import Utility.ThreadScheduler

import System.FilePath
import Data.List

data FreqPair t = FreqPair
	{ frequent :: t
	, infrequent :: t
	}

data Frequency = Frequent | Infrequent
	deriving (Show)

getFrequent :: Frequency -> FreqPair t -> t
getFrequent Frequent fp = frequent fp
getFrequent Infrequent fp = infrequent fp

type RrdName = String

hours :: Int -> Seconds
hours n = Seconds (n*60*60)

days :: Int -> Seconds
days n = Seconds (n*60*60*24)

years :: Int -> Seconds
years n = Seconds (n*60*60*24*365)

newtype XFF = XFF Float

defXFF :: XFF
defXFF = XFF 0.5

newtype CombinedSteps = CombinedSteps Int

newtype Rows = Rows Int

data RRAType = Average | Min | Max | Last

formatRRAType :: RRAType -> String
formatRRAType Average = "AVERAGE"
formatRRAType Min = "Min"
formatRRAType Max = "Max"
formatRRAType Last = "Last"

data RRA = RRA RRAType XFF CombinedSteps Rows

formatRRA :: RRA -> String
formatRRA (RRA t (XFF xff) (CombinedSteps cs) (Rows rs)) = intercalate ":"
	[ "RRA"
	, formatRRAType t
	, show xff
	, show cs
	, show rs
	]

newtype StepLen = StepLen Seconds

newtype StorageSize = StorageSize Seconds

newtype Granularity = Granularity Seconds

-- | Makes an RRA that stores an average.
mkAverage :: XFF -> StorageSize -> StepLen -> Granularity -> RRA
mkAverage xff (StorageSize (Seconds storagesize)) (StepLen (Seconds steplen)) (Granularity (Seconds granularity)) = 
	RRA Average xff (CombinedSteps cs)
		(Rows (storagesize `div` steplen `div` cs))
  where
	cs = granularity `div` steplen

-- | Makes an RRA that stores each data point as-is.
mkFull :: XFF -> StorageSize -> StepLen -> RRA
mkFull xff (StorageSize (Seconds storagesize)) (StepLen (Seconds steplen)) = 
	RRA Average xff (CombinedSteps 1) (Rows (storagesize `div` steplen))

allRras :: StepLen -> [(String, [RRA])]
allRras steplen =
	-- Recent data only so the rrd file is small and can be quickly
	-- loaded in a web browser.
	[ ( "recent",
		-- Full data for 3 days
		[ mkFull defXFF (StorageSize (days 3)) steplen
		]
	  )
	-- Long-term historical data.
	, ( "historical",
		-- Full data for 2 months
		[ mkFull defXFF (StorageSize (days 60)) steplen
		-- Hourly averages for one year.
		, mkAverage defXFF (StorageSize (years 1)) steplen
			(Granularity (hours 1))
		-- Daily averages for 50 years.
		, mkAverage defXFF (StorageSize (years 50)) steplen
			(Granularity (days 1))
		]
	  )
	]

rrdFiles :: [(String, [RRA])] -> [FreqPair ([RRA], FilePath)]
rrdFiles = map mk
  where
	mk (rdesc, rras) = FreqPair
		{ frequent = (rras, "rrds" </> rdesc </> "pv-"++rdesc++"-frequent.rrd")
		, infrequent = (rras, "rrds" </> rdesc </> "pv-"++rdesc++"-infrequent.rrd")
		}

rrdFileList :: [FilePath]
rrdFileList = 
	let v = rrdFiles (allRras (StepLen (Seconds 0)))
	in map (snd . getFrequent Frequent) v ++ 
		map (snd . getFrequent Infrequent) v
