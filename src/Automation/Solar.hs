module Automation.Solar where

import Automation.Types
import Automation.Constants
import Automation.Utility
import DataUnits
import qualified Epsolar
import qualified Batteries

import Reactive.Banana
import Reactive.Banana.Automation
import Data.Functor.Compose

-- | Combined input watts reported by all charge controllers.
--
-- If some charge controllers are not available, use the ones that are
-- available.
ccInputWatts :: Automation (Sensors t) actuators (Behavior (Sensed Watts))
ccInputWatts = getCompose $ calc
	<$> Compose (sensedBehavior cc1InputWatts)
	<*> Compose (sensedBehavior cc2InputWatts)
	<*> Compose (sensedBehavior cc3InputWatts)
	<*> Compose (sensedBehavior cc4InputWatts)
	<*> Compose (sensedBehavior cc5InputWatts)
  where
	calc SensorUnavailable SensorUnavailable SensorUnavailable SensorUnavailable SensorUnavailable = SensorUnavailable
	calc c1 c2 c3 c4 c5 = Sensed $ sum $ map go [c1, c2, c3, c4, c5]
	go (Sensed v) = v
	go SensorUnavailable = Watts 0

ccKwhGeneratedToday :: Automation (Sensors t) actuators (Behavior (Sensed KWH))
ccKwhGeneratedToday = getCompose $ calc
	<$> Compose (sensedBehavior cc1KwhGeneratedToday)
	<*> Compose (sensedBehavior cc2KwhGeneratedToday)
	<*> Compose (sensedBehavior cc3KwhGeneratedToday)
	<*> Compose (sensedBehavior cc4KwhGeneratedToday)
	<*> Compose (sensedBehavior cc5KwhGeneratedToday)
  where
	calc SensorUnavailable SensorUnavailable SensorUnavailable SensorUnavailable SensorUnavailable = SensorUnavailable
	calc c1 c2 c3 c4 c5 = Sensed $ sum $ map go [c1, c2, c3, c4, c5]
	go (Sensed v) = v
	go SensorUnavailable = KWH 0

data SolarEstimate = SolarEstimate
	{ sunnyDay :: Bool
	, inAbsorb :: Bool
	}
	deriving (Show)

solarEstimate :: Automation (Sensors t) Actuators (Behavior SolarEstimate)
solarEstimate = getCompose $ SolarEstimate
	<$> Compose isSunnyDay
	<*> Compose verifiedInAbsorb

-- | Detect absorb stage of battery charging, when watts may be
-- low, but more could be produced with more load.
--
-- Checked once per minute, and is only true if the criteria have been met
-- for the past 5 minutes.
verifiedInAbsorb :: Automation (Sensors t) Actuators (Behavior Bool)
verifiedInAbsorb = fmap (fmap and) . rollingBuffer 5 
	=<< rateLimitMinute 1
	=<< probablyInAbsorb

probablyInAbsorb :: Automation (Sensors t) Actuators (Behavior Bool)
probablyInAbsorb = getCompose $ calc
	<$> Compose (sensedBehavior ccChargingStatus)
	-- The charge controller does not estimate percent well for
	-- battleborn batteries, but this is trying to predict what mode
	-- it's in, so use the percent charge it thinks it has.
	<*> Compose (sensedBehavior ccBatteryPercent)
	-- Buffer the previous voltage reading, so a momentary dip in
	-- voltage does not affect this.
	<*> Compose (rollingBuffer 2 =<< sensedEvent ccBatteryVoltage)
	<*> Compose (sensedBehavior cc1InputWatts)
	<*> Compose (pure (pure (Epsolar.Load houseIdleWatts)))
  where
	calc (Sensed ccstatus) (Sensed ccpercentcharge) recentvoltages (Sensed wattsproduced) wattsload =
		any f recentvoltages
	  where
		f volts = Epsolar.probablyInAbsorb ccstatus ccpercentcharge volts wattsproduced wattsload
	calc _ _ _ _ _ = False

-- | Is it a sunny day and the battery is close to fully charged, or seems
-- likely to get fully charged soon?
--
-- This is rate limited, so it will only change every 15 minutes, to avoid
-- flipping between states too fast when it's partly cloudy.
isSunnyDay :: Automation (Sensors t) Actuators (Behavior Bool)
isSunnyDay = do
	b <- getCompose $ calc
		<$> Compose (sensedBehavior trimetricBatteryPercent)
		<*> Compose (averageOverMinutes 30 =<< sensedBehavior trimetricBatteryWatts)
	automationStepper False =<< rateLimitMinute 15 b
  where
	calc (Sensed batterypercent) avgwatts = or
		-- Fully charged, or very close to it.
		[ batterypercent `inRange` Batteries.fullyCharged
		-- Well on its way to charging, with enough watts coming
		-- in to get fully charged within an hour or so.
		, batterypercent `inRange` Batteries.mostlyCharged
			&& overHistory False avgwatts (> Sensed (Watts 100))
		]
	calc _ _ = False

-- | Keep a rolling average of battery voltage over a 5 minute period,
-- and enter low power mode when it gets too low. Since battery
-- voltage can dip under load (lead acid much worse than lithium ion, but
-- still potentially a small dip for lithium), this is better than using
-- a single measurement.
--
-- Uses both the voltage read by the charge controller and by trimetric,
-- so either data source can fail without forcing low power mode.
--
-- Also, when the trimetric reports the battery is not well enough charged,
-- stay in low power mode.
lowpowerMode :: Automation (Sensors t) Actuators (Behavior Bool)
lowpowerMode = getCompose $ calc
	<$> Compose (averageOverMinutes 5 =<< sensedBehavior ccBatteryVoltage)
	<*> Compose (averageOverMinutes 5 =<< sensedBehavior trimetricBatteryVoltage)
	<*> Compose (sensedBehavior ccBatteryVoltage)
	<*> Compose (sensedBehavior trimetricBatteryVoltage)
	<*> Compose (sensedBehavior trimetricBatteryPercent)
  where
	calc _ _ (Sensed curvoltage) _ _
		| curvoltage <= Batteries.dangerZone = True
	calc _ _ _ (Sensed curvoltage) _
		| curvoltage <= Batteries.dangerZone = True
	calc _ _ _ _ (Sensed batterypercent)
		| batterypercent `inRange` Batteries.badlyCharged = True
	calc (History (Sensed v)) _ _ _ _ = v < Batteries.minimumSafe
	calc _ (History (Sensed v)) _ _ _ = v < Batteries.minimumSafe
	-- When battery voltage is not being sensed from either source,
	-- default to low power mode for safety.
	calc (History SensorUnavailable) (History SensorUnavailable) _ _ _ = True
	-- Just starting out, no value known yet.
	calc _ _ _ _ _ = False

batteryAmps :: Automation (Sensors t) Actuators (Behavior (Sensed Amps))
batteryAmps = getCompose $ calc
	<$> Compose (sensedBehavior trimetricBatteryVoltage)
	<*> Compose (sensedBehavior trimetricBatteryWatts)
  where
	calc (Sensed (Volts v)) (Sensed (Watts w)) = Sensed (Amps (w/v))
	calc _ _ = SensorUnavailable
