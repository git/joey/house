module Automation.Constants where

import DataUnits

import Reactive.Banana.Automation

-- | Minimum temperature the fridge is allowed to run at.
-- This is a little lower than fridgeIdealTemp so the fridge does
-- not teeter on the edge between the two.
fridgeMinimumTemp :: Temperature
fridgeMinimumTemp = TemperatureCelsius 2

-- | A regular fridge keeps food within this range.
--
-- (A little bit colder is ok, but the fridge interior can keep cooling
-- for a while after it shuts down, and temperature is measured at the top
-- but the bottom is colder, so stopping at 2.5C is wise.)
fridgeIdealTemp :: Range Temperature
fridgeIdealTemp = Range (TemperatureCelsius 2.5) (TemperatureCelsius 4.5)

-- | Since the fridge can lose power at any time, better to keep it toward
-- the cold end of the fridgeIdealTemp range when possible. Targeting
-- this helps push down the temperature of the thermal mass (water) in
-- the fridge, so it will hold a good temperature for longer.
--
-- The fridge only turns on when it's within or above this range,
-- but it will keep running down to the lower end of fridgeIdealTemp,
-- so the small difference in the lower end avoids short runs.
fridgePreferredTemp :: Range Temperature
fridgePreferredTemp = Range (TemperatureCelsius 3) (TemperatureCelsius 3.5)

-- | I'm willing to let the food get a little warmer, rather than
-- flattening the battery. This is still fairly safe; milk will
-- last over a week in this range.
fridgeAllowedTemp :: Range Temperature
fridgeAllowedTemp = extendRange fridgeIdealTemp (TemperatureCelsius 10)

-- | When nothing in the house is producing much load, and the batteries
-- are full, it can be difficult to determine if the solar panels are
-- getting good sun or not, since the watts will be low. This is an
-- estimate of the house's idle draw in such a situation. If it's
-- producing this many, it can probably produce more with a load.
--
-- This could be handled better by putting a photosensor near the panels.
--
-- Or, perhaps by observing the input voltage from the panels. This
-- should be higher when the sun is on them, I think.
houseIdleWatts :: Watts
houseIdleWatts = Watts 25

-- | Watts the fridge consumes when running, approximately.
fridgeWatts :: Watts
fridgeWatts = Watts 60

