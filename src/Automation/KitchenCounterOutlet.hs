module Automation.KitchenCounterOutlet where

import Automation.Types
import Automation.Utility
import Sensors

import Reactive.Banana
import Reactive.Banana.Automation

-- | The kitchen counter outlet is controlled by a relay.
controlKitchenCounterOutlet :: Automation (Sensors t) Actuators ()
controlKitchenCounterOutlet = do
	b <-kitchenCounterOutletBehavior
	actuateBehavior b KitchenCounterOutletRelay

-- Behavior of the kitchen counter outlet, as controlled by its switch,
-- or as overridden.
kitchenCounterOutletBehavior :: Automation (Sensors t) Actuators (Behavior PowerChange)
kitchenCounterOutletBehavior = do
	b <- kitchenCounterOutletSwitchBehavior
	applyOverrideM b kitchenCounterOutletOverride id

-- | The kitchen counter outlet has a switch that toggles the power.
-- The switch can only turn it on for 1.5 hours at a time, since other
-- switchable AC loads are disabled when it's on, and forgetting to manually
-- turn it off should not prevent things like the fridge from running.
kitchenCounterOutletSwitchBehavior :: Automation (Sensors t) Actuators (Behavior PowerChange)
kitchenCounterOutletSwitchBehavior = do
	switched <- getEventFrom kitchenCounterSwitchSetTo
	clock <- getEventFrom currentTime
	e <- accumE (False, Nothing) $
		unionWith (\a _b -> a)
			(switch <$> switched)
			(timeout <$> clock)
	automationStepper PowerOff (power <$> e)
  where
	-- Turn on when there is a change to the switch position.
	switch (Sensed (curr, starttime)) oldv@(False, Just (prev, _, _, _, _))
		| curr /= prev = (True, Just (curr, curr, curr, curr, starttime))
		| otherwise = oldv
	-- Reading the GPIO port sometimes indicates a change in the switch
	-- positiom when it did not really change at all. Why is unknown,
	-- but to work around it, require 3 reads in a row of the same
	-- position to turn it off.
	switch (Sensed (curr, starttime)) (True, Just (prev1, prev2, prev3, prev4, oldtime))
		| curr /= prev4 && curr == prev1 && curr == prev2 && curr == prev3 = 
			(False, Just (curr, curr, curr, curr, starttime))
		| otherwise =
			(True, Just (curr, prev1, prev2, prev3, oldtime))
	-- At startup, there's one synthetic switch event when the current
	-- position of the switch is seen for the first time. Ignore that.
	switch (Sensed (curr, starttime)) (_, Nothing) =
		(False, Just (curr, curr, curr, curr, starttime))
	-- A failure to read the position of the switch does
	-- not turn it off. The timeout will handle that worst case,
	-- or more likely the switch will be successfully read later
	-- and any change handled then.
	switch SensorUnavailable v = v

	timeout (ClockSignal currtime) (True, Just oldv@(_, _, _, _, starttime))
		| currtime >= starttime + maxtime = (False, Just oldv)
	timeout _ v = v

	maxtime = 60 * 90

	power (True, _) = PowerOn
	power (False, _) = PowerOff

-- | The kitchen counter outlet is used for intermittent high-power loads,
-- so when it's on, other switchable AC loads are turned off, using this.
offWhenKitchenCounterOutletOn :: (PowerChange -> a) -> Automation (Sensors t) Actuators (Behavior a) -> Automation (Sensors t) Actuators (Behavior a)
offWhenKitchenCounterOutletOn f getb = do
	switchb <- kitchenCounterOutletSwitchBehavior
	b <- getb
	let b' = switchturnsoff <$> b <*> switchb
	o <- filterE want <$> getEventFrom kitchenCounterOutletOverride
	applyOverrideM' b' o conv
  where
	conv PowerOn = f PowerOff
	conv PowerOff = f PowerOn

	switchturnsoff _ PowerOn = f PowerOff
	switchturnsoff a PowerOff = a

	-- If the kitchen counter outlet is overridden off,
	-- avoid forcing the other load on.
	want (OverrideSeconds _ PowerOff) = False
	want (OverrideSeconds _ PowerOn) = True
	want DisableOverride = True 
