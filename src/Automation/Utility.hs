{-# LANGUAGE LambdaCase #-}

module Automation.Utility where

import Automation.Types
import DataUnits
import Sensors

import Reactive.Banana
import Reactive.Banana.Automation
import Reactive.Banana.Frameworks (changes, reactimate', newEvent)
import Data.Time.Clock.POSIX
import Data.Time.Calendar
import Data.Time.LocalTime
import Data.Maybe
import Data.List

data Annotated t = Annotated [String] t

instance Show t => Show (Annotated t) where
	show (Annotated l t) = unwords l ++ ':' : ' ' : show t

fromAnnotated :: Annotated t -> t
fromAnnotated (Annotated _ t) = t

-- | A value that is calulated from some history of input values.
-- Before any input values are availably, it is NoHistory.
--
-- Using this instead of Maybe avoids mistakes such as 
-- avgvolts < Just (Volts 24), which would be wrong before
-- an average is available.
data History v = NoHistory | History v
	deriving (Show, Eq)

overHistory :: a -> History v -> (v -> a) -> a
overHistory a NoHistory _ = a
overHistory _ (History v) f = f v

-- | Average of the value of a sensor sampled once per minute over the
-- past n minutes. (Or over less time when just starting to get data.)
-- 
-- If the sensor is unavailable in a sample, fewer values will be averaged.
-- If the sensor is unavailable for all n samples, the value will be
-- SensorUnavailable.
averageOverMinutes
	:: (Num n, Fractional n)
	=> Int
	-> Behavior (Sensed n)
	-> Automation (Sensors t) actuators (Behavior (History (Sensed n)))
averageOverMinutes n b =
	fmap (fmap avg) . rollingBuffer n =<< rateLimitMinute 1 b
  where
	avg l
		| null l = NoHistory
		| otherwise =
			let l' = flip mapMaybe l $ \case
				Sensed v -> Just v
				SensorUnavailable -> Nothing
			in if null l'
				then History SensorUnavailable
				else History $
					Sensed (sum l' / genericLength l')

-- | Buffers the past n values of an Event.
-- 
-- >>> let test = flip actuateBehavior (Debug "result" . show) =<< rollingBuffer 2 =<< getEventFrom inputWatts
-- >>> runner <- observeAutomation test testSensors
-- >>> runner $ \sensors -> inputWatts sensors =: Watts 1.0
-- [Debug result: [Sensed (Watts 1.0)]]
-- >>> runner $ \sensors -> inputWatts sensors =: Watts 1.0
-- [Debug result: [Sensed (Watts 1.0),Sensed (Watts 1.0)]]
-- >>> runner $ \sensors -> inputWatts sensors =: Watts 2.0
-- [Debug result: [Sensed (Watts 2.0),Sensed (Watts 1.0)]]
-- >>> runner $ \sensors -> inputWatts sensors =: Watts 3.0
-- [Debug result: [Sensed (Watts 3.0),Sensed (Watts 2.0)]]
rollingBuffer
	:: Int
	-> Event a
	-> Automation sensors actuators (Behavior [a])
rollingBuffer n e = accumB [] $ rollbuf <$> e
  where
	rollbuf v l
		| length l < n || null l = v : l
		| otherwise = v : init l

-- Uses my personal time zone, no matter how the server is configured.
-- https://joeyh.name/blog/entry/howto_create_your_own_time_zone/
jest :: TimeZone
jest = TimeZone (-4*60) False "JEST"

-- | The local calendar Day.
calendarDayJoey :: Automation (Sensors t) actuators (Behavior (Maybe (ClockSignal Day)))
calendarDayJoey = automationStepper Nothing
	=<< (fmap calc <$> getEventFrom currentTime)
  where
	calc (ClockSignal t) = Just $ ClockSignal $ localDay $
		utcToLocalTime jest $ posixSecondsToUTCTime t

getCurrentHour :: Automation (Sensors t) actuators (Behavior (Maybe (ClockSignal Int)))
getCurrentHour =
	automationStepper Nothing . fmap calc
		=<< getEventFrom currentTime
  where
	calc (ClockSignal t) = Just $ ClockSignal $ todHour $ localTimeOfDay $
		utcToLocalTime jest $ posixSecondsToUTCTime t

getCurrentMinute :: Automation (Sensors t) actuators (Behavior (Maybe (ClockSignal Int)))
getCurrentMinute =
	automationStepper Nothing . fmap calc
		=<< getEventFrom currentTime
  where
	calc (ClockSignal t) = Just $ ClockSignal $ todMin $ localTimeOfDay $
		utcToLocalTime jest $ posixSecondsToUTCTime t

getCurrentSecond :: Automation (Sensors t) actuators (Behavior (Maybe (ClockSignal Int)))
getCurrentSecond =
	automationStepper Nothing . fmap Just
		=<< getEventFrom secondHand

-- | Makes an Event only trigger on minutes divisible by n.
--
-- Note that the Event will occur on *every* minute divisible
-- by n, even if the Behavior normally changes less often.
--
-- Assumes that the minuteHand is updated once per minute without skipping
-- any, although skipping non-divisible minutes during testing is ok.
rateLimitMinute :: Int -> Behavior a -> Automation (Sensors t) actuators (Event a)
rateLimitMinute = rateLimitTime minuteHand

rateLimitSecond :: Int -> Behavior a -> Automation (Sensors t) actuators (Event a)
rateLimitSecond = rateLimitTime secondHand

rateLimitTime
	:: (Sensors t -> EventSource (ClockSignal Int) v)
	-> Int
	-> Behavior a
	-> Automation (Sensors t) actuators (Event a)
rateLimitTime f n b = do
	seconds <- filterE wanted <$> getEventFrom f
	return $ b <@ seconds
  where
	wanted (ClockSignal m)
		| n == 0 = True
		| otherwise = rem m n == 0

-- | Allows an Behavior to be overridden.
--
-- Overrides take effect immediately, and continue until disabled.
--
-- The Behavior will change at the same frequency of the input event,
-- plus overrides.
applyOverrideM
	:: Behavior a
	-> (Sensors t -> EventSource (Override o) t)
	-> (o -> a)
	-> Automation (Sensors t) actuators (Behavior a)
applyOverrideM b getoverride convoverride = do
	o <- getEventFrom getoverride
	applyOverrideM' b o convoverride

applyOverrideM'
	:: Behavior a
	-> Event (Override o)
	-> (o -> a)
	-> Automation sensors actuators (Behavior a)
applyOverrideM' b override convoverride = do
	o <- overrideBehavior override
	return $ applyOverride convoverride <$> o <*> b

overrideBehavior
	:: Event (Override a)
	-> Automation sensors actuators (Behavior (Override a))
overrideBehavior = automationStepper DisableOverride

data Delay = DelaySeconds Double | DelayMinutes Double

-- Calcular an overall behavior from a base, applying an override, and
-- optionally limiting the rate of change to once per some number of minutes.
overallBehavior
	:: Automation (Sensors t) actuators (Behavior (Maybe v))
	-> Maybe Delay
	-> (Sensors t -> EventSource (Override o) t)
	-> (o -> v)
	-> Automation (Sensors t) actuators (Behavior (Maybe v))
overallBehavior getbase rate override convoverride = do
	b <- rateLimitBehavior getbase rate
	applyOverrideM b override (Just . convoverride)

rateLimitBehavior
	:: Automation (Sensors t) actuators (Behavior (Maybe v))
	-> Maybe Delay
	-> Automation (Sensors t) actuators (Behavior (Maybe v))
rateLimitBehavior getbase rate = do
	b <- getbase
	case rate of
		Just (DelayMinutes m) -> 
			automationStepper Nothing
				=<< rateLimitMinute (floor m) b
		Just (DelaySeconds n) -> 
			automationStepper Nothing
				=<< rateLimitSecond (floor n) b
		Nothing -> return b
	
-- | Accumulate PowerChanges, to determine what the power is set to.
--
-- Provide an initial PowerSetting which is used only until there's a
-- PowerChange.
powerSettingBehavior
	:: PowerSetting
	-> Behavior (Maybe PowerChange)
	-> Automation sensors actuators (Behavior PowerSetting)
powerSettingBehavior initst b =
	powerSettingBehavior' b >>= return . fmap (fromMaybe initst)

-- | Accumulate PowerChanges, with no initial value.
powerSettingBehavior'
	:: Behavior (Maybe PowerChange)
	-> Automation sensors actuators (Behavior (Maybe PowerSetting))
powerSettingBehavior' b = do
	-- Normally, reactive-banana does not let an Event be made from a
	-- Behavior. But we need an Event in order to accumulate over it.
	--
	-- See Reactive.Banana.Frameworks.changes for discussion of when
	-- it's safe to do this.
	--
	-- I think this use of it is safe, because the Event that is
	-- generated is only used in a way that does not distinguish
	-- between a Behavior that happens to set eg Just PowerOn twice
	-- in a row with one that sets it once and leaves it on.
	-- And since the Event gets converted back to a Behavior by accumB,
	-- it never escapes this function.
	(e, handle) <- liftMomentIO newEvent
	eb <- liftMomentIO $ changes b
	liftMomentIO $ reactimate' $ (fmap handle) <$> eb
	
	accumB Nothing (flip calc <$> e)
  where
	calc st = maybe st (Just . powerChangeToPowerSetting)

-- | Filters out events that repeat a prior value, looking back at the
-- past N events.
--
-- >>> let test = flip actuateEvent (Debug "result" . show) =<< filterDups 1 =<< getEventFrom inputWatts
-- >>> runner <- observeAutomation test testSensors
-- >>> runner $ \sensors -> inputWatts sensors =: Watts 1.0
-- [Debug result: Sensed (Watts 1.0)]
-- >>> runner $ \sensors -> inputWatts sensors =: Watts 1.0
-- []
-- >>> runner $ \sensors -> inputWatts sensors =: Watts 1.0
-- []
-- >>> runner $ \sensors -> inputWatts sensors =: Watts 2.0
-- [Debug result: Sensed (Watts 2.0)]
-- >>> runner $ \sensors -> inputWatts sensors =: Watts 1.0
-- [Debug result: Sensed (Watts 1.0)]
filterDups :: Eq a => Int -> Event a -> Automation sensors actuators (Event a)
filterDups = filterDups' (==)

filterDups' :: (a -> a -> Bool) -> Int -> Event a -> Automation sensors actuators (Event a)
filterDups' isdup n base = do
	b <- fmap differ <$> rollingBuffer n base
	return $ filterApply b base
  where
	differ l v = not (any (isdup v) l)
