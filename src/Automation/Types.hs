module Automation.Types where

import DataUnits
import Sensors
import GPIO
import qualified Epsolar

import Reactive.Banana.Automation
import Data.Time.Clock
import Data.Time.Clock.POSIX
import Data.Time.Calendar

data Sensors t = Sensors
	{ ccBatteryVoltage :: EventSource (Sensed Volts) t
	, trimetricBatteryVoltage :: EventSource (Sensed Volts) t
	, trimetricBatteryPercent :: EventSource (Sensed Percentage) t
	, trimetricBatteryWatts :: EventSource (Sensed Watts) t
	, ccBatteryPercent :: EventSource (Sensed Percentage) t
	, cc1InputWatts :: EventSource (Sensed Watts) t
	, cc2InputWatts :: EventSource (Sensed Watts) t
	, cc3InputWatts :: EventSource (Sensed Watts) t
	, cc4InputWatts :: EventSource (Sensed Watts) t
	, cc5InputWatts :: EventSource (Sensed Watts) t
	, cc1KwhGeneratedToday :: EventSource (Sensed KWH) t
	, cc2KwhGeneratedToday :: EventSource (Sensed KWH) t
	, cc3KwhGeneratedToday :: EventSource (Sensed KWH) t
	, cc4KwhGeneratedToday :: EventSource (Sensed KWH) t
	, cc5KwhGeneratedToday :: EventSource (Sensed KWH) t
	, ccChargingStatus :: EventSource (Sensed Epsolar.ChargingStatus) t
	, fridgeRelaySetTo :: EventSource (Sensed (PowerSetting, POSIXTime)) t
	, porchTemperature :: EventSource (Sensed Temperature) t
	, fridgeTemperature :: EventSource (Sensed Temperature) t
	, kitchenCounterSwitchSetTo :: EventSource (Sensed (GPIOValue, POSIXTime)) t
	, minuteHand :: EventSource (ClockSignal Int) t
	, secondHand :: EventSource (ClockSignal Int) t
	, currentTime :: EventSource (ClockSignal POSIXTime) t
	, fridgeOverride :: EventSource (Override PowerChange) t
	, inverterOverride :: EventSource (Override PowerChange) t
	, springPumpOverride :: EventSource (Override PowerChange) t
	, springPumpRunCycle :: EventSource RunCycle t
	, kitchenCounterOutletOverride :: EventSource (Override PowerChange) t
	, carChargerOverride :: EventSource (Override PowerChange) t
	, carChargerEnabled :: EventSource (Sensed Bool) t
	, carStateOfCharge :: EventSource (Sensed Percentage) t
	, carChargeLimit :: EventSource (Sensed Percentage) t
	, carPluggedIn :: EventSource (Sensed Bool) t
	, lastKnownFridgeRuntimeToday :: EventSource (Sensed (Hours, Maybe Day)) t
	}

mkSensors :: IO t -> IO (Sensors t)
mkSensors mkt = Sensors
	<$> yada
	<*> yada
	<*> yada
	<*> yada
	<*> yada
	<*> yada
	<*> yada
	<*> yada
	<*> yada
	<*> yada
	<*> yada
	<*> yada
	<*> yada
	<*> yada
	<*> yada
	<*> yada
	<*> yada
	<*> yada
	<*> yada
	<*> yada
	<*> yada
	<*> yada
	<*> yada
	<*> yada
	<*> yada
	<*> yada
	<*> yada
	<*> yada
	<*> yada
	<*> yada
	<*> yada
	<*> yada
	<*> yada
	<*> yada
  where
	yada = newEventSource =<< mkt

testSensors :: IO (Sensors ())
testSensors = mkSensors (return ())

data Actuators
	= InverterPower PowerChange
	| FridgeRelay PowerChange
	| SpringPumpFloatSwitch PowerChange
	| KitchenCounterOutletRelay PowerChange
	| CarCharger PowerChange
	| UsbHubsRelay PowerChange
	| Debug String String
	| OverallStatus [Status]
	| FridgeRuntimeToday (Hours, Maybe Day)
	| FridgeRunDuration Minutes
	| ActuatorSequence [ActuatorSequence]

data ActuatorSequence
	= InSequence Actuators
	| PauseFor NominalDiffTime

instance Show Actuators where
	show (InverterPower p) = "InverterPower: " ++ show p
	show (FridgeRelay p) = "FridgeRelay: " ++ show p
	show (SpringPumpFloatSwitch p) = "SpringPumpFloatSwitch: " ++ show p
	show (KitchenCounterOutletRelay p) = "KitchenCounterOutletRelay: " ++ show p
	show (CarCharger p) = "CarCharger: " ++ show p
	show (UsbHubsRelay p) = "UsbHubsRelay: " ++ show p
	show (Debug s v) = "Debug " ++ s ++ ": " ++ v
	show (OverallStatus l) = "OverallStatus: " ++ show l
	show (FridgeRuntimeToday v) = "FridgeRuntimeToday: " ++ show v
	show (FridgeRunDuration v) = "FridgeRunDuration: " ++ show v
	show (ActuatorSequence l) = show l

instance Show ActuatorSequence where
	show (InSequence a) = show a
	show (PauseFor n) = "PauseFor " ++ show n
