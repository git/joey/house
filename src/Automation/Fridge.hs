{-# LANGUAGE LambdaCase, BangPatterns #-}

module Automation.Fridge where

import Automation.Types
import Automation.Constants
import Automation.Utility
import Automation.Solar
import Automation.KitchenCounterOutlet
import Automation.Car
import DataUnits
import qualified Batteries
import qualified Sensors

import Reactive.Banana
import Reactive.Banana.Automation
import Data.Time.Clock.POSIX
import Data.Time.Calendar
import Data.Functor.Compose
import Data.Maybe

-- | Overall behavior of the fridge.
fridgeBehavior :: FridgeThoughts -> Maybe Delay -> Automation (Sensors t) Actuators (Behavior (Maybe PowerChange))
fridgeBehavior t d = 
	offWhenCarCharging Just $ 
		offWhenKitchenCounterOutletOn Just $
			overallBehavior (pure (fridgeWanted t)) d fridgeOverride id

fridgeWanted :: FridgeThoughts -> Behavior (Maybe PowerChange)
fridgeWanted (FridgeThoughts t) = fmap fromAnnotated t

newtype FridgeThoughts = FridgeThoughts (Behavior (Annotated (Maybe PowerChange)))

-- | Does the fridge want to run?
--
-- Mostly full batteries are prioritized over running the fridge,
-- but the warmer the fridge is, the deeper into batteries it's
-- willing to go.
fridgeThoughts :: Automation (Sensors t) Actuators FridgeThoughts
fridgeThoughts = FridgeThoughts <$> go
  where
	go = getCompose $ calc
		<$> Compose lowpowerMode
		<*> Compose (clockSignalBehavior currentTime)
		<*> Compose (sensedBehavior trimetricBatteryPercent)
		<*> Compose (averageOverMinutes 10 =<< sensedBehavior trimetricBatteryVoltage)
		<*> Compose (averageOverMinutes 10 =<< sensedBehavior trimetricBatteryWatts)
		<*> Compose (averageOverMinutes 10 =<< ccInputWatts)
		<*> Compose ccKwhGeneratedToday
		<*> Compose (sensedBehavior fridgeTemperature)
		<*> Compose solarEstimate
		<*> Compose (sensedBehavior fridgeRelaySetTo)
	calc lowpower now (Sensed battery) avgvoltage avgbatterywatts avginputwatts kwhtoday (Sensed temp) solarestimate relaysetting =
		let v@(Annotated l mp) = calc' lowpower battery avgvoltage avgbatterywatts avginputwatts kwhtoday temp solarestimate
		-- Avoid turning back on until 10 minutes after it
		-- last turned off, to avoid wear and tear on the
		-- compressor.
		in case (relaysetting, now, mp) of
			(Sensed (PowerSetting False, offtime), Just (ClockSignal nowtime), Just PowerOn)
				| nowtime - offtime > 60*10 -> v
				| otherwise -> Annotated (["Next state: "] ++ l) Nothing
			_ -> v
	calc _lowpower _now SensorUnavailable _avgvoltage _avgbatterywatts _avginputwatts _kwhtoday _temp _solarestimate _relaysetting = Annotated
		[ "Battery status unknown! Risking spoiling food." ]
		(Just PowerOff)
	-- TODO perhaps run the fridge for an hour or so a day
	-- when its temperature is not known, hoping to keep
	-- it close to the allowed range.
	calc _lowpower now _battery _avgvoltage _avgbatterywatts _avginputwatts _kwhtoday SensorUnavailable _solarestimate relaysetting  =
		let v = Annotated
			[ "Temperature unknown!" ]
			(Just PowerOff)
		-- Avoid turning off due to unknown temperature immediately
		-- after turning on. Sometimes the motor spinup load can
		-- cause a few temperature reads to fail.
		in case (relaysetting, now) of
			(Sensed (PowerSetting True, ontime), Just (ClockSignal nowtime))
				| nowtime - ontime > 60*5 -> v
				| otherwise -> Annotated [ "Temperature unknown, waiting for sensor read" ] Nothing
			_ -> v
	
	calc' lowpower battery avgvoltage avgbatterywatts avginputwatts kwhtoday temp solarestimate
		| temp < fridgeMinimumTemp = thought
			["much too cold, food in danger of freezing soon"]
			(Just PowerOff)
		| lowpower = thought ["low power mode"] (Just PowerOff)
		| temp `belowRange` fridgeAllowedTemp = thought
			["too cold, food in danger of freezing"]
			(Just PowerOff)
		| battery `inRange` Batteries.fullyCharged
			&& not (temp `belowRange` fridgePreferredTemp) = thought
			["battery is full"] (Just PowerOn)
		| overHistory False avgwatts (<= Sensed houseIdleWatts)
			&& battery `aboveRange` Batteries.mostlyCharged = thought
				["battery is full, but solar panels appear to be shaded"]
				(Just PowerOff)
		| battery `aboveRange` Batteries.mostlyCharged
			&& overHistory False avgvoltage (<= Sensed Batteries.lowFloat) = thought
				["battery is in float, but its voltage is getting too low"]
				(Just PowerOff)
		| inAbsorb solarestimate = thought
			[ "battery is charging in absorb stage,"
			, "running will probably produce more watts"
			]
			(Just PowerOn)
		| temp `aboveRange` fridgeAllowedTemp
			&& battery `aboveRange` Batteries.badlyCharged = thought
				[ "much too warm, running despite batteries"
				, "only being a little bit charged"
				]
				(Just PowerOn)
		| overHistory False avgwatts (<= Sensed fridgeWatts)
			&& not (battery `aboveRange` Batteries.mostlyCharged) = thought
				[ "batteries are charging and the solar panels are"
				, "not producing enough watts to run me"
				]
				(Just PowerOff)
		| temp `belowRange` fridgePreferredTemp
			&& battery `aboveRange` Batteries.mostlyCharged = thought
			[ "battery is mostly full; getting quite cold;"
			, "keep running if I was running"
			]
			Nothing
		| battery `aboveRange` Batteries.mostlyCharged = thought
			[ "banking cold since the battery is mostly full" ]
			(Just PowerOn)
		| temp `aboveRange` fridgePreferredTemp
			&& (kwhtoday >= Sensed (KWH 0.5) || kwhtoday == SensorUnavailable)
			&& overHistory False avgwatts (>= Sensed (Watts 150))
			&& battery `aboveRange` Batteries.badlyCharged = thought
				[ "a little too warm; batteries only a little"
				, "bit charged, but getting good power"
				]
				(Just PowerOn)
		| temp `aboveRange` fridgePreferredTemp
			&& battery `aboveRange` Batteries.badlyCharged = thought
				[ "a little too warm, battery only a little"
				, "bit charged; keep running if I am running"
				, "but don't turn on otherwise"
				] Nothing
		| battery `belowRange` Batteries.mostlyCharged = thought
			[ "battery is not well charged and I'm not too"
			, "warm yet"
			]
			(Just PowerOff)
		| sunnyDay solarestimate = thought
			[ "banking cold on a sunny day"
			, "(battery is not full yet, but should be soon)"
			]
			(Just PowerOn)
		| kwhtoday >= Sensed (KWH 1) = thought
			[ "more than 1 kWH generated today,"
			, "and battery is mostly charged,"
			, "so it's not too cloudy; high power mode"
			]
			(Just PowerOn)
		| otherwise = thought
			[ "not too hot, not too cold, battery is charging up;"
			, "best action unclear; keep doing what I've"
			, "been doing"
			]
			Nothing
	  where
		thought = tempthought temp
		-- Prefer the watts being produced by the solar panels,
		-- but if that is not available directly, look at the watts
		-- going into the battery. The difference is that running
		-- the fridge affects the latter, so making the fridge
		-- behavior depend on it can cause the fridge to cycle on
		-- and off a bit more often.
		avgwatts
			| avginputwatts /= History SensorUnavailable = avginputwatts
			| otherwise = avgbatterywatts
		
	tempthought temp l = Annotated (l ++ [tempdelta])
	  where
		tempdelta = "(temp is " ++ showDelta showtemp delta ++
			"C from " ++ fromwhat ++ ")"
		showtemp (TemperatureCelsius t) = showDoublePrecision 2 t
		deltaallowed = rangeDelta temp fridgeAllowedTemp
		deltaideal = rangeDelta temp fridgeIdealTemp
		deltafreezing = rangeDelta temp (Range 0 0)
		(delta, fromwhat)
			| deltaallowed /= Delta 0 = (deltaallowed, "allowed range")
			| deltaideal /= Delta 0 = (deltaideal, "ideal range")
			| otherwise = (deltafreezing, "freezing")
	
statusFridgeThoughts :: FridgeThoughts -> Automation (Sensors t) Actuators (Behavior (Sensors.Status))
statusFridgeThoughts (FridgeThoughts t) = return $ 
	(Sensors.FridgeThoughts . show) <$> t

-- | Sums up the duration of the last run of the fridge. Does not
-- reset to 0 when it turns off, so that the poller can collect the data
-- without missing the final length of the run.
fridgeRunDuration :: FridgeThoughts -> Maybe Delay -> Automation (Sensors t) Actuators (Behavior Minutes)
fridgeRunDuration fridgethoughts d = do
	power <- powerSettingBehavior (PowerSetting False)
		=<< fridgeBehavior fridgethoughts d
	clock <- clockSignalBehavior currentTime
	calce <- rateLimitMinute 1 $ calc <$> power <*> clock
	fmap (secondsToMinutes . fst) <$> accumB initst calce
  where
	initst = (0, (0, False))

	calc (PowerSetting True) (Just (ClockSignal now)) (runtime, (prevtime, wasrunning)) =
		let !runtime' = if wasrunning
			then runtime + now - prevtime
			else 0
		in (runtime', (now, True))
	calc (PowerSetting False) _ (runtime, (prevtime, _wasrunning)) =
		(runtime, (prevtime, False))
	calc _ _ oldst = oldst

-- | Sums up how long the fridge has run, resetting at midnight.
--
-- This starts with the value of lastKnownFridgeRuntimeToday, so when the
-- automation is restarted, it will remember the old value it calculated,
-- as long as the poller is able to provide it.
fridgeRuntimeToday :: FridgeThoughts -> Maybe Delay -> Automation (Sensors t) Actuators (Behavior (Hours, Maybe Day))
fridgeRuntimeToday fridgethoughts d = do
	lastknown <- sensedBehavior lastKnownFridgeRuntimeToday
	clock <- declock $ clockSignalBehavior currentTime
	currday <- declock calendarDayJoey
	power <- powerSettingBehavior (PowerSetting False)
		=<< fridgeBehavior fridgethoughts d
	calce <- rateLimitMinute 1 $
		calc <$> lastknown <*> currday <*> clock <*> power
	fmap extract <$> accumB initst calce
  where
	declock = fmap $ fmap $ fmap (\(ClockSignal t) -> t)

	initst :: (Bool, Hours, Maybe Day, Maybe POSIXTime)
	initst = (False, Hours 0, Nothing, Nothing)

	extract :: (Bool, Hours, Maybe Day, Maybe POSIXTime) -> (Hours, Maybe Day)
	extract (_, n, day, _) = (n, day)

	calc :: (Sensed (Hours, Maybe Day)) -> Maybe Day -> Maybe POSIXTime -> PowerSetting -> (Bool, Hours, Maybe Day, Maybe POSIXTime) -> (Bool, Hours, Maybe Day, Maybe POSIXTime)
	calc lastknown currday clock power (seenlastknown, n, prevday, prevclock)
		| isNothing currday =
			(False, Hours 0, currday, clock)
		| currday /= prevday =
			(seenlastknown', lastknownadj, currday, clock)
		| otherwise =
			let elapsedtime = if power == PowerSetting True
				then secondsToHours $ fromMaybe 0 $
					(-) <$> clock <*> prevclock
				else Hours 0
			in (seenlastknown', lastknownadj + n + elapsedtime, prevday, clock)
	  where
		seenlastknown' = seenlastknown || case lastknown of
			SensorUnavailable -> False
			Sensed _ -> True
		lastknownadj
			| seenlastknown = Hours 0
			| otherwise = case lastknown of
				Sensed (lastn, lastd)
					| lastd == currday -> lastn
					| otherwise -> Hours 0
				SensorUnavailable -> Hours 0
