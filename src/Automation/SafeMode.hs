module Automation.SafeMode where

import GPIO
import GPIO.SamlexInverter
import DataUnits

import System.IO
import Control.Concurrent.Async

-- | Try to turn eveything off, used if the automation is failing for some
-- reason, or has been stopped.
--
-- The system could be in a degraded state where any of these actions
-- crash or get stuck, so run them all concurrently.
safeMode :: IO ()
safeMode = do
	a0 <- async $ hPutStrLn stderr "entering safe mode"
	a1 <- async $ setSamlexInverterPower off
	a2 <- async $ setGPIOPower relayA off
	a3 <- async $ setGPIOPower relayB off
	a4 <- async $ setGPIOPower relayC off
	a5 <- async $ setGPIOPower relayD off
	a6 <- async $ setGPIOPower relayE off
	a7 <- async $ setGPIOPower relayF off
	a8 <- async $ setSpringPumpPower off
	mapM_ waitCatch [a0, a1, a2, a3, a4, a5, a6, a7, a8]
  where
	off = PowerSetting False
