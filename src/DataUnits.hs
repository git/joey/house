{-# LANGUAGE DeriveGeneric, GeneralizedNewtypeDeriving #-}

module DataUnits where

import Text.Read
import Text.Printf
import Data.Aeson
import Data.Time.Clock
import Data.Time.Clock.POSIX
import GHC.Generics
import Reactive.Banana.Automation

class DataUnit t where
	-- | Parse a string, with a given number of decimals of precision.
	parseDataUnit :: Int -> String -> Maybe t
	formatDataUnit :: t -> String

newtype Volts = Volts Double
	deriving (Generic, Show, Eq, Ord, Num, Fractional)

instance DataUnit Volts where
	parseDataUnit = parseDouble Volts
	formatDataUnit (Volts n) = showDouble n

newtype Amps = Amps Double
	deriving (Generic, Show, Eq, Ord, Num, Fractional)

instance DataUnit Amps where
	parseDataUnit = parseDouble Amps
	formatDataUnit (Amps n) = showDouble n

newtype Watts = Watts Double
	deriving (Generic, Show, Eq, Ord, Num, Fractional)

instance DataUnit Watts where
	parseDataUnit = parseDouble Watts
	formatDataUnit (Watts n) = showDouble n

newtype KWH = KWH Double
	deriving (Generic, Show, Eq, Ord, Num, Fractional)

instance DataUnit KWH where
	parseDataUnit = parseDouble KWH
	formatDataUnit (KWH n) = showDouble n

newtype AH = AH Double
	deriving (Generic, Show, Eq, Ord, Num, Fractional)

instance DataUnit AH where
	parseDataUnit = parseDouble AH
	formatDataUnit (AH n) = showDouble n

newtype Temperature = TemperatureCelsius Double
	deriving (Generic, Show, Eq, Ord, Num, Fractional)

celsiusToFahrenheit :: Temperature -> Int
celsiusToFahrenheit (TemperatureCelsius c) = round
	(((fromInteger . round) ((c * 1.8 + 32) * 100)) / 100 :: Double)

instance DataUnit Temperature where
	parseDataUnit = parseDouble TemperatureCelsius
	formatDataUnit (TemperatureCelsius n) = showDouble n

newtype Percentage = Percentage Double
	deriving (Generic, Show, Eq, Ord, Num, Fractional)

instance DataUnit Percentage where
	parseDataUnit = parseDouble Percentage
	formatDataUnit (Percentage n) = showDouble n

newtype Hours = Hours Double
	deriving (Generic, Show, Eq, Ord, Num)

instance DataUnit Hours where
	parseDataUnit = parseDouble Hours
	formatDataUnit (Hours n) = showDouble n

secondsToHours :: NominalDiffTime -> Hours
secondsToHours d = Hours (fromRational (toRational d) / 3600)

newtype Minutes = Minutes Double
	deriving (Generic, Show, Eq, Ord, Num)

instance DataUnit Minutes where
	parseDataUnit = parseDouble Minutes
	formatDataUnit (Minutes n) = showDouble n

secondsToMinutes :: NominalDiffTime -> Minutes
secondsToMinutes d = Minutes (fromRational (toRational d) / 60)

newtype Miles = Miles Double
	deriving (Generic, Show, Eq, Ord, Num)

instance DataUnit Miles where
	parseDataUnit = parseDouble Miles
	formatDataUnit (Miles n) = showDouble n

-- | Degrees clockwise from north.
newtype AbsoluteBearing = AbsoluteBearing Double
	deriving (Generic, Show, Eq, Ord, Num)

newtype RelativeHumidity = RelativeHumidity Percentage
	deriving (Generic, Show, Eq, Ord, Num, Fractional)

instance DataUnit RelativeHumidity where
	parseDataUnit = parseDouble (RelativeHumidity . Percentage)
	formatDataUnit (RelativeHumidity (Percentage n)) = showDouble n

newtype InchesPerHour = InchesPerHour Double
	deriving (Generic, Show, Eq, Ord, Num)

instance DataUnit InchesPerHour where
	parseDataUnit = parseDouble InchesPerHour
	formatDataUnit (InchesPerHour n) = showDouble n

-- | Isomorphic to PowerChange from reactive-banana-automation, 
-- but a separate data type to ensure that json serialization remains
-- stable.
data PowerSetting = PowerSetting Bool
	deriving (Generic, Show, Eq, Ord)

powerSettingToPowerChange :: PowerSetting -> PowerChange
powerSettingToPowerChange (PowerSetting True) = PowerOn
powerSettingToPowerChange (PowerSetting False) = PowerOff

powerChangeToPowerSetting :: PowerChange -> PowerSetting
powerChangeToPowerSetting PowerOn = PowerSetting True
powerChangeToPowerSetting PowerOff = PowerSetting False

data UntilTime a = UntilTime
	{ untilTime :: POSIXTime
	, untilValue :: a
	}
	deriving (Generic, Show, Eq, Ord)

instance DataUnit a => DataUnit (UntilTime a) where
	parseDataUnit p s = UntilTime 0 <$> parseDataUnit p s
	formatDataUnit (UntilTime _ a) = formatDataUnit a

instance ToJSON Volts
instance FromJSON Volts
instance ToJSON Amps
instance FromJSON Amps
instance ToJSON Watts
instance FromJSON Watts
instance ToJSON KWH
instance FromJSON KWH
instance ToJSON AH
instance FromJSON AH
instance ToJSON RelativeHumidity
instance FromJSON RelativeHumidity
instance ToJSON Temperature
instance FromJSON Temperature
instance ToJSON Percentage
instance FromJSON Percentage
instance ToJSON Hours
instance FromJSON Hours
instance ToJSON Minutes
instance FromJSON Minutes
instance ToJSON Miles
instance FromJSON Miles
instance ToJSON AbsoluteBearing
instance FromJSON AbsoluteBearing
instance ToJSON InchesPerHour
instance FromJSON InchesPerHour
instance ToJSON PowerSetting
instance FromJSON PowerSetting
instance ToJSON a => ToJSON (UntilTime a)
instance FromJSON a => FromJSON (UntilTime a)

-- | Parse a string to a double, rounding to the given number of decimals.
parseDouble :: (Double -> t) -> Int -> String -> Maybe t
parseDouble mkt p s = mkt . setprecision <$> readMaybe s
  where
	setprecision d = (fromIntegral $ (round (d * v) :: Integer)) / v
	v = 10 ^ p

-- | Avoid exponential notation
showDouble :: Double -> String
showDouble = printf "%f"

-- | Avoid exponential notation and with some number of decimal digits.
showDoublePrecision :: Int -> Double -> String
showDoublePrecision ndecimals d = printf "%.*f" ndecimals d

data Delta t = Delta t
	deriving (Eq, Ord, Show)

-- | Show delta with + or - sign prefixed always.
showDelta :: (Num t, Ord t) => (t -> String) -> Delta t -> String
showDelta f (Delta t)
	| t > 0 = "+" ++ f t
	| otherwise = f t

rangeDelta :: (Num t, Ord t) => t -> Range t -> Delta t
rangeDelta t r@(Range n1 n2)
	| t `aboveRange` r = Delta (t - max n1 n2)
	| t `belowRange` r = Delta (t- min n1 n2)
	| otherwise = Delta 0
