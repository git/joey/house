-- | Poller for trimetric.

module Trimetric.Poller where

import Control.Monad

import Trimetric
import WebServers
import qualified Sensors
import Utility.ThreadScheduler

pollTrimetric :: Seconds -> AddSensedValue -> GetSensedValue -> IO ()
pollTrimetric howoften sensed getsendsensedvalue = do
	Trimetric.prepSampleTrimetricStream
	forever $ do
		l <- Trimetric.sampleTrimetricStream
		let report mk f = do
			let val = mk (findone l f)
			sensed val
			when (Sensors.sensorFailed val) $
				sensorFailure getsendsensedvalue sensed
		report Sensors.TrimetricBatteryPercent $ \case
			(TrimetricBatteryPercent v) -> Just v
			_ -> Nothing
		report Sensors.TrimetricBatteryWatts $ \case
			(TrimetricBatteryWatts v) -> Just v
			_ -> Nothing
		report Sensors.TrimetricBatteryVolts $ \case
			(TrimetricBatteryVolts v) -> Just v
			_ -> Nothing
		report Sensors.TrimetricCCStatusCode $ \case
			(TrimetricCCStatusCode v) -> Just v
			_ -> Nothing
		threadDelaySeconds howoften
  where
	findone [] _ = Nothing
	findone (v:vs) f = maybe (findone vs f) Just (f v)
