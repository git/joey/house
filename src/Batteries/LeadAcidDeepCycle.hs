module Batteries.LeadAcidDeepCycle where

import DataUnits

import Reactive.Banana.Automation

-- 23.5 is the danger zone for lead acid 24v nominal;
-- they should not be discharged beyond that point
-- at any time
dangerZone :: Volts
dangerZone = 23.5

-- 24.0 is the minimum I like to deplete deep cycle lead acid,
-- although short dips to 23.9 or so are acceptable.
minimumSafe :: Volts
minimumSafe = Volts 24.0

-- | Normal range of voltage when in float and the batteries are well
-- charged.
--
-- The Epsolar charge controller sometimes stays in float mode when the
-- batteries discharge some, but such lower voltages are not in this range.
--
-- (27.6 is the configured float voltage in the charge controller)
floatVoltage :: Range Volts
floatVoltage = Range (Volts 26.5) (Volts 30.0)

-- When the battery is being charged and is at or below this, it's
-- perhaps in float, but getting low so may be a good idea to discontinue
-- large loads that are not wanted to discharge the battery any.
lowFloat :: Volts
lowFloat = Volts 26.7

-- Range where the battery is pretty well charged, but not
-- entirely full yet. Normally the fridge only turns on
-- then the battery is above this range. Once on, it's allowed
-- to keep running until the battery falls below this range.
--
-- This prioritizes charging the battery over running the fridge,
-- while avoiding turning the fridge off for every passing cloud.
mostlyCharged :: Range Percentage
mostlyCharged = Range (Percentage 75) (Percentage 90)

-- When the fridge exceeds the allowed temp, it's worth running the
-- fridge rather than charging the batteries, as long as they remain
-- above this range.
badlyCharged :: Range Percentage
badlyCharged = Range (Percentage 0) (Percentage 50)
