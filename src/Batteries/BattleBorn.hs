module Batteries.BattleBorn where

import DataUnits

import Reactive.Banana.Automation

-- The battery percent charge can be determined roughly from the voltage,
-- but this is accurate only when the charge controller is not charging it.
-- 
-- Values provided by Battleborne
voltsToPercentage :: Volts -> Percentage
voltsToPercentage (Volts v)
	| v >= 27.2 = Percentage 100
	| v >= 26.8 = Percentage 99
	| v >= 26.6 = Percentage 90
	| v >= 26.4 = Percentage 70
	| v >= 26.2 = Percentage 40
	| v >= 26.0 = Percentage 30
	| v >= 25.8 = Percentage 20
	| v >= 25.6 = Percentage 17
	| v >= 25.0 = Percentage 14
	| v >= 24.0 = Percentage 5
	| otherwise = Percentage 0

-- This forumula comes from fitting the voltsToPercentage to a 
-- curve, particularly an asymmetric sigmoidial.
--
-- The curve fit is within a few percent for the values in the 
-- voltsToPercentageTable, and often lands right on them, 
-- except at the low end, which is hardcoded instead.
voltsToPercentageSmooth :: Volts -> Percentage
voltsToPercentageSmooth (Volts v)
	| basev <= 12.0 = Percentage 5
	| basev <= 12.5 = Percentage 14
	| otherwise = Percentage $ min 100 $ curve basev
  where
	basev = v/2
	curve n = 
		100.1504 + 
		(10.46593 - 100.1504)
			/
		(1 + (n/15.65729)**99.80404)
		**25585390

-- 24.0 (5%) is the danger zone for Battleborn 24v nominal.
-- Discharging below this point repeatedly risks damaging the battery,
-- and going too low will prevent the battery from being able to recharge.
dangerZone :: Volts
dangerZone = 24.0

-- Lithium ion does not mind being discharged low, so just keep far enough
-- above the danger zone that a large load is unlikely to push below it,
-- and that it's unlikely to discharge overnight if left in this state
-- due to small loads that are not under computer control (including the
-- computer itself).
minimumSafe :: Volts
minimumSafe = Volts 24.5

-- Battleborn do not distinguish between bulk and absorb.
-- Ideal voltage for both, per the manual is 28.4-28.8, with 28.8
-- recommended.
--
-- (The charge controller is configured to use 29.2.)
absorbVoltage :: Range Volts
absorbVoltage = Range (Volts 29.2) (Volts 29.2)

-- Do not equalize Battleborn.
equalizationVoltage :: Maybe Volts
equalizationVoltage = Nothing

-- Do not termperature compensate Battleborn; set to 0 mV in charge
-- controller.
temperatureCompensation :: Bool
temperatureCompensation = False

-- | Normal range of voltage when in float and the batteries are well
-- charged.
--
-- Battleborn does not need a float stage, but can accept this float range,
-- according to the manual.
--
-- The Epsolar charge controller sometimes stays in float mode when the
-- batteries discharge some, but such lower voltages are not in this range.
--
-- (The charge controller is configured to use 27.2. The trimetric is also
-- configured to use 27.2 as its charged setpoint voltage.)
floatVoltage :: Range Volts
floatVoltage = Range (Volts 26.8) (Volts 27.2)

-- When the battery is being charged and is at or below this, it's
-- perhaps in float, but getting low so may be a good idea to discontinue
-- large loads that are not wanted to discharge the battery any.
lowFloat :: Volts
lowFloat = Volts 26.6

fullyCharged :: Range Percentage
fullyCharged = Range (Percentage 90) (Percentage 100)

-- Range where the battery is pretty well charged (several days power), 
-- but not entirely full yet. Normally the fridge only turns on
-- then the battery is above this range. Once on, it's allowed
-- to keep running until the battery falls below this range.
--
-- This prioritizes charging the battery over running the fridge,
-- while avoiding turning the fridge off for every passing cloud.
mostlyCharged :: Range Percentage
mostlyCharged = Range (Percentage 50) (Percentage 90)

-- Range where the battery is charged enough to start running major loads
-- that may prevent charging it further even on a sunny day.
--
-- This varies with the hour of day. Early in the day, the battery does not
-- need to be as full to be considered well charged, since there is
-- still plenty of time for it to charge up. Later in the day, with less
-- time to charge, it needs to be more full.
wellCharged :: Int -> Range Percentage
wellCharged hour
	| pmhour <= 0 = Range (Percentage 50) (Percentage 100)
	| pmhour <= 1 = Range (Percentage 60) (Percentage 100)
	| pmhour <= 2 = Range (Percentage 70) (Percentage 100)
	| pmhour <= 3 = Range (Percentage 80) (Percentage 100)
	| pmhour <= 5 = Range (Percentage 90) (Percentage 100)
	| otherwise = Range (Percentage 95) (Percentage 100)
  where
	pmhour = hour - 12

-- 5% is about enough to power to get through a night in low power mode,
-- doubled for some safety margin.
--
-- If the batteries are fuller than that, it's more important to keep the
-- fridge from getting excessively warm than it is to charge them.
--
-- Also, the satellite internet is turned off when below this range.
badlyCharged :: Range Percentage
badlyCharged = Range (Percentage 0) (Percentage 10)

-- The batteries should not charge at fater than this rate.
-- 
-- 1/2 of C rating, which is 50 amps per battery in series.
maxChargeRate :: Amps 
maxChargeRate = Amps (50 * 2)

nearMaxChargeRate :: Amps
nearMaxChargeRate = 
	let Amps n = maxChargeRate
	in Amps (n * 0.9)
