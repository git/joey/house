{-# LANGUAGE LambdaCase, BangPatterns #-}

-- | Automation of my house, using FRP.

module Automation where

import Automation.Types
import Automation.Utility
import Automation.Solar
import Automation.Fridge
import Automation.KitchenCounterOutlet
import Automation.Car
import DataUnits
import Sensors

import Reactive.Banana
import Reactive.Banana.Automation
import Data.Functor.Compose
import Data.Time.Clock.POSIX
import Data.Maybe

homeAutomation :: Automation (Sensors t) Actuators ()
homeAutomation = do
	fridgethoughts <- fridgeThoughts
	controlInverter
	controlFridgeRelay fridgethoughts
	controlSpringPump
	controlKitchenCounterOutlet
	controlCarCharger
	overallStatus fridgethoughts
	collectStats fridgethoughts
	debug fridgethoughts

controlFridgeRelay :: FridgeThoughts -> Automation (Sensors t) Actuators ()
controlFridgeRelay t = do
	b <- fridgeBehavior t inverterDelay
	actuateBehaviorMaybe b FridgeRelay

-- | The spring pump can run any time.
--
-- It can be overridden off. Its run cycle is otherwise controlled by the
-- springPumpRunCycle.
springPumpBehavior :: Automation (Sensors t) Actuators (Behavior PowerChange)
springPumpBehavior = do
	b <- getCompose $ go 
		<$> Compose (clockSignalBehavior currentTime)
		<*> Compose (automationStepper defaultruncycle
			=<< getEventFrom springPumpRunCycle)
	applyOverrideM b springPumpOverride id
  where
	go (Just (ClockSignal clock)) runcycle = calcRunCycle runcycle clock
	go Nothing _ = PowerOff

	defaultruncycle = OnOffRunCycle
		{ onMinutes = 1
		, offMinutes = 0
		}

calcRunCycle :: RunCycle -> POSIXTime -> PowerChange
calcRunCycle runcycle@(OnOffRunCycle {}) clock
	| seglen == 0 = PowerOn
	| minnum `mod` seglen < onMinutes runcycle = PowerOn
	| otherwise = PowerOff
  where
	seglen = onMinutes runcycle + offMinutes runcycle
	minnum = floor (clock / 60)

controlSpringPump :: Automation (Sensors t) Actuators ()
controlSpringPump = do
	b <- springPumpBehavior
	actuateBehavior b SpringPumpFloatSwitch

controlInverter :: Automation (Sensors t) Actuators ()
controlInverter = do
	b <- inverterBehavior
	actuateBehaviorMaybe b InverterPower

-- | Overall behavior of the inverter.
inverterBehavior :: Automation (Sensors t) Actuators (Behavior (Maybe PowerChange))
inverterBehavior =
	overallBehavior inverterWanted inverterDelay inverterOverride id

-- | Limit rate that InverterPower actuator is used to once per 2 seconds, 
-- to prevent excessive cycling if something went badly wrong.
--
-- The fridge relay is also limited to this rate, because there is
-- no point in changing it more frequently than the inverter actuator.
--
-- However, overrides to the inverter power take effect immediately.
inverterDelay :: Maybe Delay
inverterDelay = Just (DelaySeconds 2)

-- | Do we want to run the inverter?
--
-- When power is low turn off the inverter.
--
-- But, turning off the inverter with the car plugged in can keep the car
-- awake (complaining that power has been lost) even when it was not
-- scheduled to be charging, which drains its battery slowly.
-- So only turn off the inverter when the house battery is quite low.
inverterWanted :: Automation (Sensors t) Actuators (Behavior (Maybe PowerChange))
inverterWanted = getCompose $ react
	<$> Compose lowpowerMode
  where
	react lowpower
		| lowpower = Just PowerOff
		| otherwise = Just PowerOn

debug :: FridgeThoughts -> Automation (Sensors t) Actuators ()
debug fridgethoughts = do
	fridgetemp <- sensedBehavior fridgeTemperature
	actuateBehavior fridgetemp $ Debug "fridge temperature" . show
	fridgeruntimetoday <- fridgeRuntimeToday fridgethoughts inverterDelay
	actuateBehavior fridgeruntimetoday $ Debug "fridge runtime today" . show . fst
	fridgerunduration <- fridgeRunDuration fridgethoughts inverterDelay
	actuateBehavior fridgerunduration $ Debug "fridge run duration" . show
	
	lp <- lowpowerMode
	actuateBehavior lp $ Debug "low power mode" . show
	solarestimate <- solarEstimate
	actuateBehavior solarestimate $ Debug "solar estimate" . show

overallStatus :: FridgeThoughts -> Automation (Sensors t) Actuators ()
overallStatus fridgethoughts = do
	st <- getCompose $ go
		<$> Compose lowpowerMode
		<*> Compose isSunnyDay
		<*> Compose (powerSettingBehavior' =<< inverterBehavior)
		<*> Compose (powerSettingBehavior' =<< fridgeBehavior fridgethoughts inverterDelay)
		<*> Compose (statusFridgeThoughts fridgethoughts)
		<*> Compose (powerSettingBehavior' =<< isCarChargerOn)
		<*> Compose (override inverterOverride)
		<*> Compose (override fridgeOverride)
		<*> Compose (override carChargerOverride)
		<*> Compose kitchenCounterOutletBehavior
		<*> Compose springPumpBehavior
	st' <- rateLimitBehavior (pure st) (Just (DelaySeconds 1))
	actuateBehaviorMaybe st' OverallStatus
  where
	override f = getEventFrom f
		>>= automationStepper DisableOverride
		>>= return . fmap (fmap powerChangeToPowerSetting)
	go lowpower sunnyday inverter fridge sfridgethoughts carcharger inverteroverride fridgeoverride carchargeroverride kitchenoutlet springpump =
		Just $ catMaybes
			[ case sunnyday of
				True -> Just Sunny
				False -> Nothing
			, Just $ InverterStatus $ mkOverridable
				inverteroverride inverter
			, Just $ FridgeStatus $ mkOverridable
				fridgeoverride fridge
			, Just $ CarChargerStatus $ mkOverridable
				carchargeroverride carcharger
			, case lowpower of
				True -> Just LowPower
				False -> Nothing
			, Just sfridgethoughts
			, Just $ KitchenCounterOutletStatus $ powerChangeToPowerSetting kitchenoutlet
			, Just $ SpringPumpStatus $ powerChangeToPowerSetting springpump
			]

-- | Collection of various stats about the automation.
collectStats :: FridgeThoughts -> Automation (Sensors t) Actuators ()
collectStats fridgethoughts = do
	flip actuateBehavior FridgeRuntimeToday =<< fridgeRuntimeToday fridgethoughts inverterDelay
	flip actuateBehavior FridgeRunDuration =<< fridgeRunDuration fridgethoughts inverterDelay
