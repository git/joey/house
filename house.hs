{-# LANGUAGE LambdaCase #-}

-- | Command-line tool to interact with house.

import Control.Concurrent.STM
import System.IO
import System.Environment
import System.Exit
import Data.Maybe
import Text.Read (readMaybe)
import qualified Data.Map.Strict as M

import Sensors
import DataUnits
import WebServers
import Automation.Constants
import Epsolar
import qualified Batteries
import Utility.HumanTime

main :: IO ()
main = getArgs >>= go
  where
	go [] = watchXDG display
	go ("watch":"-":[]) = watchStdout (const (return ()))
	go ("watch":[]) = watchXDG (const (return ()))
	go ("watch":f:[]) = watch (const (return ())) (writeFile f)
	go ("stream":[]) = streamSensors
	go ("inverter":l) = overrideOf InverterOverride l (hours 12)
	go ("fridge":l) = overrideOf FridgeOverride l (hours 8)
	go ("kitchen":l) = overrideOf KitchenCounterOutletOverride l (hours 4)
	go ("carcharger":l) = overrideOf CarChargerOverride l (hours 4)
	go ("spring":l) = overrideOf SpringPumpOverride l (days 7)
	go ("springruncycle":l) = runCycle SpringPumpRunCycle l
	go _ = usage

	minutes n = n * 60
	hours n = 60 * minutes n
	days n = 24 * hours n
	
usage :: IO ()
usage = do
	hPutStrLn stderr $ unwords
		 [ "Usage: house watch [file] | stream"
		 , "| (inverter|fridge|kitchen|carcharger|spring) (on|off|auto) duration"
		 , "| springruncycle onminutes offminutes"
		 ]
	exitWith (ExitFailure 1)

overrideOf :: (Override PowerSetting -> SensedValue) -> [String] -> Int -> IO ()
overrideOf mk (v:n:[]) maxoverrideduration = case override of
	Right o@(OverrideSeconds nsecs _)
		| nsecs > maxoverrideduration ->
			error "duration is too long to be safe!"
		| otherwise -> send (mk o)
	Right DisableOverride -> return ()
	Left e -> do
		hPutStrLn stderr e
		usage
  where
	override = OverrideSeconds
		<$> (fromIntegral . durationSeconds <$> parseDuration n)
		<*> parseps v
	parseps "on" = Right (PowerSetting True)
	parseps "off" = Right (PowerSetting False)
	parseps _ = Left "expected on|off"
overrideOf mk ("auto":[]) _ = send (mk DisableOverride)
overrideOf _ _ _ = usage

runCycle :: (RunCycle -> SensedValue) -> [String] -> IO ()
runCycle mk (on:off:[]) = case (readMaybe on, readMaybe off) of
	(Just non, Just noff) -> send $ mk $
		OnOffRunCycle { onMinutes = non, offMinutes = noff }
	_ -> do
		hPutStrLn stderr "Expected number of minutes on followed by number of minutes off."
		usage
runCycle _ _ = usage

send :: SensedValue -> IO ()
send v = do
	ok <- postSensedValue v
	if ok
		then return ()
		else error "failed communicating with house"

streamSensors :: IO ()
streamSensors = streamPollerWebserverReconnect display

type Displayer = Either String SensedValue -> IO ()

display :: Displayer
display (Left e) = do
	putStrLn e
	hFlush stdout
display (Right v) = do
	print v
	hFlush stdout

watchStdout :: Displayer -> IO ()
watchStdout displayer = 
	watch displayer $ \l -> do
		putStrLn l
		hFlush stdout
		
watchXDG :: Displayer -> IO ()
watchXDG displayer = do
	d <- getEnv "XDG_RUNTIME_DIR"
	watch displayer (writeFile (d ++ "/house"))

watch :: Displayer -> (String -> IO ()) -> IO ()
watch displayer callback = do
	sv <- newTVarIO (M.empty, "")
	streamPollerWebserverReconnect $ \ev -> do
		displayer ev
		case ev of
			Left _e -> callback "x"
			Right v -> do
				(m, l) <- atomically (readTVar sv)
				let k = keySensedValue' v
				let m' = M.insert k v m
				let l' = renderStatus m'
				if l /= l'
					then do
						callback l'
						atomically $ writeTVar sv (m', l')
					else atomically $ writeTVar sv (m', l)

renderStatus :: M.Map KeySensedValue SensedValue -> String
renderStatus m = concat
	[ fmt housestatus id ""
	, fmt batterypercent (showDoublePrecision 0) (batteryvoltpercent ++ "% ")
	, fmt trimetricvolts (showDoublePrecision 2) "v "
	, if ccinputwatts >= Just 1
		then fmt ccinputwatts (showDoublePrecision 0) $ 
			case ccchargingstatus of
				Just Float -> "⇝"
				Just Boost -> "↗"
				Just Equalization -> "="
				Just NoCharging -> "!"
				Nothing -> "?"
		else ""
	, fmt trimetricwatts (showDoublePrecision 0) "W "
	, fmt dailykwh (showDoublePrecision 2) "kwh "
	--, fmt pumpstatus id ""
	, fmt porchtemperature (show . celsiusToFahrenheit) ""
	, "("
	, fmt halltemperature (show . celsiusToFahrenheit) ""
	, ")"
	, case fridgetemperature of
		Just t -> 
			let (Delta d) = rangeDelta t fridgeAllowedTemp
			    fd = celsiusToFahrenheit d - 32
			    s = concat
			    	[ if fd > 0 then "+" else ""
				, takeWhile (/= '.') (show fd)
				]
			in if s == "0"
				then ""
				else "[" ++ s ++ "]"
		Nothing -> "[?]"
	, "°F "
	, case get AutomationOverallStatus of
		Just (AutomationOverallStatus l) ->
			case filter isCarChargerStatus l of
				[] -> ""
				(CarChargerStatus (SetTo (PowerSetting False)):_) -> ""
				(CarChargerStatus (Overridden (PowerSetting False)):_) -> ""
				(c:_) -> 
					statusGlyph c ++ case carcharge of
						Just (Percentage p) -> 
							takeWhile (/= '.') (show p) ++ "%"
						_ -> "?"
		_ -> ""
	]
  where
	fmt v f s = maybe "?" f v ++ s
	
	get f = M.lookup (keySensedValue f) m
	with f extract = extract =<< get f
	sumccs f = Just $ sum $ map (\n -> f (CcID n)) [1..5]
	housestatus = case get AutomationOverallStatus of
		Just (AutomationOverallStatus l) ->
			let h = filter isHouseStatus l
			    ret = Just . concatMap statusGlyph
			in ret h
		_ -> Nothing
	batterypercent = with TrimetricBatteryPercent $ \case
		TrimetricBatteryPercent (Just (Percentage p)) -> Just p
		_ -> Nothing
	trimetricwatts = with TrimetricBatteryWatts $ \case
		TrimetricBatteryWatts (Just (Watts p)) -> Just p
		_ -> Nothing
	trimetricvolts = with TrimetricBatteryVolts $ \case
		TrimetricBatteryVolts (Just (Volts p)) -> Just p
		_ -> Nothing
	ccinputwatts = sumccs $ \n -> fromMaybe 0 $ 
		with (CcInputWatts n) $ \case
			CcInputWatts _ (Just (Watts p)) -> Just p
			_ -> Nothing
	dailykwh = sumccs $ \n -> fromMaybe 0 $
		with (CcKWHGeneratedToday n) $ \case
			CcKWHGeneratedToday _ (Just (KWH p)) -> Just p
			_ -> Nothing
	ccchargingstatus = with (CcStatusCode 1) $ \case
		CcStatusCode (CcID 1) (Just c) -> Just (chargingStatus c)
		_ -> Nothing
	porchtemperature = with PorchTemperature $ \case
		PorchTemperature (Just t) -> Just t
		_ -> Nothing
	halltemperature = with HallTemperature $ \case
		HallTemperature (Just t) -> Just t
		_ -> Nothing
	fridgetemperature = with FridgeTemperature $ \case
		FridgeTemperature (Just t) -> Just t
		_ -> Nothing
	carcharge = with CarStateOfCharge $ \case
		CarStateOfCharge (Just c) -> Just c
		_ -> Nothing

	-- Calculating battery percentage from voltage can be more accurate
	-- on the low end, but with less granularity and only when it's
	-- night or at least when the charge controller is not pushing up the
	-- voltage much. Since a momentary voltage sag due to high load makes
	-- it innaccruate, only display when it's higher than the trimetric's
	-- value. The trimetric tends low.
	batteryvoltpercent
		| ccinputwatts > Just 100 = ""
		| otherwise =
			case (trimetricvolts, batterypercent) of
				(Just v, Just bp)
					| bp > 30 -> ""
					| otherwise -> 
						let Percentage p = Batteries.voltsToPercentageSmooth (Volts v)
						    p' = floor p :: Int
						in if p' <= floor bp
							then ""
							else "-" ++ show p'
				_ -> ""

isHouseStatus :: Status -> Bool
isHouseStatus (SpringPumpStatus _) = False
isHouseStatus (CarChargerStatus _) = False
isHouseStatus _ = True

isCarChargerStatus :: Status -> Bool
isCarChargerStatus (CarChargerStatus _) = True
isCarChargerStatus _ = False

statusGlyph :: Status -> String
statusGlyph LowPower = emoji WarningSign
statusGlyph Sunny = emoji HappySun
statusGlyph (InverterStatus (SetTo (PowerSetting True))) = emoji SineWave
statusGlyph (InverterStatus (SetTo (PowerSetting False))) = "✘"
statusGlyph (InverterStatus (Overridden (PowerSetting True))) = 
	emoji SineWave ++ "!"
statusGlyph (InverterStatus (Overridden (PowerSetting False))) = "✘!"
statusGlyph (InverterStatus NotKnown) = "?"
statusGlyph (FridgeStatus (SetTo (PowerSetting True))) = emoji SnowFlake
statusGlyph (FridgeStatus (Overridden (PowerSetting True))) =
	emoji SnowFlake ++ "!"
statusGlyph (FridgeStatus _) = ""
statusGlyph (FridgeThoughts _) = ""
statusGlyph (KitchenCounterOutletStatus (PowerSetting True)) = emoji KitchenSink
statusGlyph (KitchenCounterOutletStatus (PowerSetting False)) = ""
statusGlyph (CarChargerStatus (SetTo (PowerSetting True))) = emoji CarCharger
statusGlyph (CarChargerStatus (SetTo (PowerSetting False))) = ""
statusGlyph (CarChargerStatus (Overridden (PowerSetting True))) = 
	emoji CarCharger ++ "!"
statusGlyph (CarChargerStatus (Overridden (PowerSetting False))) = ""
statusGlyph (CarChargerStatus NotKnown) = emoji CarCharger ++ "?"
statusGlyph (SpringPumpStatus (PowerSetting True)) = emoji Pump
statusGlyph (SpringPumpStatus (PowerSetting False)) = ""

data Emoji = SnowFlake | SineWave | HappySun | WarningSign
	| Power | Pump | KitchenSink | CarCharger

emoji :: Emoji -> String
emoji SnowFlake = "❄"
emoji SineWave = "∿"
-- ghc does not like the unicde power symbol
emoji Power = "\9211"
emoji HappySun = "🌞"
emoji WarningSign = "⚠️"
emoji Pump = "🚰"
-- sadly, emojis and unicode left out the kitchen sink
emoji KitchenSink = "🥚"
-- and nothing yet for EVs!
emoji CarCharger = "⚇"

