all:
	cabal build
	@for p in controller house poller setup watchdog; do \
		if [ -e dist-newstyle ]; then \
			ln -sf $$(cabal exec -- sh -c "command -v $$p") $$p; \
		else \
			ln -sf dist/build/$$p/$$p $$p; \
		fi; \
		strip $$p; \
	done
	$(MAKE) SI7021

test:
	cabal test

clean:
	cabal clean
	rm -f SI7021

.PHONY: test
