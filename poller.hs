{-# LANGUAGE FlexibleContexts, LambdaCase #-}

-- | Polls the charge controllers and other sensors and sources for data,
-- and updates the rrd files and the json for the web page.
--
-- Runs a http server which streams sensed values to clients as they
-- become available. Some data can also be posted to the http server.

import System.IO
import System.Process
import System.Directory
import System.FilePath
import Data.List
import Data.Maybe
import Data.Char
import Control.Monad
import Control.Concurrent
import Control.Concurrent.Async
import Data.Time.Clock
import Control.Exception
import System.Timeout
import System.Exit
import System.Posix.Files
import Text.Read

import RRD
import Sensors
import WebServers
import DataUnits
import Utility.ThreadScheduler
import Epsolar
import Trimetric.Poller
import Car
import CarCharger.GrizzlE

data DataSource = DataSource RrdName (Maybe (String -> SensedValue)) DataSourceType Heartbeat
data DataSourceType = Gauge | Counter
newtype Heartbeat = HeartBeat Seconds

formatDataSource :: DataSource -> String
formatDataSource (DataSource name _ t (HeartBeat (Seconds h))) = intercalate ":"
	[ "DS"
	, name
	, formatDataSourceType t
	, show h
	, "U" -- unknown min
	, "U" -- unknown max
	]

formatDataSourceType :: DataSourceType -> String
formatDataSourceType Gauge = "GAUGE"
formatDataSourceType Counter = "COUNTER"

data DataSourceFrom
	= EpsolarStatusValue CcID (Maybe EpsolarStatus -> SensedValue)
	| EpsolarExtStatusValue CcID (Maybe EpsolarExtStatus -> SensedValue)
	| EpsolarOtherStatusValue CcID (Maybe EpsolarOtherStatus -> SensedValue)
	| OneWireSensor String
	| SI7021 String
	| StoredSensedValue KeySensedValue SensedValue

dataSources :: FreqPair StepLen -> FreqPair [(DataSourceFrom, DataSource)]
dataSources steplen = FreqPair
	{ frequent =
		[ mkf Gauge (OneWireSensor "28-000009b76d38") "fridge_temp_celsius" FridgeTemperature 3
		, mkfa Gauge TrimetricBatteryPercent "battery_percent"
		, mkfa Gauge TrimetricBatteryWatts "battery_watts"
		, mkfa Gauge TrimetricBatteryVolts "battery_volts"
		, mkfe Gauge (EpsolarStatusValue 1) (CcInputVolts 1) epsolar_input_volts "cc1_input_volts"
		, mkfe Gauge (EpsolarStatusValue 1) (CcOutputVolts 1) epsolar_output_volts "cc1_output_volts"
		, mkfe Gauge (EpsolarStatusValue 1) (CcInputWatts 1) epsolar_input_watts "cc1_input_watts"
		, mkfe Gauge (EpsolarStatusValue 2) (CcInputVolts 2) epsolar_input_volts "cc2_input_volts"
		, mkfe Gauge (EpsolarStatusValue 2) (CcOutputVolts 2) epsolar_output_volts "cc2_output_volts"
		, mkfe Gauge (EpsolarStatusValue 2) (CcInputWatts 2) epsolar_input_watts "cc2_input_watts"
		, mkfe Gauge (EpsolarStatusValue 3) (CcInputVolts 3) epsolar_input_volts "cc3_input_volts"
		, mkfe Gauge (EpsolarStatusValue 3) (CcOutputVolts 3) epsolar_output_volts "cc3_output_volts"
		, mkfe Gauge (EpsolarStatusValue 3) (CcInputWatts 3) epsolar_input_watts "cc3_input_watts"
		, mkfe Gauge (EpsolarStatusValue 4) (CcInputVolts 4) epsolar_input_volts "cc4_input_volts"
		, mkfe Gauge (EpsolarStatusValue 4) (CcOutputVolts 4) epsolar_output_volts "cc4_output_volts"
		, mkfe Gauge (EpsolarStatusValue 4) (CcInputWatts 4) epsolar_input_watts "cc4_input_watts"
		, mkfe Gauge (EpsolarStatusValue 5) (CcInputVolts 5) epsolar_input_volts "cc5_input_volts"
		, mkfe Gauge (EpsolarStatusValue 5) (CcOutputVolts 5) epsolar_output_volts "cc5_output_volts"
		, mkfe Gauge (EpsolarStatusValue 5) (CcInputWatts 5) epsolar_input_watts "cc5_input_watts"
		]
	, infrequent =
		[ mki Gauge (OneWireSensor "28-0000098cd407") "porch_temp_celsius" PorchTemperature 3
		, mki Gauge (OneWireSensor "28-0517001678ff") "hall_temp_celsius" HallTemperature 3
		--, mki Gauge (SI7021 "Temperature") "hall_temp_celsius" HallTemperature 3
		, mki Gauge (OneWireSensor "28-000009b76d38") "fridge_temp_celsius" FridgeTemperature 3
		, mkia Gauge AutomationFridgeRuntimeToday "fridge_hours_today"
		, mkia Gauge AutomationFridgeRunDuration "fridge_run_duration"
		, mki Gauge (OneWireSensor "28-0417007c85ff") "fridge_aux_temp" FridgeAuxTemperature 3
		, mki Gauge (OneWireSensor "28-00000a37ba58") "spare_probe_temp" SpareProbeTemperature 3
		, mki Gauge (SI7021 "Relative Humidity") "hall_humidity" HallHumidity 1
		, mki Gauge (OneWireSensor "28-03170068a7ff") "slab_temp" SlabTemperature 3
		, mkia Gauge SensorFailures "sensor_failures"
		, mkia Gauge TrimetricCCStatusCode "trimetric_cc_status"
		-- Using charge controller 1 only since these will not be
		-- significantly different for the others.
		, mkie Gauge (EpsolarOtherStatusValue 1) (CcMaxBatteryVoltsToday 1) epsolar_max_battery_volts_today "max_battery_volts"
		, mkie Gauge (EpsolarOtherStatusValue 1) (CcMinBatteryVoltsToday 1) epsolar_min_battery_volts_today "min_battery_volts"
		-- This is not accurate with battleborn lithium batteries,
		-- but is used as a fallback.
		, mkie Gauge (EpsolarExtStatusValue 1) (CcBatteryPercent 1) epsolar_battery_percent "cc1_battery_percent"
		, mkie Gauge (EpsolarOtherStatusValue 1) (CcKWHGeneratedToday 1) epsolar_kwh_generated_today "cc1_kwh_today"
		, mkie Gauge (EpsolarOtherStatusValue 1) (CcKWHGeneratedTotal 1) epsolar_kwh_generated_total "cc1_kwh_total"
		, mkie Gauge (EpsolarExtStatusValue 1) (CcTemperature 1) epsolar_inside_temp "cc1_temp_celsius"
		, mkie Gauge (EpsolarOtherStatusValue 1) (CcStatusCode 1) epsolar_statuscode "cc1_status_code"
		, mkie Gauge (EpsolarOtherStatusValue 2) (CcKWHGeneratedToday 2) epsolar_kwh_generated_today "cc2_kwh_today"
		, mkie Gauge (EpsolarOtherStatusValue 2) (CcKWHGeneratedTotal 2) epsolar_kwh_generated_total "cc2_kwh_total"
		, mkie Gauge (EpsolarExtStatusValue 2) (CcTemperature 2) epsolar_inside_temp "cc2_temp_celsius"
		, mkie Gauge (EpsolarOtherStatusValue 2) (CcStatusCode 2) epsolar_statuscode "cc2_status_code"
		, mkie Gauge (EpsolarOtherStatusValue 3) (CcKWHGeneratedToday 3) epsolar_kwh_generated_today "cc3_kwh_today"
		, mkie Gauge (EpsolarOtherStatusValue 3) (CcKWHGeneratedTotal 3) epsolar_kwh_generated_total "cc3_kwh_total"
		, mkie Gauge (EpsolarExtStatusValue 3) (CcTemperature 3) epsolar_inside_temp "cc3_temp_celsius"
		, mkie Gauge (EpsolarOtherStatusValue 3) (CcStatusCode 3) epsolar_statuscode "cc3_status_code"
		, mkie Gauge (EpsolarOtherStatusValue 4) (CcKWHGeneratedToday 4) epsolar_kwh_generated_today "cc4_kwh_today"
		, mkie Gauge (EpsolarOtherStatusValue 4) (CcKWHGeneratedTotal 4) epsolar_kwh_generated_total "cc4_kwh_total"
		, mkie Gauge (EpsolarExtStatusValue 4) (CcTemperature 4) epsolar_inside_temp "cc4_temp_celsius"
		, mkie Gauge (EpsolarOtherStatusValue 4) (CcStatusCode 4) epsolar_statuscode "cc4_status_code"
		, mkie Gauge (EpsolarOtherStatusValue 5) (CcKWHGeneratedToday 5) epsolar_kwh_generated_today "cc5_kwh_today"
		, mkie Gauge (EpsolarOtherStatusValue 5) (CcKWHGeneratedTotal 5) epsolar_kwh_generated_total "cc5_kwh_total"
		, mkie Gauge (EpsolarExtStatusValue 5) (CcTemperature 5) epsolar_inside_temp "cc5_temp_celsius"
		, mkie Gauge (EpsolarOtherStatusValue 5) (CcStatusCode 5) epsolar_statuscode "cc5_status_code"
		, mkia Gauge CarStateOfCharge "car_soc"
		, mkia Gauge TrimetricBatteryPercent "battery_percent"
		]
	}
  where
	mkf t dsf rrdname mkval p = mk (frequent steplen) t dsf rrdname (valparser mkval p)
	mki t dsf rrdname mkval p = mk (infrequent steplen) t dsf rrdname (valparser mkval p)
	mkfe t c u f rrdname = mk (frequent steplen) t dsf rrdname Nothing
	  where
		dsf = c $ \v -> u (f <$> v)
	mkie t c u f rrdname = mk (infrequent steplen) t dsf rrdname Nothing
	  where
		dsf = c $ \v -> u (f <$> v)
	mkfa t f rrdname = mk (frequent steplen) t dsf rrdname Nothing
	  where
		dsf = StoredSensedValue (keySensedValue f) (f defSensedValue)
	mkia t f rrdname = mk (infrequent steplen) t dsf rrdname Nothing
	  where
		dsf = StoredSensedValue (keySensedValue f) (f defSensedValue)
	mk (StepLen (Seconds steplen')) t dsf rrdname p =
		(dsf, DataSource rrdname p t hb)
	  where
		-- Allow missing one value before it goes unknown in the rrd.
		hb = HeartBeat (Seconds (steplen' * 2))
	valparser mkval p = Just $ mkval . parseDataUnit p

createRRD :: FilePath -> StepLen -> [(DataSourceFrom, DataSource)] -> [RRA] -> IO ()
createRRD f (StepLen (Seconds steplensecs)) ds rra = do
	createDirectoryIfMissing True (takeDirectory f)
	print ps
	callProcess "rrdtool" ps
  where
	ps = concat
		[ [ "create", f, "--step=" ++ show steplensecs ]
		, map (formatDataSource . snd) ds
		, map formatRRA rra
		]

updateRRD :: LogHandle -> FilePath -> [(DataSource, SensedValue)] -> IO ()
updateRRD lh f vs
	| null vs' = return ()
	| otherwise = do
		(Nothing, Nothing, Nothing, p) <- createProcess $ proc "rrdupdate" ps
		r <- waitForProcess p
		case r of
			ExitFailure _ -> do
				logError lh $ "rrdupdate failed with params: " ++ show ps
				return ()
			ExitSuccess -> return ()
  where
	ps = [ f, "--template", template, "N:" ++ vals ]
	template = intercalate ":" (map (rrdname . fst) vs')
	vals = intercalate ":" (map snd vs')
	vs' = catMaybes $ map elim vs
	elim (ds, sv) = case formatSensedValue sv of
		Nothing -> Nothing
		Just s -> Just (ds, s)
	rrdname (DataSource n _ _ _) = n

data QueryHandle = QueryHandle
	QSem
	(Maybe EpsolarHandle)
	(RegisterVectorFor EpsolarStatus)
	(RegisterVectorFor EpsolarSingleValue)
	(RegisterVectorFor EpsolarSingleValueWide)

mkQueryHandles :: IO [QueryHandle]
mkQueryHandles = do
	qh0 <- mkQueryHandle Nothing
	qh1 <- mkQueryHandle (Just epsolarSerialDevice1)
	qh2 <- mkQueryHandle (Just epsolarSerialDevice2)
	qh3 <- mkQueryHandle (Just epsolarSerialDevice3)
	qh4 <- mkQueryHandle (Just epsolarSerialDevice4)
	qh5 <- mkQueryHandle (Just epsolarSerialDevice5)
	return [qh0, qh1, qh2, qh3, qh4, qh5]

mkQueryHandle :: Maybe FilePath -> IO QueryHandle
mkQueryHandle epsolardevice = QueryHandle
	<$> newQSem 1 -- only one query using this handle allowed at a time
	<*> (case epsolardevice of
		Nothing -> pure Nothing
		Just dev -> Just <$> mkEpsolarHandle dev)
	<*> mkEpsolarStatusRegisterVector
	<*> mkEpsolarSingleValueRegisterVector
	<*> mkEpsolarSingleValueWideRegisterVector

runQuery :: QueryHandle -> IO a -> IO a
runQuery (QueryHandle lcksem _ _ _ _) a = bracket_ lock unlock a
  where
	lock = waitQSem lcksem
	unlock = signalQSem lcksem

-- This allows multiple queries to share data that's bundled together.
data QueryPass = QueryPass
	(MVar EpsolarStatus)
	(MVar EpsolarExtStatus)
	(MVar EpsolarOtherStatus)

queryPass :: [QueryHandle] -> ([QueryPass] -> IO a) -> IO a
queryPass qhs a = do
	qps <- forM [0..length qhs] $ \_ ->
		QueryPass
			<$> newEmptyMVar
			<*> newEmptyMVar
			<*> newEmptyMVar
	a qps
  where

type QueryFailureCallback = SensedValue -> IO ()

epsolarQuery :: LogHandle -> QueryHandle -> MVar a -> (Maybe a -> SensedValue) -> QueryFailureCallback -> (EpsolarHandle -> IO (Either String a)) -> IO SensedValue
epsolarQuery lh qh@(QueryHandle _ (Just h) _ _ _) mv mksensedvalue onfailure q =
	runQuery qh $ tryReadMVar mv >>= \case
		Just st -> return (mksensedvalue (Just st))
		Nothing -> do
			res <- try' (q h)
			case res of
				Right (Right st) -> do
					putMVar mv st
					return (mksensedvalue (Just st))
				Right (Left e) -> do
					logError lh e
					failed
				Left e -> do
					logError lh (show e)
					failed
  where
	failed = do
		let v = mksensedvalue Nothing
		onfailure v
		return v
	try' :: IO a -> IO (Either SomeException a)
	try' = try
epsolarQuery _ _ _ _ _ _ = error "epsolarQuery called on QueryHandle without EpsolarHandle"

query :: LogHandle -> [QueryHandle] -> [QueryPass] -> GetSensedValue -> DataSourceFrom -> DataSource -> QueryFailureCallback -> IO SensedValue
query lh qhs qps _ (EpsolarStatusValue (CcID n) mksensedvalue) _ onfailure =
	let qh@(QueryHandle _ _ reg _ _) = qhs !! n
	    QueryPass mv _ _ = qps !! n
	in epsolarQuery lh qh mv mksensedvalue onfailure $ \ctx ->
		readEpsolarStatus ctx reg
query lh qhs qps _ (EpsolarExtStatusValue (CcID n) mksensedvalue) _ onfailure =
	let qh@(QueryHandle _ _ _ reg _) = qhs !! n
	    QueryPass _ mv _ = qps !! n
	in epsolarQuery lh qh mv mksensedvalue onfailure $ \ctx ->
		readEpsolarExtStatus ctx reg
query lh qhs qps _ (EpsolarOtherStatusValue (CcID n) mksensedvalue) _ onfailure =
	let qh@(QueryHandle _ _ _ reg1 reg2) = qhs !! n
	    QueryPass _ _ mv = qps !! n
	in epsolarQuery lh qh mv mksensedvalue onfailure $ \ctx ->
		readEpsolarOtherStatus ctx reg1 reg2
query lh qhs _ _ sensor@(OneWireSensor _) (DataSource _ (Just mksensedvalue) _ _) onfailure =
	runQuery (qhs !! 0) (loop (2 :: Int))
  where
	loop n = do
		let f = oneWireSensorDataFile sensor
		res <- timeout 3000000 (try $ readFile f :: IO (Either IOException String))
		case res of
			Nothing -> readerr $ "timeout reading " ++ f
			Just (Left e) -> readerr $ "query for " ++ f ++ " failed: " ++ show e
			Just (Right s)
				| "YES" `isInfixOf` s -> 
					case map (readMaybe . dropWhile (not . numchar)) $ filter ("t=" `isPrefixOf`) (words s) of
						(Just temp:[]) -> do
							let tempc = temp / (1000 :: Double)
							if tempc < 80 && tempc > (-80)
								then return $ mksensedvalue $ show tempc
								else readerr $ "query for " ++ f ++ " returned absurd value " ++ show tempc
						_ -> readerr $ "query for " ++ f ++ " failed parse"
				| otherwise -> readerr $ "query for " ++ f ++ " failed CRC"
	  where
		numchar c = isNumber c || c == '-'
		readerr s = do
			logError lh s
			let v = mksensedvalue ""
			onfailure v
			if n > 0
				then do
					threadDelay 1500000
					loop (n-1)
				else return v
query lh qhs _ _ (SI7021 match) (DataSource _ (Just mksensedvalue) _ _) onfailure = 
	runQuery (qhs !! 0) (loop (2 :: Int))
  where
	loop n = timeout 3000000 (readProcessWithExitCode "./SI7021" [] "") >>= \case
		Nothing -> readerr "timeout reading from SI7021"
		Just (_exitcode, out, _err) ->
			case map (reverse . words) $ filter (match `isPrefixOf`) (lines out) of
				((w:_):[]) -> return $ mksensedvalue w
				_ -> readerr "SI7021 output parse failed"
	  where
		readerr s = do
			logError lh s
			let v = mksensedvalue ""
			onfailure v
			if n > 0
				then do
					threadDelay 1500000
					loop (n-1)
				else return v
query _ _ _ getsensedvalue (StoredSensedValue k defv) (DataSource _ Nothing _ _) _ =
	fromMaybe defv <$> getsensedvalue k
query _ _ _ _ _ _ _ = error "bad data source definition!"

oneWireSensorDataFile :: DataSourceFrom -> FilePath
oneWireSensorDataFile (OneWireSensor sid) =
	"/sys/bus/w1/devices" </> sid </> "w1_slave"
oneWireSensorDataFile _ = ""

main :: IO ()
main = do
	linkSensors
	lh <- mkLogHandle
	qhs <- mkQueryHandles
	(webserverthread, addsensedvalue, getsensedvalue) <- runPollerWebserver postSensedValueCallback
	prep Frequent
	prep Infrequent
	void $ pollthread lh "Frequent query" (querier lh qhs getsensedvalue addsensedvalue Frequent False)
		`concurrently` pollthread lh "Infrequent query" (querier lh qhs getsensedvalue addsensedvalue Infrequent True)
		`concurrently` pollthread lh "trimetric" (Trimetric.Poller.pollTrimetric (Seconds 5) addsensedvalue getsensedvalue)
		`concurrently` pollthread lh "car status" (pollCarStatus (Seconds 120) addsensedvalue getsensedvalue)
		`concurrently` pollthread lh "car charger status" (pollCarChargerStatus (Seconds 120) addsensedvalue getsensedvalue)
		`concurrently` wait webserverthread
  where
	prep frequency = do
		let dsl = getFrequent frequency (dataSources mainStepLen)
		let fs = map (getFrequent frequency) (rrdFiles (allRras (getFrequent frequency mainStepLen)))
		forM_ fs $ \(rras, f) -> do
			e <- doesFileExist f
			if e
				then return ()
				else createRRD f (getFrequent frequency mainStepLen) dsl rras

	querier lh qhs getsensedvalue addsensedvalue frequency waitbetween = do
		let dsl = getFrequent frequency (dataSources mainStepLen)
		go lh qhs dsl getsensedvalue addsensedvalue frequency waitbetween

	go lh qhs dsl getsensedvalue addsensedvalue frequency waitbetween = do
		let fs = map (getFrequent frequency) (rrdFiles (allRras (getFrequent frequency mainStepLen)))
		forM_ fs $ \(rras, f) -> do
			e <- doesFileExist f
			if e
				then return ()
				else createRRD f (getFrequent frequency mainStepLen) dsl rras

		pollstart <- getCurrentTime
		putStrLn $ show frequency ++ " query start"
		hFlush stdout
		let incfailure val = when (sensorFailed val) $
			sensorFailure getsensedvalue addsensedvalue
		sensed <- queryPass qhs $ \qps ->
			forConcurrently dsl $ \(esv, ds) -> do
				val <- query lh qhs qps getsensedvalue esv ds incfailure
				() <- addsensedvalue val
				return (ds, val)
		
		pollend <- getCurrentTime
		let elapsed = pollend `diffUTCTime` pollstart
		putStrLn $ show frequency ++ " query complete"
		hFlush stdout

		forM_ fs $ \(_, f) ->
			updateRRD lh f sensed
		putStrLn $ show frequency ++ " rrd update complete"
		hFlush stdout

		updateJsonForWebPage frequency pollstart sensed getsensedvalue
		putStrLn $ show frequency ++ " json update complete"
		hFlush stdout

		if waitbetween
			then do
				let (StepLen (Seconds goal)) = getFrequent frequency mainStepLen
				let delta = fromIntegral goal - elapsed
				putStrLn $ show frequency ++ " delaying: " ++ show delta
				hFlush stdout
				threadDelay $ floor $ 1000000 * delta
			else return ()

		go lh qhs dsl getsensedvalue addsensedvalue frequency waitbetween

mainStepLen :: FreqPair StepLen
mainStepLen = FreqPair
	{ frequent = StepLen (Seconds 30)
	, infrequent = StepLen (Seconds 300)
	}

pollthread :: LogHandle -> String -> IO () -> IO ()
pollthread lh name a = forever $ do
	r <- try' a
	case r of
		Left e -> logError lh $
			"thread " ++ name ++ " crashed: " ++ show e ++ "(will restart in 5 seconds)"
		Right () -> logError lh $
			"thread " ++ name ++ " exited (will restart in 5 seconds)"
	threadDelaySeconds (Seconds 5)
  where
	try' :: IO a -> IO (Either SomeException a)
	try' = try
	
updateJsonForWebPage :: Frequency -> UTCTime -> [(DataSource, SensedValue)] -> GetSensedValue -> IO ()
updateJsonForWebPage frequency pollstart sensed getsensedvalue = do
	let jsname = case frequency of
		Frequent -> "frequent"
		Infrequent -> "infrequent"
	let jsfile = "rrds/recent" </> "data-" ++ jsname ++ ".js"
	let datapoints = map (\(DataSource rrdname _ _ _, val) -> (rrdname, formatSensedValue val)) sensed
	automationstatus <- getsensedvalue (keySensedValue AutomationOverallStatus)
		>>= return . \case
			Just (AutomationOverallStatus l) -> l
			_ -> []
	let fridgethoughts = Just $ concat $
		flip mapMaybe automationstatus $ \case
			FridgeThoughts t -> Just t
			_ -> Nothing
	let jsondatapoints = ("fridge_thoughts", fridgethoughts) : concatMap addFahrenheit datapoints
	writeFile (jsfile ++ ".new") $
		jsondata jsname (("lastpoll", Just (takeWhile (/= '.') (show pollstart))):jsondatapoints)
	renameFile (jsfile ++ ".new") jsfile
  where
	jsondata jsname l = 
		let inner = intercalate ", " $
			map (\(k, v) -> k ++ ": " ++ show (fromMaybe "unknown" v)) l
		in jsname ++ "={" ++ inner ++ "}"

addFahrenheit :: (String, Maybe String) -> [(String, Maybe String)]
addFahrenheit (k, v)
	| "celsius" `isSuffixOf` k =
		go (reverse (drop 7 (reverse k)) ++ "fahrenheit")
	| "_temp" `isSuffixOf` k = 
		go (k ++ "_fahrenheit")
	| otherwise = [(k, v)]
  where
	go kf = [ (k, v)
		, case readMaybe =<< v of
			Nothing -> (kf, Nothing)
			Just c -> (kf, Just $ show $ celsiusToFahrenheit (TemperatureCelsius c))
		]

postSensedValueCallback :: AddSensedValue -> SensedValue -> IO Bool
postSensedValueCallback addsensedvalue v = case v of
	-- Let the SensedValue be forwarded back to the automation.
	-- This way, the automation can send us values for safekeeping,
	-- and when it's restarted, it can access the previous value.
	_ -> do
		addsensedvalue v
		return True

-- | Make symlinks to sensor data files, for easy manual access.
linkSensors :: IO ()
linkSensors = do
	let ds = dataSources mainStepLen
	let w1sensors = filter (isw1 . fst) $
		getFrequent Frequent ds ++ getFrequent Infrequent ds
	createDirectoryIfMissing False "sensors"
	forM_ w1sensors $ \(sensor, DataSource name _ _ _) -> do
		let src = oneWireSensorDataFile sensor
		let dest = "sensors" </> name
		void $ try' $ removeLink dest
		createSymbolicLink src dest
  where
	isw1 (OneWireSensor _) = True
	isw1 _ = False

	try' :: IO a -> IO (Either SomeException a)
	try' = try

newtype LogHandle = LogHandle (MVar ())

mkLogHandle :: IO LogHandle
mkLogHandle = LogHandle <$> newMVar ()

logError :: LogHandle -> String -> IO ()
logError (LogHandle mvar) s = do
	takeMVar mvar
	hPutStrLn stderr s
	hFlush stderr
	putMVar mvar ()
