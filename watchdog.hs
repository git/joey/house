-- Watchdog. Detect fatal problems and reboot to try to fix them.
-- Must run as root.

{-# LANGUAGE LambdaCase #-}

import Automation.SafeMode
import Utility.ThreadScheduler
import Utility.Monad
import Epsolar

import System.Process
import System.Directory
import Control.Concurrent.Async
import Control.Exception
import System.Posix.Files
import System.IO
import Control.Concurrent.STM
import System.Posix.Types

main :: IO ()
main = do
	-- Wait for the charge controller serial port device to show up.
	-- If it's not available after boot, rebooting won't help,
	-- and this avoids a reboot loop.
	_ <- waitUntil "epsolar exists" $
		doesFileExist epsolarSerialDevice1
			<&&>
		doesFileExist epsolarSerialDevice2
			<&&>
		doesFileExist epsolarSerialDevice3
			<&&>
		doesFileExist epsolarSerialDevice4
			<&&>
		doesFileExist epsolarSerialDevice5

	-- If an epsolar serial port device vanishes, it's likely
	-- because the usb subsystem has glitched out.
	-- Rebooting is the best way I know to get it back.
	a <- async $ waitUntil "epsolar serial device gone" $
		(not <$> doesFileExist epsolarSerialDevice1)
			<||>
		(not <$> doesFileExist epsolarSerialDevice2)
			<||>
		(not <$> doesFileExist epsolarSerialDevice3)
			<||>
		(not <$> doesFileExist epsolarSerialDevice4)
			<||>
		(not <$> doesFileExist epsolarSerialDevice5)
	-- If the poller stops updating files, it may
	-- have gotten stuck, and need a reboot.
	tv <- newTMVarIO (0, Nothing)
	b <- async $ waitUntil "json file too old" 
		(pollerFileTooOld tv "rrds/recent/data-frequent.js")
	(_, reason) <- waitAny [a, b]

	reboot reason

	-- Try to enter safe mode, to not leave everything turned on
	-- if we fail to come back from the reboot. This might not be
	-- able to turn everything off given the degraded state.
	-- The reboot will happen even if this gets stuck.
	stopAutomation
	safeMode

waitUntil :: String -> IO Bool -> IO String
waitUntil desc a = do
	r <- a
	if r
		then return desc
		else do
			threadDelaySeconds (Seconds 60)
			waitUntil desc a

reboot :: String -> IO ()
reboot reason = do
	hPutStrLn stderr ("watchdog rebooting because " ++ reason)
	hFlush stderr
	_ <- system "shutdown -r now 'house watchdog detected a problem'"
	return ()

stopAutomation :: IO ()
stopAutomation = do
	_ <- system "systemctl stop house-poller"
	_ <- system "systemctl stop house-controller"
	return ()

-- This should not be used with rrd files, because the system clock could
-- be wrong after a reboot, set to eg an hour behind the time of last write
-- to the rrd file. That causes rrdtool to refuse to update the rrd file
-- for that period of time.
--
-- For similar reasons, this does not compare the file modification time
-- with the current time. Instead, it detects when the modification time
-- changes, and counts how much time has elpased.
pollerFileTooOld :: TMVar (Integer, Maybe EpochTime) -> FilePath -> IO Bool
pollerFileTooOld tv f = go
  where
	go = try' (modificationTime <$> getFileStatus f) >>= \case
		Right mtime -> atomically (takeTMVar tv) >>= \case
			(n, Nothing) -> do
				atomically $ putTMVar tv (n, Just mtime)
				return False
			(n, Just prev) -> do
				if mtime /= prev
					then do
						atomically $ putTMVar tv (0, Just mtime)
						return False
					else do
						atomically $ putTMVar tv (succ n, Just mtime)
						checkn n
		Left _ -> do
			(n, oldv) <- atomically $ takeTMVar tv
			atomically $ putTMVar tv (succ n, oldv)
			checkn n
	
	try' :: IO a -> IO (Either SomeException a)
	try' = try

	-- File is too old if unchanged for 20 minutes.
	checkn n = return (succ n > 20)
